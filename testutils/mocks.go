/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package testutils

import (
	"net/http"
	"net/http/httptest"
)

type MockCanvas struct {
	handler func(*MockCanvas, http.ResponseWriter, *http.Request, interface{})
	Server  *httptest.Server
	Arg     interface{}
}

func (m *MockCanvas) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	m.handler(m, w, req, m.Arg)
}

func NewMockCanvasServer(handler func(*MockCanvas, http.ResponseWriter, *http.Request, interface{}), arg interface{}) MockCanvas {
	mock := MockCanvas{
		handler: handler,
		Arg:     arg,
	}
	mock.Server = httptest.NewTLSServer(&mock)
	http.DefaultClient = mock.Server.Client()
	return mock
}
