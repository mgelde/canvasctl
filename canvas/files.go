/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package canvas

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"

	"github.com/sirupsen/logrus"
	"gitlab.com/mgelde/canvasctl/rest"
)

func (ctx *CourseContext) ListFiles() error {
	if ctx == nil {
		panic("Context in ListFiles() is nil")
	}
	course := ctx.Course()
	id, err := course.AsId()
	if err != nil {
		return err
	}
	files, err := ctx.API.ListFiles(id)
	if err != nil {
		return err
	}
	fmt.Println("Files in course:", course.AsName())
	for _, file := range files {
		fmt.Println(" -", file.DisplayName)
		fmt.Println("   Hidden:", file.Hidden)
		fmt.Println("   URL:", file.Url)
	}
	return nil
}

func (ctx *CourseContext) PushFile(localname, remotename, remotepath string) (rest.CanvasId, string, error) {
	if ctx == nil {
		panic("Context in PushFile() is nil")
	}
	course := ctx.Course()
	id, err := course.AsId()
	if err != nil {
		logrus.Error(err)
		return 0, "", fmt.Errorf("Did not get an ID for the course.")
	}
	finfo, err := os.Stat(localname)
	if err != nil {
		if os.IsNotExist(err) {
			return 0, "", fmt.Errorf("File '%s' does not exist.", localname)
		}
		return 0, "", fmt.Errorf("Cannot stat file. %s", err)
	}
	if finfo.Size() < 0 {
		logrus.Errorf("File size was negative: %d", finfo.Size())
		return 0, "", fmt.Errorf("Error during stat.")
	}
	fsize := uint64(finfo.Size())
	if finfo.IsDir() {
		return 0, "", fmt.Errorf("Error. %s is a directory. Please specify a file", localname)
	}
	file, err := os.Open(localname)
	if err != nil {
		logrus.Error(err)
		if os.IsPermission(err) {
			return 0, "", fmt.Errorf("Cannot open file. Permission denied")
		}
		return 0, "", fmt.Errorf("Cannot open file: %s", err)
	}
	defer file.Close()

	logrus.Debugf("calling PushFile. local=%s, remote=%s, remotepath=%s, fsize=%d, id=%d",
		localname, remotename, remotepath, fsize, id)

	pushSpec := rest.PushFileSpec{
		File:           file,
		FileSize:       fsize,
		RemoteFilename: remotename,
		RemotePath:     remotepath,
	}
	uploadResponse, err := ctx.API.PushFile(id, pushSpec)
	if err != nil && rest.NeedsAuthentication(err) {
		return 0, "", fmt.Errorf("Error. Not authenticated. Is your token valid?")
	}
	if err == nil {
		logrus.Debugf("New file has id %d and URL %s", uploadResponse.Id, uploadResponse.Url)
	}
	return uploadResponse.Id, uploadResponse.Url, err
}

func (ctx *Context) PublishFile(id rest.CanvasId, publish bool) error {
	if ctx == nil {
		panic("Context in ModifyFile() is nil")
	}
	logrus.Debugf("Attempting to set publication status of file %d to %v", id, publish)

	mod := rest.MakeFileModification().Published(publish).Build()
	_, err := ctx.API.ModifyFile(id, mod)
	if err != nil && rest.NeedsAuthentication(err) {
		return fmt.Errorf("Error. Not authenticated. Is your token valid?")
	} else if err != nil {
		return fmt.Errorf("Error modifying file %s", err)
	}
	logrus.Debug("Success")
	return nil
}

func (ctx *Context) MoveFile(fileId, newFolderId rest.CanvasId) error {
	if ctx == nil {
		panic("Context in MoveFile is nil")
	}
	logrus.Debugf("Moving file with ID %d to folder %d", fileId, newFolderId)

	modification := rest.MakeFileModification().Move(newFolderId)
	file, err := ctx.API.ModifyFile(fileId, modification.Build())
	logrus.Debugf("Got this file info back: %v", file)
	return err
}

type directoryTreeImpl struct {
	name       string
	parent     DirectoryTree
	children   []DirectoryTree
	folderInfo rest.FolderInfo
	files      []rest.FileInfo
}

type DirectoryTree interface {
	Name() string
	Parent() DirectoryTree
	Children() []DirectoryTree
	FolderInfo() rest.FolderInfo
	Files() []rest.FileInfo
}

func (dir *directoryTreeImpl) Name() string {
	if dir == nil {
		panic("received nil")
	}
	return dir.name
}

func (dir *directoryTreeImpl) Parent() DirectoryTree {
	if dir == nil {
		panic("received nil")
	}
	return dir.parent
}

func (dir *directoryTreeImpl) Children() []DirectoryTree {
	if dir == nil {
		panic("received nil")
	}
	result := make([]DirectoryTree, len(dir.children))
	numCopied := copy(result, dir.children)
	if numCopied < len(dir.children) {
		panic("O noez")
	}
	return result
}

func (dir *directoryTreeImpl) FolderInfo() rest.FolderInfo {
	if dir == nil {
		panic("received nil")
	}
	return dir.folderInfo
}

func (dir *directoryTreeImpl) Files() []rest.FileInfo {
	if dir == nil {
		panic("received nil")
	}
	result := make([]rest.FileInfo, len(dir.files))
	numCopied := copy(result, dir.files)
	if numCopied < len(dir.files) {
		panic("O noez")
	}
	return result
}

func (ctx *CourseContext) BuildCourseDirTree() (DirectoryTree, []DirectoryTree, error) {
	if ctx == nil {
		panic("Nil context in BuildCourseDirTree")
	}
	logrus.Info("Building DirTree")
	id, err := ctx.Course().AsId()
	if err != nil {
		return nil, nil, err
	}

	files, err := ctx.API.ListFiles(id)
	if err != nil {
		logrus.Error("Error getting all files: ", err)
		return nil, nil, err
	}
	folders, err := ctx.API.ListAllFoldersInCourse(id)
	if err != nil {
		logrus.Error("Error getting all folders: ", err)
		return nil, nil, err
	}
	foldersCache := make(map[rest.CanvasId]*directoryTreeImpl, len(folders))
	for _, folder := range folders {
		dt := new(directoryTreeImpl)
		dt.name = folder.Name
		dt.folderInfo = folder
		dt.children = make([]DirectoryTree, 0, 200)
		dt.files = make([]rest.FileInfo, 0, len(files)/len(folders))
		foldersCache[folder.Id] = dt
	}

	dangling := make([]DirectoryTree, 0, len(folders))
	var root *directoryTreeImpl = nil
	for _, value := range foldersCache {
		logrus.Debugf("Performing lookup on parent ID %d", value.folderInfo.ParentFolderId)
		if parent, hit := foldersCache[value.folderInfo.ParentFolderId]; !hit {
			if value.folderInfo.ParentFolderId == 0 {
				if root != nil {
					logrus.Errorf(
						"Already have a root. Data appears malformed. Already have %d, but now also found %d",
						root.folderInfo.Id, value.folderInfo.Id)
					return nil, nil, fmt.Errorf("Cannot determine root of directory-tree")
				}
				root = value
			} else {
				dangling = append(dangling, value)
			}
		} else {
			value.parent = parent
			parent.children = append(parent.children, value)
		}
	}
	if root == nil {
		return nil, nil, fmt.Errorf("Could not determine root of tree")
	}
	for _, file := range files {
		dt := foldersCache[file.FolderId]
		dt.files = append(dt.files, file)
	}
	return root, dangling, nil
}

type ResolvedPath struct {
	Filename string
	Path     string
	Files    []rest.FileInfo
	Folders  []rest.FolderInfo
}

func (ctx *CourseContext) ResolvePath(path string) (ResolvedPath, error) {
	if ctx == nil {
		panic("Context is nil")
	}
	id, err := ctx.Course().AsId()
	if err != nil {
		return ResolvedPath{}, err
	}
	dir := path
	filename := ""

	logrus.Debugf("Resolving path %s", path)
	folders, err := ctx.API.ResolvePath(id, path)
	if err != nil {
		logrus.Debugf("Could not resolve full path %s", path)
		logrus.Debugf("Reason: %s", err)
		if httpErr, ok := rest.IsHTTPError(err); !ok {
			// not an http error; abort
			return ResolvedPath{}, err
		} else {
			if httpErr.Code() != http.StatusNotFound {
				// not a 404; abort
				logrus.Errorf("HTTP Error: %v", string(httpErr.Body()))
				return ResolvedPath{}, err
			}
		}

		dir, filename = filepath.Split(path)
		logrus.Debugf("Trying to resolve %s", dir)
		folders, err = ctx.API.ResolvePath(id, dir)
	}
	if err != nil {
		logrus.Infof("Resolution error on %s. Reason %v", dir, err)
		if httpErr, ok := rest.IsHTTPError(err); ok {
			errorMsg := rest.UnmarshalErrors(httpErr.Body())
			logrus.Debugf("Error message: %v", errorMsg.Errors)
			if httpErr.Code() == http.StatusNotFound {
				return ResolvedPath{}, fmt.Errorf("File or folder was not found on server %s", dir)
			}
			logrus.Warnf("The server responded with:")
			for _, msg := range errorMsg.Errors {
				logrus.Warnf("-> %s", msg)
			}
			logrus.Warnf("Unexpected error %s", err)
			return ResolvedPath{}, fmt.Errorf("Unexpected HTTP status code: %s", http.StatusText(httpErr.Code()))
		}
		logrus.Warnf("Unexpected error %s", err)
		return ResolvedPath{}, fmt.Errorf("Unexpected error. I do not know what happened here.")
	}
	if len(folders) <= 0 {
		return ResolvedPath{}, fmt.Errorf("Found a folder at %s, but could not retrieve it.", dir)
	}
	last_dir := folders[len(folders)-1]
	logrus.Debugf("Retrieved folder with id %d, name %s", last_dir.Id, last_dir.Name)
	if filename == "" {
		//TODO: Implement
		//subfolders, err := ctx.API.ListFoldersInFolder(last_dir.Id)

		files, err := ctx.API.ListFilesInFolder(last_dir.Id)
		if err != nil {
			logrus.Debugf("Could not load files in folder %d. Reason: %s", last_dir.Id, err)
			if httpErr, ok := rest.IsHTTPError(err); ok {
				errorMsg := rest.UnmarshalErrors(httpErr.Body())
				logrus.Debugf("Error message: %v", errorMsg.Errors)
				if httpErr.Code() == http.StatusNotFound {
					return ResolvedPath{}, fmt.Errorf("Folder was not found on server %s", last_dir.Name)
				}
				logrus.Warnf("The server responded with:")
				for _, msg := range errorMsg.Errors {
					logrus.Warnf("-> %s", msg)
				}
				logrus.Warnf("Unexpected error %s", err)
				return ResolvedPath{}, fmt.Errorf("Unexpected HTTP status code: %s", http.StatusText(httpErr.Code()))
			}
			logrus.Warnf("Unexpected error %s", err)
			return ResolvedPath{}, fmt.Errorf("Unexpected error. I do not know what happened here.")
		}
		return ResolvedPath{
			Files: files,
			//Folders: subfolders,
			Path:     dir,
			Filename: filename,
		}, nil
	}
	files, err := ctx.API.ListFilesInFolder(last_dir.Id)
	if err != nil {
		logrus.Debugf("Could not load files in folder %d. Reason: %s", last_dir.Id, err)
		if httpErr, ok := rest.IsHTTPError(err); ok {
			errorMsg := rest.UnmarshalErrors(httpErr.Body())
			logrus.Debugf("Error message: %v", errorMsg.Errors)
			if httpErr.Code() == http.StatusNotFound {
				return ResolvedPath{}, fmt.Errorf("Folder was not found on server %s", last_dir.Name)
			}
			logrus.Warnf("The server responded with:")
			for _, msg := range errorMsg.Errors {
				logrus.Warnf("-> %s", msg)
			}
			logrus.Warnf("Unexpected error %s", err)
			return ResolvedPath{}, fmt.Errorf("Unexpected HTTP status code: %s", http.StatusText(httpErr.Code()))
		}
		logrus.Warnf("Unexpected error %s", err)
		return ResolvedPath{}, fmt.Errorf("Unexpected error. I do not know what happened here.")
	}
	for _, file := range files {
		if file.DisplayName == filename {
			return ResolvedPath{
				Files:    []rest.FileInfo{file},
				Folders:  nil,
				Path:     dir,
				Filename: filename,
			}, nil
		}
	}
	return ResolvedPath{}, fmt.Errorf("File %s does not appear to be inside the subfolder %s", filename, dir)
}
