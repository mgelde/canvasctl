/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package canvas

import (
	"fmt"
	"github.com/stretchr/testify/require"
	"gitlab.com/mgelde/canvasctl/rest"
	"testing"
)

func TestListAccounts(t *testing.T) {
	mockAPI := new(MockAPI)
	mockAPI.On("GetAccounts").Return([]rest.Account{
		{Id: 1234, Name: "A", DefaultStorageQuotaMb: uint32(100)},
		{Id: 1235, Name: "B", DefaultStorageQuotaMb: uint32(12)},
	}, nil).Once()
	context := CreateMockContext(mockAPI)
	err := context.ListAccounts()
	require.Nil(t, err)

	expectedError := fmt.Errorf("some error")
	mockAPI.On("GetAccounts").Return([]rest.Account{}, expectedError).Once()
	err = context.ListAccounts()
	require.NotNil(t, err)
	require.Equal(t, err, expectedError)

	mockAPI.On("GetAccounts").Return([]rest.Account{}, rest.NewAuthError(401, nil, true)).Once()
	err = context.ListAccounts()
	require.Equal(t, err.Error(), "Error. Not authenticated. Is your token valid?")
	mockAPI.AssertExpectations(t)
}
