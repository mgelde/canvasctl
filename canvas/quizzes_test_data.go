/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package canvas

// for io.Reader based test
const sampleTestData string = `
Title =  "My Quiz"
Text = "This is a quiz"

[[Groups]]
Name= "Group 1"
PickCount = 1
Points = 1.2

[[Groups.McQuestions]]
Title = "Question 1"
Text = "This is the first question"
Correct = "this one is correct"
Wrong = ["these", "are", "all", "false"]


[[Groups.ShortAnswerQuestions]]
Title= "Question 4"
Text = "This is a short answer questions"
Answers = ["one", "two", "three"]

[[Groups.FillBlanksQuestions]]
Title = "Bla"
Text = "This is [a]. There are [b] [c]."
[Groups.FillBlanksQuestions.Answers]
a = ["question"]
b = ["three"]
c = ["blank fields", "blanks"]
`
