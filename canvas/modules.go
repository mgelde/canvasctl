/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package canvas

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/mgelde/canvasctl/rest"
)

func (ctx *CourseContext) ListModules() error {
	if ctx == nil {
		panic("Context in ListModules() is nil")
	}
	course := ctx.Course()
	id, err := course.AsId()
	if err != nil {
		return err
	}
	modules, err := ctx.API.GetModules(id, true)
	if err != nil {
		logrus.Error(err)
		if rest.NeedsAuthentication(err) {
			return fmt.Errorf("Error. Not authenticated. Is your token valid?")
		}
		return fmt.Errorf("Error getting modules from server")
	}

	fmt.Printf("Modules for course %s (id=%d)\n", course.AsName(), id)
	for _, m := range modules {
		fmt.Printf(" - %s (%d items)\n", m.Name, m.ItemsCount)
		if m.Items == nil {
			continue
		}
		for _, item := range m.Items {
			fmt.Printf("   -> %s (id=%d, content id= %d, type=%s)\n",
				item.Title, item.Id, item.ContentId, item.Type)
		}
	}
	return nil
}

func (ctx *CourseContext) AddModule(name string) (rest.CanvasId, error) {
	if ctx == nil {
		panic("Context in AddModule() is nil")
	}
	course := ctx.Course()
	id, err := course.AsId()
	if err != nil {
		return rest.CanvasId(0), err
	}
	module, err := ctx.API.CreateModule(id, name)
	return module.Id, nil
}

type ModuleItemType int

const (
	MODITEM_FILE       ModuleItemType = iota
	MODITEM_PAGE       ModuleItemType = iota
	MODITEM_DISCUSSION ModuleItemType = iota
	MODITEM_ASSIGNMENT ModuleItemType = iota
	MODITEM_QUIZ       ModuleItemType = iota
	MODITEM_SUBHEADER  ModuleItemType = iota
	MODITEM_EXTURL     ModuleItemType = iota
	MODITEM_EXTTOOL    ModuleItemType = iota
)

func (t ModuleItemType) ToCanvasName() string {
	switch t {
	case MODITEM_FILE:
		return "File"
	case MODITEM_PAGE:
		return "Page"
	case MODITEM_DISCUSSION:
		return "Discussion"
	case MODITEM_ASSIGNMENT:
		return "Assignment"
	case MODITEM_QUIZ:
		return "Quiz"
	case MODITEM_SUBHEADER:
		return "SubHeader"
	case MODITEM_EXTURL:
		return "ExternalUrl"
	case MODITEM_EXTTOOL:
		return "ExternalTool"
	default:
		return "File"
	}
}

func (ctx *CourseContext) AddModuleItem(moduleId, contentId rest.CanvasId, title string, t ModuleItemType) (rest.CanvasId, error) {
	if ctx == nil {
		panic("Context in AddModuleItem() is nil")
	}
	item := rest.ModuleItem{
		Title:     title,
		Type:      t.ToCanvasName(),
		ContentId: contentId,
	}
	course := ctx.Course()
	id, err := course.AsId()
	if err != nil {
		return rest.CanvasId(0), err
	}
	newItem, err := ctx.API.CreateModuleItem(id, moduleId, item)
	if err != nil {
		if rest.IsNotAuthorizedError(err) {
			return 0, fmt.Errorf("Error. Not authenticated. Is your token valid?")
		}
		return 0, err
	}
	return newItem.Id, nil
}

func (ctx *CourseContext) ModuleIdFromName(name string, create bool) (rest.CanvasId, error) {
	if ctx == nil {
		panic("Context in ModuleIdFromName() is nil")
	}
	course := ctx.Course()
	courseId, err := course.AsId()
	if err != nil {
		return rest.CanvasId(0), err
	}

	modules, err := ctx.API.GetModules(courseId, false)
	if err != nil {
		if rest.IsNotAuthorizedError(err) {
			return 0, fmt.Errorf("Error. Not authenticated. Is your token valid?")
		}
		return 0, err
	}
	for _, m := range modules {
		if m.Name == name {
			return m.Id, nil
		}
	}
	logrus.Info("Did not find module", name)
	if !create {
		return 0, fmt.Errorf("Did not find module '%s'", name)
	}
	logrus.Info("Creating module with name", name)
	moduleId, err := ctx.AddModule(name)
	if err != nil {
		return 0, fmt.Errorf("Cannot create module %s. Reasons: %s", name, err)
	}
	return moduleId, nil
}

type ModuleItemMatcher func(rest.ModuleItem) bool

func (c *CourseContext) FindModuleItem(moduleId rest.CanvasId, matcher ModuleItemMatcher) (rest.ModuleItem, bool) {
	course := c.Course()
	courseId, err := course.AsId()
	if err != nil {
		return rest.ModuleItem{}, false
	}

	items, err := c.API.GetModuleItems(courseId, moduleId)
	if err != nil {
		return rest.ModuleItem{}, false
	}
	for _, item := range items {
		if matcher(item) {
			return item, true
		}
	}
	return rest.ModuleItem{}, false
}
