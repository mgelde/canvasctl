/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package canvas

import (
	"fmt"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/mgelde/canvasctl/canvas/quiz_parsing"
	"gitlab.com/mgelde/canvasctl/rest"
	"gitlab.com/mgelde/canvasctl/rest/quizzes"
	"path/filepath"
	"runtime"
	"strings"
	"testing"
)

func TestAddMcAnswer(t *testing.T) {
	mockAPI := new(MockAPI)
	mockContext := CreateMockCourseContextFromId(mockAPI, 123)
	mockAPI.On("CreateQuestion", mock.Anything, mock.Anything, mock.Anything).Return(
		rest.QuizQuestion{},
		fmt.Errorf("bla"),
	).Once()
	qParams, err := mockContext.createQuestion(quiz_parsing.QuestionSpec{}, 456)
	require.Nil(t, err)
	err = createMcQuestion(qParams, mockAPI, 123, 789, quiz_parsing.McQuestionSpec{
		QuestionSpec: quiz_parsing.QuestionSpec{},
		Correct:      "irrelevant",
		Wrong:        []string{"irrelevant"},
	})
	require.Equal(t, err.Error(), "bla")

	q := quizzes.NewQuestion()
	q.SetTitle("title").AddDescription("text").SetQuizGroup(789)
	q.MakeMultipleChoiceQuestion().AddAnswer("correct", true).
		AddAnswer("wrong", false).
		AddAnswer("also wrong", false)
	mockAPI.On("CreateQuestion", rest.CanvasId(123), rest.CanvasId(456), q.MakeParams()).Return(
		rest.QuizQuestion{},
		nil,
	).Once()

	questionSpec := quiz_parsing.QuestionSpec{
		Title: "title",
		Text:  "text",
	}
	qParams, err = mockContext.createQuestion(questionSpec, 789)

	require.Nil(t, err)
	err = createMcQuestion(qParams, mockAPI, 123, 456, quiz_parsing.McQuestionSpec{
		QuestionSpec: questionSpec,
		// Trailing whitespace to test that this is removed
		Correct: "correct ",
		Wrong:   []string{"wrong ", "also wrong "},
	})
	require.Nil(t, err)
	mockAPI.AssertExpectations(t)
}

func TestShortAnswer(t *testing.T) {
	mockAPI := new(MockAPI)
	mockContext := CreateMockCourseContextFromId(mockAPI, 123)
	mockAPI.On("CreateQuestion", mock.Anything, mock.Anything, mock.Anything).Return(
		rest.QuizQuestion{}, fmt.Errorf("bla")).Once()
	qParams, err := mockContext.createQuestion(quiz_parsing.QuestionSpec{}, 789)
	require.Nil(t, err)

	err = createShortAnswerQuestion(qParams, mockAPI, 123, 456, quiz_parsing.ShortAnswerQuestionSpec{})
	require.Equal(t, err.Error(), "bla")

	q := quizzes.NewQuestion()
	q.SetTitle("title").AddDescription("text").SetQuizGroup(789)
	q.MakeShortAnswer().AddAnswer("correct")
	mockAPI.On("CreateQuestion", rest.CanvasId(123), rest.CanvasId(456),
		q.MakeParams()).
		Return(rest.QuizQuestion{}, nil).Once()

	questionSpec := quiz_parsing.QuestionSpec{
		Title: "title",
		Text:  "text",
	}
	qParams, err = mockContext.createQuestion(questionSpec, 789)
	require.Nil(t, err)
	err = createShortAnswerQuestion(qParams, mockAPI, 123, 456, quiz_parsing.ShortAnswerQuestionSpec{
		// Trailing whitespace to test that this is removed
		Answers: []string{"correct "},
	})
	require.Nil(t, err)
	mockAPI.AssertExpectations(t)
}

func TestEssayQuestion(t *testing.T) {
	mockAPI := new(MockAPI)
	mockContext := CreateMockCourseContextFromId(mockAPI, 123)
	mockAPI.On("CreateQuestion", mock.Anything, mock.Anything, mock.Anything).Return(
		rest.QuizQuestion{}, fmt.Errorf("bla")).Once()
	qParams, err := mockContext.createQuestion(quiz_parsing.QuestionSpec{}, 789)
	require.Nil(t, err)

	err = createEssayQuestion(qParams, mockAPI, 123, 456, quiz_parsing.QuestionSpec{})
	require.Equal(t, err.Error(), "bla")

	q := quizzes.NewQuestion()
	q.SetTitle("title").AddDescription("text").SetQuizGroup(789)
	q.MakeEssayQuestion()
	mockAPI.On("CreateQuestion", rest.CanvasId(123), rest.CanvasId(456),
		q.MakeParams()).
		Return(rest.QuizQuestion{}, nil).Once()

	questionSpec := quiz_parsing.QuestionSpec{
		Title: "title",
		Text:  "text",
	}
	qParams, err = mockContext.createQuestion(questionSpec, 789)
	require.Nil(t, err)
	err = createEssayQuestion(qParams, mockAPI, 123, 456, questionSpec)
	require.Nil(t, err)
	mockAPI.AssertExpectations(t)
}

func TestFileUploadQuestion(t *testing.T) {
	mockAPI := new(MockAPI)
	mockContext := CreateMockCourseContextFromId(mockAPI, 123)
	mockAPI.On("CreateQuestion", mock.Anything, mock.Anything, mock.Anything).Return(
		rest.QuizQuestion{}, fmt.Errorf("bla")).Once()
	qParams, err := mockContext.createQuestion(quiz_parsing.QuestionSpec{}, 789)
	require.Nil(t, err)

	err = createFileUploadQuestion(qParams, mockAPI, 123, 456, quiz_parsing.QuestionSpec{})
	require.Equal(t, err.Error(), "bla")

	q := quizzes.NewQuestion()
	q.SetTitle("title").AddDescription("text").SetQuizGroup(789)
	q.MakeFileUploadQuestion()
	mockAPI.On("CreateQuestion", rest.CanvasId(123), rest.CanvasId(456),
		q.MakeParams()).
		Return(rest.QuizQuestion{}, nil).Once()

	questionSpec := quiz_parsing.QuestionSpec{
		Title: "title",
		Text:  "text",
	}
	qParams, err = mockContext.createQuestion(questionSpec, 789)
	require.Nil(t, err)
	err = createFileUploadQuestion(qParams, mockAPI, 123, 456, questionSpec)
	require.Nil(t, err)
	mockAPI.AssertExpectations(t)
}

func TestAddMultiAnswer(t *testing.T) {
	mockAPI := new(MockAPI)
	mockContext := CreateMockCourseContextFromId(mockAPI, 123)
	mockAPI.On("CreateQuestion", rest.CanvasId(123), rest.CanvasId(456), mock.Anything).
		Return(rest.QuizQuestion{}, fmt.Errorf("bla")).Once()
	qParams, err := mockContext.createQuestion(quiz_parsing.QuestionSpec{Title: "title", Text: "text"}, 789)
	require.Nil(t, err)
	err = createMultiAnswerQuestion(qParams, mockAPI, 123, 456, quiz_parsing.MultAnswersQuestionSpec{
		QuestionSpec: quiz_parsing.QuestionSpec{
			Title: "title",
			Text:  "text",
		},
		Correct: []string{"correct", "also correct"},
		Wrong:   []string{"wrong", "also wrong"},
	})
	require.Equal(t, err.Error(), "bla")

	q := quizzes.NewQuestion()
	q.SetTitle("title").AddDescription("text").SetQuizGroup(789)
	q.MakeMultipleAnswerQuestion().AddAnswer("correct", true).AddAnswer("also correct", true).
		AddAnswer("wrong", false).
		AddAnswer("also wrong", false)
	mockAPI.On("CreateQuestion", rest.CanvasId(123), rest.CanvasId(456),
		q.MakeParams()).Return(rest.QuizQuestion{}, nil).Once()
	qParams, err = mockContext.createQuestion(quiz_parsing.QuestionSpec{Title: "title", Text: "text"}, 789)
	require.Nil(t, err)
	err = createMultiAnswerQuestion(qParams, mockAPI, 123, 456, quiz_parsing.MultAnswersQuestionSpec{
		QuestionSpec: quiz_parsing.QuestionSpec{
			Title: "title",
			Text:  "text",
		},
		// Trailing whitespace to test that this is removed
		Correct: []string{"correct", "also correct "},
		Wrong:   []string{"wrong", "also wrong "},
	})
	mockAPI.AssertExpectations(t)
	require.Nil(t, err, "REST succeeds -> should be no errror")
}

func getMockFile() string {
	_, filename, _, _ := runtime.Caller(0)
	dir := filepath.Dir(filename)
	return filepath.Join(dir, "mock_file.png")
}

func TestCreateQuestion(t *testing.T) {
	qSpec := quiz_parsing.QuestionSpec{
		Text:  fmt.Sprintf("A text with a placeholder {%%mime=(image/png),filename=(%s),folder=(/remotedir),type-specific=(x=(123),y=(345))%%}.", getMockFile()),
		Title: "bla",
	}

	mockAPI := new(MockAPI)

	mockAPI.On("PushFile", rest.CanvasId(123), mock.MatchedBy(func(spec rest.PushFileSpec) bool {
		return spec.RemoteFilename == "mock_file.png" && spec.RemotePath == "/remotedir"
	})).Return(rest.FileUploadResponse{
		Id:          rest.CanvasId(1235),
		Url:         "bla",
		ContentType: "image/png",
		DisplayName: "mock_file.png",
		Size:        int64(1337),
	}, nil).Once()
	mockContext := CreateMockCourseContextFromId(mockAPI, 123)
	builder, err := mockContext.createQuestion(qSpec, rest.CanvasId(6666))
	require.Nil(t, err)
	params := builder.MakeParams()
	require.Equal(t, `A text with a placeholder <img src="/courses/123/files/1235/preview" alt="mock_file.png" width="123" height="345" />.`, params.Get("question[question_text]"))
}

func TestCreateQuestionWithIllegalFilename(t *testing.T) {
	mockAPI := new(MockAPI)
	mockContext := CreateMockCourseContextFromId(mockAPI, 123)
	qSpec := quiz_parsing.QuestionSpec{
		Text:  "A text with a placeholder {%mime=(image/png),filename=(./doesnotexist.png),type-specific=(x=(123),y=(345))%}.",
		Title: "bla",
	}
	builder, err := mockContext.createQuestion(qSpec, rest.CanvasId(666))
	params := builder.MakeParams()
	require.Equal(t, "A text with a placeholder ERROR DURING UPLOAD.", params.Get("question[question_text]"))
	require.Nil(t, err)
	mockAPI.AssertNotCalled(t, "PushFile", mock.Anything, mock.Anything)
	mockAPI.AssertExpectations(t)
}

func TestAddNumeric(t *testing.T) {
	mockAPI := new(MockAPI)
	mockContext := CreateMockCourseContextFromId(mockAPI, 123)
	mockAPI.On("CreateQuestion", rest.CanvasId(123), rest.CanvasId(456),
		mock.Anything).Return(rest.QuizQuestion{}, fmt.Errorf("bla")).Once()
	qParams, err := mockContext.createQuestion(quiz_parsing.QuestionSpec{Title: "title", Text: "text"}, 789)
	require.Nil(t, err)
	err = createNumericQuestion(qParams, mockAPI, 123, 456, quiz_parsing.NumericQuestionSpec{
		QuestionSpec: quiz_parsing.QuestionSpec{
			Title: "title",
			Text:  "text",
		},
		Approximate: []quiz_parsing.ApproximateAnswer{{Answer: 12, Precision: 1}},
		Exact:       []quiz_parsing.ExactAnswer{{Answer: 12.0, ErrorMargin: 1.0}},
		Range:       []quiz_parsing.RangeAnswer{{Begin: 1.0, End: 2.3}},
	})
	require.Equal(t, err.Error(), "bla")

	q := quizzes.NewQuestion()
	q.SetTitle("title").AddDescription("text").SetQuizGroup(789)
	q.MakeNumericAnswer().AddAnswerApproximate(12, 1).AddAnswerExact(12.0, 1.0).
		AddAnswerRange(1.0, 2.3)
	mockAPI.On("CreateQuestion", rest.CanvasId(123), rest.CanvasId(456),
		q.MakeParams()).Return(rest.QuizQuestion{}, nil).Once()
	qParams, err = mockContext.createQuestion(quiz_parsing.QuestionSpec{Title: "title", Text: "text"}, 789)
	require.Nil(t, err)
	err = createNumericQuestion(qParams, mockAPI, 123, 456, quiz_parsing.NumericQuestionSpec{
		QuestionSpec: quiz_parsing.QuestionSpec{
			Title: "title",
			Text:  "text",
		},
		Approximate: []quiz_parsing.ApproximateAnswer{{Answer: 12, Precision: 1}},
		Exact:       []quiz_parsing.ExactAnswer{{Answer: 12.0, ErrorMargin: 1.0}},
		Range:       []quiz_parsing.RangeAnswer{{Begin: 1.0, End: 2.3}},
	})
	require.Nil(t, err)
	mockAPI.AssertExpectations(t)
}

func TestCreateQuizFromReader(t *testing.T) {
	reader := strings.NewReader(sampleTestData)

	mockAPI := new(MockAPI)
	mockContext := CreateMockCourseContextFromId(mockAPI, 123)
	mockAPI.On("CreateQuiz", mock.MatchedBy(func(spec rest.QuizCreationSpec) bool {
		return spec.Title == "My Quiz" && spec.QuizType == nil
	}), rest.CanvasId(123)).Return(rest.Quiz{
		Id:       456,
		QuizType: "practice_quiz",
	}, nil).Once()
	mockAPI.On("CreateQuizQuestionGroup", rest.CanvasId(123), rest.CanvasId(456), "Group 1", 1, 1.2).Return(
		rest.QuestionGroup{
			Id: 888,
		}, nil).Once()
	mockAPI.On("CreateQuestion", rest.CanvasId(123), rest.CanvasId(456), mock.Anything).Return(rest.QuizQuestion{
		Id: 888,
	}, nil).Times(3)

	err := mockContext.CreateQuizFromReader(reader)
	require.Nil(t, err)
	mockAPI.AssertExpectations(t)
}

func TestDeleteQuiz(t *testing.T) {
	mockAPI := new(MockAPI)
	mockContext := CreateMockCourseContextFromId(mockAPI, 123)
	mockAPI.On("DeleteQuiz", rest.CanvasId(123), rest.CanvasId(456)).Return(nil).Once()
	err := mockContext.DeleteQuiz(456)
	require.Nil(t, err)
	mockAPI.AssertExpectations(t)
}
