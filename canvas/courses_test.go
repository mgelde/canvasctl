/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package canvas

import (
	"fmt"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/mgelde/canvasctl/rest"
	"testing"
)

func TestShowCourse(t *testing.T) {
	mockAPI := new(MockAPI)
	mockAPI.On("GetSingleCourse", rest.CanvasId(1337)).Return(rest.Course{Name: "bla"}, nil).Once()
	context := CreateMockCourseContext(mockAPI, &integerCourseId{Id: 1337, name: "name"})
	require.Nil(t, context.ShowCourse())

	mockAPI.On("GetSingleCourse", mock.Anything).Return(rest.Course{}, fmt.Errorf("some error")).Once()
	require.NotNil(t, context.ShowCourse())
	mockAPI.AssertExpectations(t)
}

func TestListCourses(t *testing.T) {
	mockAPI := new(MockAPI)
	mockAPI.On("GetCourses").Return([]rest.Course{}, nil).Once()
	context := CreateMockContext(mockAPI)
	require.Nil(t, context.ListCourses())

	mockAPI.On("GetCourses").Return([]rest.Course{}, fmt.Errorf("some error")).Once()
	require.NotNil(t, context.ListCourses())

	mockAPI.On("GetCourses").Return([]rest.Course{}, rest.NewAuthError(401, nil, true)).Once()
	require.Equal(t, context.ListCourses().Error(), "Error. Not authenticated. Is your token valid?")
	mockAPI.AssertExpectations(t)
}

func TestCourseFromId(t *testing.T) {
	mockAPI := new(MockAPI)
	mockAPI.On("GetSingleCourse", rest.CanvasId(12)).Return(rest.Course{Id: 12, Name: "Course1"}, nil).Once()
	course := CourseFromId(mockAPI, rest.CanvasId(12))
	id, err := course.AsId()
	require.Nil(t, err, "should not get error querying name")
	require.Equal(t, id, rest.CanvasId(12))
	require.Equal(t, course.AsName(), "Course1")
	mockAPI.AssertExpectations(t)
}

func TestCourseFromName(t *testing.T) {
	mockAPI := new(MockAPI)
	mockAPI.On("GetCourses").Return(
		[]rest.Course{{Id: 12, Name: "Course1"}, {Id: 13, Name: "Course2"}},
		nil).Once()
	course := CourseFromName(mockAPI, "Course2")
	require.Equal(t, course.AsName(), "Course2")
	id, err := course.AsId()
	require.Nil(t, err, "should not get error querying name")
	require.Equal(t, id, rest.CanvasId(13))
	mockAPI.AssertExpectations(t)
}
