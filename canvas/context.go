/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package canvas

import (
	"fmt"
	"strings"

	"github.com/99designs/keyring"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/mgelde/canvasctl/config"
	"gitlab.com/mgelde/canvasctl/rest"
)

type Context struct {
	API        rest.API
	CliContext *cli.Context
}

type CourseContext struct {
	Context
	course CanvasCourse
}

func apiFromCli(c *cli.Context) (rest.API, error) {
	// TODO: Pass in config as a parameter
	conf, err := config.LoadConfig()
	if err != nil {
		return nil, err
	}

	url := conf.GetDefaultURL()
	logrus.Debug("Default URL:", url)

	// command-line overrides default
	if c.IsSet("url") {
		url = c.String("url")
	}
	url = strings.TrimSpace(url)
	if url == "" {
		return nil, fmt.Errorf("No url in config or on commandline")
	}

	token, err := conf.GetTokenForURL(url)
	if err != nil {
		logrus.Error("Could not load token:", err)
		return nil, err
	}
	if token == "" {
		logrus.Warn("No token in config. Trying keyring")
		ring, err := keyring.Open(keyring.Config{
			// TODO: Make this configurable
			ServiceName:             "Default_keyring",
			LibSecretCollectionName: "Default_keyring",
		})
		item, err := ring.Get(url)
		if err != nil {
			logrus.Error("Could not load token:", err)
			return nil, fmt.Errorf("No token in config or keyring")
		}
		token = string(item.Data)
	} else {
		if config.IsEncrypted(token) {
			plainToken, err := config.DecryptTokenForUrl(token, url)
			if err != nil {
				return nil, err
			}
			token = plainToken
		}
	}
	api, err := rest.NewAPI(url, token)
	if err != nil {
		return nil, err
	}
	return api, nil
}

func NewContext(cliContext *cli.Context) (*Context, error) {
	api, err := apiFromCli(cliContext)
	if err != nil {
		return nil, err
	}
	return &Context{API: api, CliContext: cliContext}, nil
}

func NewCourseContext(cliContext *cli.Context) (*CourseContext, error) {
	plainContext, err := NewContext(cliContext)
	if err != nil {
		return nil, err
	}
	ctx := CourseContext{
		Context: *plainContext,
		course:  nil,
	}
	if cliContext.IsSet("id") {
		ctx.course = CourseFromId(ctx.API, rest.CanvasId(cliContext.Uint64("id")))
	} else if cliContext.IsSet("name") {
		ctx.course = CourseFromName(ctx.API, cliContext.String("name"))
	}
	if ctx.course == nil {
		return nil, fmt.Errorf("No course specified. Try '-i' or '-n'.")
	}
	return &ctx, nil
}

func (ctx *CourseContext) Course() CanvasCourse {
	if ctx == nil {
		panic("nil receiver in Course() on CourseContext")
	}
	if ctx.course == nil {
		panic("course on CourseContext is nil")
	}
	return ctx.course
}
