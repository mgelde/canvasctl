/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package canvas

import (
	"flag"
	"github.com/stretchr/testify/mock"
	"github.com/urfave/cli/v2"
	"gitlab.com/mgelde/canvasctl/rest"
	"net/url"
)

func CreateMockContext(mockAPI rest.API) Context {
	fs := flag.NewFlagSet("bla", flag.PanicOnError)
	fs.String("id", "", "")
	fs.String("name", "", "")
	return Context{
		API:        mockAPI,
		CliContext: cli.NewContext(cli.NewApp(), fs, nil),
	}
}

func CreateMockCourseContextFromId(mockAPI rest.API, id rest.CanvasId) CourseContext {
	rv := CourseContext{
		CreateMockContext(mockAPI),
		nil,
	}
	rv.CliContext.Set("id", id.ToBase10String())
	rv.course = CourseFromId(mockAPI, id)
	return rv
}

func CreateMockCourseContextFromName(mockAPI rest.API, name string) CourseContext {
	rv := CourseContext{
		CreateMockContext(mockAPI),
		nil,
	}
	rv.CliContext.Set("name", name)
	rv.course = CourseFromName(mockAPI, name)
	return rv
}

func CreateMockCourseContext(mockAPI rest.API, mockCourse CanvasCourse) CourseContext {
	if mockCourse == nil {
		panic("course should never be nil")
	}
	return CourseContext{
		CreateMockContext(mockAPI),
		mockCourse,
	}
}

type MockAPI struct {
	mock.Mock
}

func (m *MockAPI) GetModules(courseId rest.CanvasId, details bool) ([]rest.Module, error) {
	args := m.Called(courseId, details)
	return args.Get(0).([]rest.Module), args.Error(1)
}

func (m *MockAPI) CreateModule(id rest.CanvasId, name string) (rest.Module, error) {
	args := m.Called(id, name)
	return args.Get(0).(rest.Module), args.Error(1)
}

func (m *MockAPI) CreateModuleItem(courseId, moduleId rest.CanvasId, item rest.ModuleItem) (rest.ModuleItem, error) {
	args := m.Called(courseId, moduleId, item)
	return args.Get(0).(rest.ModuleItem), args.Error(1)
}

func (m *MockAPI) GetSingleCourse(id rest.CanvasId) (rest.Course, error) {
	args := m.Called(id)
	return args.Get(0).(rest.Course), args.Error(1)
}

func (m *MockAPI) GetCourses() ([]rest.Course, error) {
	args := m.Called()
	return args.Get(0).([]rest.Course), args.Error(1)
}

func (m *MockAPI) GetAccounts() ([]rest.Account, error) {
	args := m.Called()
	return args.Get(0).([]rest.Account), args.Error(1)
}

func (m *MockAPI) PushFile(id rest.CanvasId, pushSpec rest.PushFileSpec) (rest.FileUploadResponse, error) {
	args := m.Called(id, pushSpec)
	return args.Get(0).(rest.FileUploadResponse), args.Error(1)
}

func (m *MockAPI) ListFiles(id rest.CanvasId) ([]rest.FileInfo, error) {
	args := m.Called(id)
	return args.Get(0).([]rest.FileInfo), args.Error(1)
}

func (m *MockAPI) GetModuleItems(courseId, moduleId rest.CanvasId) ([]rest.ModuleItem, error) {
	args := m.Called(courseId, moduleId)
	return args.Get(0).([]rest.ModuleItem), args.Error(1)
}

func (m *MockAPI) ModifyFile(id rest.CanvasId, mod url.Values) (rest.FileInfo, error) {
	args := m.Called(id, mod)
	return args.Get(0).(rest.FileInfo), args.Error(1)
}

func (m *MockAPI) ListQuizzes(id rest.CanvasId) ([]rest.Quiz, error) {
	args := m.Called(id)
	return args.Get(0).([]rest.Quiz), args.Error(1)
}

func (m *MockAPI) CreateQuiz(spec rest.QuizCreationSpec, id rest.CanvasId) (rest.Quiz, error) {
	args := m.Called(spec, id)
	return args.Get(0).(rest.Quiz), args.Error(1)
}

func (m *MockAPI) CreateQuestion(courseId, quizId rest.CanvasId, params *url.Values) (rest.QuizQuestion, error) {
	args := m.Called(courseId, quizId, params)
	return args.Get(0).(rest.QuizQuestion), args.Error(1)
}

func (m *MockAPI) CreateQuizQuestionGroup(courseId, quizId rest.CanvasId, title string, pickCount int, points float64) (rest.QuestionGroup, error) {
	args := m.Called(courseId, quizId, title, pickCount, points)
	return args.Get(0).(rest.QuestionGroup), args.Error(1)
}

func (m *MockAPI) DeleteQuiz(courseId, quizId rest.CanvasId) error {
	args := m.Called(courseId, quizId)
	return args.Error(0)
}

func (m *MockAPI) ListAllFoldersInCourse(id rest.CanvasId) ([]rest.FolderInfo, error) {
	args := m.Called(id)
	return args.Get(0).([]rest.FolderInfo), args.Error(1)
}

func (m *MockAPI) SetProgressCallback(cb rest.ProgressCallback) rest.ProgressCallback {
	args := m.Called(cb)
	return args.Get(0).(rest.ProgressCallback)
}

func (m *MockAPI) ResolvePath(courseId rest.CanvasId, path string) ([]rest.FolderInfo, error) {
	args := m.Called(courseId, path)
	return args.Get(0).([]rest.FolderInfo), args.Error(1)
}

func (m *MockAPI) ListFilesInFolder(folderId rest.CanvasId) ([]rest.FileInfo, error) {
	args := m.Called(folderId)
	return args.Get(0).([]rest.FileInfo), args.Error(1)
}

func (m *MockAPI) GetFile(fileId rest.CanvasId) (rest.FileInfo, error) {
	args := m.Called(fileId)
	return args.Get(0).(rest.FileInfo), args.Error(1)
}

func (m *MockAPI) ListFoldersInFolder(folderId rest.CanvasId) ([]rest.FolderInfo, error) {
	args := m.Called(folderId)
	return args.Get(0).([]rest.FolderInfo), args.Error(1)
}

func (m *MockAPI) GetQuiz(courseId, quizId rest.CanvasId) (rest.Quiz, error) {
	args := m.Called(courseId, quizId)
	return args.Get(0).(rest.Quiz), args.Error(1)
}

func (m *MockAPI) UpdateQuiz(courseId, quizId rest.CanvasId, spec rest.QuizCreationSpec) error {
	args := m.Called(courseId, quizId, spec)
	return args.Error(0)
}
