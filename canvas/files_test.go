/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package canvas

import (
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"testing"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/mgelde/canvasctl/rest"
)

func TestPushFileDoesNotCallApiOnError(t *testing.T) {
	mockAPI := new(MockAPI)

	context := CreateMockCourseContext(mockAPI, &integerCourseId{Id: 1337, name: "name"})
	_, _, err := context.PushFile("illegalfilename", "remote", "path")
	require.NotNil(t, err, "file does not exist -> should give an errror")

	_, _, err = context.PushFile(os.TempDir(), "remote", "path")
	require.NotNil(t, err, "file is directory -> should give an errror")
	mockAPI.AssertNotCalled(t, "PushFile", mock.Anything, mock.Anything)
	mockAPI.AssertExpectations(t)
}

func TestPushFile(t *testing.T) {
	mockAPI := new(MockAPI)
	context := CreateMockCourseContext(mockAPI, &integerCourseId{Id: 1337, name: "name"})

	tempfile, _ := ioutil.TempFile(os.TempDir(), "blaPattern")
	tempfile.WriteString("13 bytes long")
	tempfile.Close()

	mockAPI.On("PushFile", rest.CanvasId(1337),
		mock.MatchedBy(func(spec rest.PushFileSpec) bool {
			return spec.FileSize == uint64(13) &&
				spec.RemoteFilename == "remote" &&
				spec.RemotePath == "path"
		})).Return(
		rest.FileUploadResponse{
			Id:          rest.CanvasId(12),
			Size:        123,
			DisplayName: "bla",
			Url:         "https://example.com/bla/foo",
		},
		nil).Once()

	id, url, err := context.PushFile(tempfile.Name(), "remote", "path")
	require.Nil(t, err, "did not expect an error pushing", err)
	require.Equal(t, id, rest.CanvasId(12))
	require.Equal(t, url, "https://example.com/bla/foo")

	mockAPI.On("PushFile", mock.Anything, mock.Anything).Return(
		rest.FileUploadResponse{}, rest.NewAuthError(401, nil, true)).Once()
	_, _, err = context.PushFile(tempfile.Name(), "ignored", "ignored")
	require.NotNil(t, err, "not authenticated -> should give an errror")
	require.Equal(t, err.Error(), "Error. Not authenticated. Is your token valid?")
	mockAPI.AssertExpectations(t)
}

func TestListFiles(t *testing.T) {
	mockAPI := new(MockAPI)
	mockAPI.On("ListFiles", rest.CanvasId(1337)).Return([]rest.FileInfo{
		{
			Id:       rest.CanvasId(12),
			Filename: "bla1",
			Size:     666,
		},
		{
			Id:       rest.CanvasId(13),
			Filename: "bla2",
			Size:     777,
		},
	}, nil).Once()

	context := CreateMockCourseContext(mockAPI, integerCourseId{Id: 1337, name: "name"})
	err := context.ListFiles()
	require.Nil(t, err)
	mockAPI.AssertExpectations(t)
}

func TestMoveFile(t *testing.T) {
	mockAPI := new(MockAPI)
	mockAPI.On("ModifyFile", rest.CanvasId(666), mock.MatchedBy(func(v url.Values) bool {
		return v.Get("parent_folder_id") == "12"
	})).Return(rest.FileInfo{
		Filename: "bla",
		Size:     777,
		Id:       666,
	}, nil).Once()
	context := CreateMockCourseContext(mockAPI, integerCourseId{Id: 1337, name: "name"})
	err := context.MoveFile(666, 12)
	require.Nil(t, err)
	mockAPI.AssertExpectations(t)
}

func TestBuildNormalTree(t *testing.T) {
	mockAPI := new(MockAPI)
	mockAPI.On("ListFiles", rest.CanvasId(1337)).Return([]rest.FileInfo{
		{
			Id:       rest.CanvasId(12),
			Filename: "inroot",
			Size:     666,
			FolderId: 1000,
		},
		{
			Id:       rest.CanvasId(13),
			Filename: "level1",
			Size:     777,
			FolderId: 1001,
		},
		{
			Id:       rest.CanvasId(14),
			Filename: "level1also",
			Size:     777,
			FolderId: 1001,
		},
		{
			Id:       rest.CanvasId(15),
			Filename: "level1aswell",
			Size:     777,
			FolderId: 1003,
		},
		{
			Id:       rest.CanvasId(16),
			Filename: "deperdown",
			Size:     777,
			FolderId: 1005,
		},
	}, nil).Once()
	mockAPI.On("ListAllFoldersInCourse", rest.CanvasId(1337)).Return([]rest.FolderInfo{
		{
			Id:             rest.CanvasId(1000),
			Name:           "dira",
			ParentFolderId: 0,
		},
		{
			Id:             rest.CanvasId(1001),
			Name:           "dirb",
			ParentFolderId: 1000,
		},
		{
			Id:             rest.CanvasId(1002),
			Name:           "dirc",
			ParentFolderId: 1000,
		},
		{
			Id:             rest.CanvasId(1003),
			Name:           "dird",
			ParentFolderId: 1000,
		},
		{
			Id:             rest.CanvasId(1004),
			Name:           "dire",
			ParentFolderId: 1001,
		},
		{
			Id:             rest.CanvasId(1005),
			Name:           "dirf",
			ParentFolderId: 1004,
		},
	}, nil).Once()
	context := CreateMockCourseContext(mockAPI, integerCourseId{Id: 1337, name: "name"})
	root, dangling, err := context.BuildCourseDirTree()
	require.Nil(t, err)
	require.Empty(t, dangling)
	require.Equal(t, rest.CanvasId(1000), root.FolderInfo().Id)
	for _, child := range root.Children() {
		require.Contains(t, []rest.CanvasId{1001, 1002, 1003}, child.FolderInfo().Id)
		if child.FolderInfo().Id == 1001 {
			require.Len(t, child.Children(), 1)
			require.Len(t, child.Files(), 2)

			grandchild := child.Children()[0]
			require.Equal(t, rest.CanvasId(1004), grandchild.FolderInfo().Id)
			require.Len(t, grandchild.Children(), 1)
			require.Len(t, grandchild.Files(), 0)

			require.Equal(t, rest.CanvasId(1005), grandchild.Children()[0].FolderInfo().Id)
			require.Len(t, grandchild.Children()[0].Files(), 1)
			require.Equal(t, rest.CanvasId(16), grandchild.Children()[0].Files()[0].Id)
		}
	}
	require.Len(t, root.Children(), 3)
	require.Len(t, root.Files(), 1)
	require.Equal(t, rest.CanvasId(12), root.Files()[0].Id)
}

func TestBuilTreeWithDanglingFolders(t *testing.T) {
	mockAPI := new(MockAPI)
	mockAPI.On("ListFiles", rest.CanvasId(1337)).Return([]rest.FileInfo{
		{
			Id:       rest.CanvasId(12),
			Filename: "inroot",
			Size:     666,
			FolderId: 1000,
		},
		{
			Id:       rest.CanvasId(13),
			Filename: "level1",
			Size:     777,
			FolderId: 1001,
		},
	}, nil).Once()
	mockAPI.On("ListAllFoldersInCourse", rest.CanvasId(1337)).Return([]rest.FolderInfo{
		{
			Id:             rest.CanvasId(1000),
			Name:           "dira",
			ParentFolderId: 0,
		},
		{
			Id:             rest.CanvasId(1001),
			Name:           "dirb",
			ParentFolderId: 1337,
		},
		{
			Id:             rest.CanvasId(1002),
			Name:           "dirc",
			ParentFolderId: 1000,
		},
		{
			Id:             rest.CanvasId(1003),
			Name:           "dird",
			ParentFolderId: 1000,
		},
		{
			Id:             rest.CanvasId(1004),
			Name:           "dire",
			ParentFolderId: 1338,
		},
		{
			Id:             rest.CanvasId(1005),
			Name:           "dirf",
			ParentFolderId: 1002,
		},
	}, nil).Once()
	context := CreateMockCourseContext(mockAPI, integerCourseId{Id: 1337, name: "name"})
	root, dangling, err := context.BuildCourseDirTree()
	require.Nil(t, err)
	require.Len(t, dangling, 2)
	require.Equal(t, rest.CanvasId(1000), root.FolderInfo().Id)

	for _, danglingFolder := range dangling {
		require.Contains(t, []rest.CanvasId{1001, 1004}, danglingFolder.FolderInfo().Id)
	}
}

func TestWithAmbiguousRoot(t *testing.T) {
	mockAPI := new(MockAPI)
	mockAPI.On("ListFiles", rest.CanvasId(1337)).Return([]rest.FileInfo{
		{
			Id:       rest.CanvasId(12),
			Filename: "inroot",
			Size:     666,
			FolderId: 1000,
		},
		{
			Id:       rest.CanvasId(13),
			Filename: "level1",
			Size:     777,
			FolderId: 1001,
		},
	}, nil).Once()
	mockAPI.On("ListAllFoldersInCourse", rest.CanvasId(1337)).Return([]rest.FolderInfo{
		{
			Id:             rest.CanvasId(1000),
			Name:           "dira",
			ParentFolderId: 0,
		},
		{
			Id:             rest.CanvasId(1001),
			Name:           "dirb",
			ParentFolderId: 1337,
		},
		{
			Id:             rest.CanvasId(1002),
			Name:           "dirc",
			ParentFolderId: 0,
		},
		{
			Id:             rest.CanvasId(1003),
			Name:           "dird",
			ParentFolderId: 1000,
		},
		{
			Id:             rest.CanvasId(1004),
			Name:           "dire",
			ParentFolderId: 1001,
		},
		{
			Id:             rest.CanvasId(1005),
			Name:           "dirf",
			ParentFolderId: 1002,
		},
	}, nil).Once()
	context := CreateMockCourseContext(mockAPI, integerCourseId{Id: 1337, name: "name"})
	_, _, err := context.BuildCourseDirTree()
	require.NotNil(t, err)
	require.Equal(t, "Cannot determine root of directory-tree", err.Error())
}

func TestResolvePathOnFolder(t *testing.T) {
	mockAPI := new(MockAPI)
	mockAPI.On("ResolvePath", rest.CanvasId(1337), "/path/to/folder").Return([]rest.FolderInfo{
		{
			Id:   rest.CanvasId(12),
			Name: "folderA",
		},
		{
			Id:   rest.CanvasId(13),
			Name: "folderB",
		},
	}, nil).Once()
	mockAPI.On("ListFilesInFolder", rest.CanvasId(13)).Return([]rest.FileInfo{
		{
			Id:       rest.CanvasId(13),
			Filename: "level1",
			Size:     777,
			FolderId: 1001,
		},
		{
			Id:       rest.CanvasId(14),
			Filename: "level1also",
			Size:     777,
			FolderId: 1001,
		},
		{
			Id:       rest.CanvasId(15),
			Filename: "level1aswell",
			Size:     777,
			FolderId: 1003,
		},
	}, nil).Once()
	context := CreateMockCourseContext(mockAPI, integerCourseId{Id: 1337, name: "name"})
	resolvedpath, err := context.ResolvePath("/path/to/folder")
	require.Nil(t, err)
	require.Equal(t, "", resolvedpath.Filename)
	require.Equal(t, "/path/to/folder", resolvedpath.Path)
	require.Len(t, resolvedpath.Folders, 0)
	require.Len(t, resolvedpath.Files, 3)
	mockAPI.AssertExpectations(t)
}

func TestResolvePathOnFolderDoesNotExist(t *testing.T) {
	mockAPI := new(MockAPI)
	mockAPI.
		On("ResolvePath", rest.CanvasId(1337), "/path/to/folder").
		Return([]rest.FolderInfo(nil), rest.NewHTTPError(http.StatusNotFound, nil,
			errors.New("this is an error"))).Once().
		On("ResolvePath", rest.CanvasId(1337), "/path/to/").
		Return([]rest.FolderInfo(nil), rest.NewHTTPError(http.StatusNotFound, nil,
			errors.New("this is an error"))).Once()
	context := CreateMockCourseContext(mockAPI, integerCourseId{Id: 1337, name: "name"})
	_, err := context.ResolvePath("/path/to/folder")
	require.NotNil(t, err)
	mockAPI.AssertExpectations(t)
}

func TestResolvePathOnFolderFallsBackToFile(t *testing.T) {
	mockAPI := new(MockAPI)
	mockAPI.
		On("ResolvePath", rest.CanvasId(1337), "/path/to/file").
		Return([]rest.FolderInfo(nil), rest.NewHTTPError(http.StatusNotFound, nil,
			errors.New("this is an error"))).Once().
		On("ResolvePath", rest.CanvasId(1337), "/path/to/").
		Return([]rest.FolderInfo{{Id: 13, Name: "/path"}, {Id: 14, Name: "/path/to"}}, nil).Once().
		On("ListFilesInFolder", rest.CanvasId(14)).Return([]rest.FileInfo{{DisplayName: "file", Id: 666}}, nil).Once()
	context := CreateMockCourseContext(mockAPI, integerCourseId{Id: 1337, name: "name"})
	resolved, err := context.ResolvePath("/path/to/file")
	require.Nil(t, err)
	require.Equal(t, "file", resolved.Filename)
	require.Equal(t, "/path/to/", resolved.Path)
	require.Len(t, resolved.Files, 1)
	require.Equal(t, rest.CanvasId(666), resolved.Files[0].Id)
	mockAPI.AssertExpectations(t)
}

func TestResolvePathOnFolderFileDoesNotExist(t *testing.T) {
	mockAPI := new(MockAPI)
	mockAPI.
		On("ResolvePath", rest.CanvasId(1337), "/path/to/file").
		Return([]rest.FolderInfo(nil), rest.NewHTTPError(http.StatusNotFound, nil,
			errors.New("this is an error"))).Once().
		On("ResolvePath", rest.CanvasId(1337), "/path/to/").
		Return([]rest.FolderInfo{{Id: 13, Name: "/path"}, {Id: 14, Name: "/path/to"}}, nil).Once().
		On("ListFilesInFolder", rest.CanvasId(14)).Return([]rest.FileInfo{{DisplayName: "anotherfile", Id: 666}}, nil).Once()
	context := CreateMockCourseContext(mockAPI, integerCourseId{Id: 1337, name: "name"})
	_, err := context.ResolvePath("/path/to/file")
	require.NotNil(t, err)
	require.Equal(t, "File file does not appear to be inside the subfolder /path/to/", err.Error())
	mockAPI.AssertExpectations(t)
}
