/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package canvas

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/mgelde/canvasctl/rest"
)

func (ctx *Context) ListAccounts() error {
	if ctx == nil {
		panic("Context in ListAccounts() is nil")
	}
	accounts, err := ctx.API.GetAccounts()
	if err != nil {
		logrus.Error(err)
		if rest.NeedsAuthentication(err) {
			return fmt.Errorf("Error. Not authenticated. Is your token valid?")
		}
		return err
	}
	for _, a := range accounts {
		fmt.Println(a)
	}
	return nil
}
