/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package canvas

import (
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/mgelde/canvasctl/rest"
	"testing"
)

func TestNumericContextUsesPassedInID(t *testing.T) {
	mockAPI := new(MockAPI)
	context := CreateMockCourseContextFromId(mockAPI, 1337)
	require.True(t, context.CliContext.IsSet("id"), "should be set")
	course := context.Course()
	id, err := course.AsId()
	require.Nil(t, err)
	require.Equal(t, rest.CanvasId(1337), id)
	mockAPI.AssertExpectations(t)
	mockAPI.AssertNotCalled(t, "GetSingleCourse", mock.Anything)
	mockAPI.AssertNotCalled(t, "GetCourses")
}

func TestNameContextCallsApiForId(t *testing.T) {
	mockAPI := new(MockAPI)
	mockAPI.On("GetCourses").Return([]rest.Course{
		{
			Name: "a name",
			Id:   12,
		},
	}, nil).Once()
	context := CreateMockCourseContextFromName(mockAPI, "a name")
	course := context.Course()
	id, err := course.AsId()
	require.Nil(t, err)
	require.Equal(t, id, rest.CanvasId(12))
	mockAPI.AssertExpectations(t)

}
