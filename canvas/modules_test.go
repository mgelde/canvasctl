/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package canvas

import (
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/mgelde/canvasctl/rest"
	"testing"
)

func TestListModules(t *testing.T) {
	mockAPI := new(MockAPI)
	mockAPI.On("GetModules", rest.CanvasId(1337), true).Return([]rest.Module{
		{
			Id:         rest.CanvasId(12),
			ItemsCount: 0,
			Name:       "bloop",
		},
		{
			Id:         rest.CanvasId(13),
			ItemsCount: 1,
			Name:       "gloop",
			Items: []rest.ModuleItem{
				{
					Id:        rest.CanvasId(1223344),
					Type:      "File",
					ContentId: rest.CanvasId(666),
				},
			},
		}}, nil).Once()

	context := CreateMockCourseContext(mockAPI, &integerCourseId{Id: 1337, name: "name"})
	err := context.ListModules()
	require.Nil(t, err)

	mockAPI.On("GetModules", mock.Anything, mock.Anything).Return(
		[]rest.Module{}, rest.NewAuthError(401, nil, true)).Once()
	err = context.ListModules()
	require.NotNil(t, err, "not authenticated -> should give an errror")
	require.Equal(t, err.Error(), "Error. Not authenticated. Is your token valid?")
	mockAPI.AssertExpectations(t)
}

func TestFindModule(t *testing.T) {
	mockAPI := new(MockAPI)
	mockAPI.On("GetModules", rest.CanvasId(1337), false).Return(
		[]rest.Module{
			{
				Id:         rest.CanvasId(12),
				ItemsCount: 0,
				Name:       "bloop",
			},
			{
				Id:         rest.CanvasId(13),
				ItemsCount: 1,
				Name:       "gloop",
				Items: []rest.ModuleItem{
					{
						Id:        rest.CanvasId(1223344),
						Type:      "File",
						ContentId: rest.CanvasId(666),
					},
				},
			},
		},
		nil,
	).Twice()

	context := CreateMockCourseContext(mockAPI, &integerCourseId{Id: 1337, name: "name"})
	_, err := context.ModuleIdFromName("not a module name", false)
	require.NotNil(t, err, "module not present. should produce error")

	id, err := context.ModuleIdFromName("gloop", false)
	require.Nil(t, err, "module present. should not produce error")
	require.Equal(t, id, rest.CanvasId(13))

	mockAPI.On("GetModules", mock.Anything, mock.Anything).Return(
		[]rest.Module{}, rest.NewAuthError(401, nil, true)).Once()
	_, err = context.ModuleIdFromName("name", false)
	require.NotNil(t, err, "not authenticated -> should give an errror")
	require.Equal(t, err.Error(), "Error. Not authenticated. Is your token valid?")

	mockAPI.AssertExpectations(t)
	mockAPI.AssertNotCalled(t, "CreateModule", mock.Anything, mock.Anything)
}

func TestFindAndCreateModule(t *testing.T) {
	mockAPI := new(MockAPI)
	mockAPI.On("GetModules", rest.CanvasId(1337), false).Return(
		[]rest.Module{
			{
				Id:         rest.CanvasId(12),
				ItemsCount: 0,
				Name:       "bloop",
			},
			{
				Id:         rest.CanvasId(13),
				ItemsCount: 1,
				Name:       "gloop",
				Items: []rest.ModuleItem{
					{
						Id:        rest.CanvasId(1223344),
						Type:      "File",
						ContentId: rest.CanvasId(666),
					},
				},
			},
		},
		nil,
	).Once()
	mockAPI.On("CreateModule", rest.CanvasId(1337), "newmod").Return(rest.Module{
		Name: "newmod",
		Id:   1234,
	}, nil).Once()
	context := CreateMockCourseContext(mockAPI, &integerCourseId{Id: 1337, name: "name"})
	id, err := context.ModuleIdFromName("newmod", true)
	require.Nil(t, err, "module should have been created")
	require.Equal(t, id, rest.CanvasId(1234))
}

func TestCreateModuleItem(t *testing.T) {
	mockAPI := new(MockAPI)
	mockAPI.On("CreateModuleItem", rest.CanvasId(1337), rest.CanvasId(666),
		rest.ModuleItem{Title: "title", Type: "Quiz", ContentId: 777}).
		Return(
			rest.ModuleItem{
				Id:   rest.CanvasId(12),
				Type: "Quiz",
			},
			nil,
		).Once()
	context := CreateMockCourseContext(mockAPI, &integerCourseId{Id: 1337, name: "name"})

	item, err := context.AddModuleItem(rest.CanvasId(666), rest.CanvasId(777), "title", MODITEM_QUIZ)
	require.Nil(t, err)
	require.Equal(t, item, rest.CanvasId(12))

	mockAPI.On("CreateModuleItem", mock.Anything, mock.Anything, mock.Anything).Return(
		rest.ModuleItem{}, rest.NewAuthError(401, nil, true)).Once()
	_, err = context.AddModuleItem(rest.CanvasId(666), rest.CanvasId(777), "title", MODITEM_FILE)
	require.NotNil(t, err, "not authenticated -> should give an errror")
	require.Equal(t, err.Error(), "Error. Not authenticated. Is your token valid?")
	mockAPI.AssertExpectations(t)
}
