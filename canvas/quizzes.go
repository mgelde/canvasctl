/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020, 2021, 2022 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package canvas

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/mgelde/canvasctl/canvas/quiz_parsing"
	"gitlab.com/mgelde/canvasctl/rest"
	"gitlab.com/mgelde/canvasctl/rest/quizzes"
	"io"
	"os"
	"path/filepath"
	"strings"
)

func (ctx *CourseContext) ListQuizzes() ([]rest.Quiz, error) {
	if ctx == nil {
		panic("Context in ListFiles() is nil")
	}
	course := ctx.Course()
	id, err := course.AsId()
	if err != nil {
		return nil, err
	}
	return ctx.API.ListQuizzes(id)
}

func (ctx *CourseContext) DeleteQuiz(quizId rest.CanvasId) error {
	if ctx == nil {
		panic("Context in ListFiles() is nil")
	}
	course := ctx.Course()
	id, err := course.AsId()
	if err != nil {
		return err
	}
	err = ctx.API.DeleteQuiz(id, quizId)
	if err != nil {
		return err
	}
	return nil
}

func (ctx *CourseContext) createQuestion(question quiz_parsing.QuestionSpec, canvasGroupId rest.CanvasId) (quizzes.QuestionBuilder, error) {
	filenames := quiz_parsing.GetFilesFromDescription(question.Text)
	var err error = nil
	courseId, err := ctx.course.AsId()
	if err != nil {
		return nil, err
	}
	for _, embeddedFileSpec := range filenames.FileSpec {
		filename := embeddedFileSpec.Filename
		remotename := filepath.Base(filename)
		dirname := embeddedFileSpec.Dirname
		fileId, _, err := ctx.PushFile(filename, remotename, dirname)
		if err != nil {
			logrus.Errorf("Could not push file %s to directory %s", filename, dirname)
			filenames.AddReplacement(embeddedFileSpec, "ERROR DURING UPLOAD")
			continue
		}
		if strings.HasPrefix(embeddedFileSpec.MimeType, "image/") {
			if embeddedFileSpec.TypeSpecific != nil {
				imgSpecificSpec := embeddedFileSpec.TypeSpecific.(quiz_parsing.ImageSpecificData)
				filenames.AddReplacement(embeddedFileSpec, fmt.Sprintf(
					`<img src="/courses/%s/files/%s/preview" alt="%s" width="%s" height="%s" />`,
					courseId.ToBase10String(), fileId.ToBase10String(), remotename,
					imgSpecificSpec.DimX,
					imgSpecificSpec.DimY))
			} else {
				filenames.AddReplacement(embeddedFileSpec, fmt.Sprintf(
					`<img src="/courses/%s/files/%s/preview" alt="%s"/>`,
					courseId.ToBase10String(), fileId.ToBase10String(), remotename))
			}
		} else {
			filenames.AddReplacement(embeddedFileSpec, fmt.Sprintf(
				`<a class="instructure_file_link" title="%s" href="/courses/%s/files/%s/download?wrap=1">%s</a>`,
				remotename, courseId.ToBase10String(), fileId.ToBase10String(), remotename))
		}
	}
	modified_text := quiz_parsing.ReplaceLaTeXInDescription(filenames.ReplaceFilesInText(question.Text))

	qParams := quizzes.NewQuestion().
		SetTitle(question.Title).
		SetQuizGroup(canvasGroupId).
		AddDescription(modified_text)
	return qParams, err
}

func createMcQuestion(qParams quizzes.QuestionBuilder, api rest.API, courseId, quizId rest.CanvasId, question quiz_parsing.McQuestionSpec) error {
	answerBuilder := qParams.MakeMultipleChoiceQuestion()
	answerBuilder.AddAnswer(strings.TrimSpace(question.Correct), true)
	for _, falseAnswer := range question.Wrong {
		answerBuilder.AddAnswer(strings.TrimSpace(falseAnswer), false)
	}
	logrus.Infof("Creating question: %s (multiple choice)", question.Title)
	logrus.Debug("Params:", qParams.MakeParams())
	_, err := api.CreateQuestion(courseId, quizId, qParams.MakeParams())
	if err != nil {
		fmt.Fprintln(os.Stderr, "[!] Cannot create question:", question.Title)
		fmt.Fprintln(os.Stderr, "    Reason:", err)
	}
	return err
}

func createMultiAnswerQuestion(qParams quizzes.QuestionBuilder, api rest.API, courseId, quizId rest.CanvasId, question quiz_parsing.MultAnswersQuestionSpec) error {
	answerBuilder := qParams.MakeMultipleAnswerQuestion()
	for _, correctAnswer := range question.Correct {
		answerBuilder.AddAnswer(strings.TrimSpace(correctAnswer), true)
	}
	for _, falseAnswer := range question.Wrong {
		answerBuilder.AddAnswer(strings.TrimSpace(falseAnswer), false)
	}
	logrus.Infof("Creating question: %s (multiple answers)", question.Title)
	logrus.Debug("Params:", qParams.MakeParams())
	_, err := api.CreateQuestion(courseId, quizId, qParams.MakeParams())
	if err != nil {
		fmt.Fprintln(os.Stderr, "[!] Cannot create question:", question.Title)
		fmt.Fprintln(os.Stderr, "    Reason:", err)
	}
	return err
}

func createNumericQuestion(qParams quizzes.QuestionBuilder, api rest.API, courseId, quizId rest.CanvasId, question quiz_parsing.NumericQuestionSpec) error {
	for _, approximateAnswer := range question.Approximate {
		qParams.MakeNumericAnswer().AddAnswerApproximate(
			approximateAnswer.Answer, approximateAnswer.Precision)
	}
	for _, exactAnswer := range question.Exact {
		qParams.MakeNumericAnswer().AddAnswerExact(
			exactAnswer.Answer, exactAnswer.ErrorMargin)
	}
	for _, rangeAnswer := range question.Range {
		qParams.MakeNumericAnswer().AddAnswerRange(
			rangeAnswer.Begin, rangeAnswer.End)
	}
	logrus.Infof("Creating question: %s (numeric answers)", question.Title)
	logrus.Debug("Params:", qParams.MakeParams())
	_, err := api.CreateQuestion(courseId, quizId, qParams.MakeParams())
	if err != nil {
		fmt.Fprintln(os.Stderr, "[!] Cannot create question:", question.Title)
		fmt.Fprintln(os.Stderr, "    Reason:", err)
	}
	return err
}

func createShortAnswerQuestion(qParams quizzes.QuestionBuilder, api rest.API, courseId, quizId rest.CanvasId, question quiz_parsing.ShortAnswerQuestionSpec) error {
	builder := qParams.MakeShortAnswer()
	for _, answer := range question.Answers {
		builder.AddAnswer(strings.TrimSpace(answer))
	}
	logrus.Infof("Creating question: %s (short answer)", question.Title)
	logrus.Debug("Params:", qParams.MakeParams())
	_, err := api.CreateQuestion(courseId, quizId, qParams.MakeParams())
	if err != nil {
		fmt.Fprintln(os.Stderr, "[!] Cannot create question:", question.Title)
		fmt.Fprintln(os.Stderr, "    Reason:", err)
	}
	return err
}

func createFillBlankQuestion(qParams quizzes.QuestionBuilder, api rest.API, courseId, quizId rest.CanvasId, question quiz_parsing.FillBlanksQuestionSpec) error {
	builder := qParams.MakeMultipleBlankQuestion()
	for field, answerList := range question.Answers {
		for _, answer := range answerList {
			builder.AddAnswer(strings.TrimSpace(field), strings.TrimSpace(answer))
		}
	}
	logrus.Infof("Creating question: %s (fill multiple blanks)", question.Title)
	logrus.Debug("Params:", qParams.MakeParams())
	params, err := builder.ValidateAndReturn()
	if err != nil {
		return err
	}
	_, err = api.CreateQuestion(courseId, quizId, params)
	if err != nil {
		fmt.Fprintln(os.Stderr, "[!] Cannot create question:", question.Title)
		fmt.Fprintln(os.Stderr, "    Reason:", err)
	}
	return err
}

func createEssayQuestion(qParams quizzes.QuestionBuilder, api rest.API, courseId, quizId rest.CanvasId, question quiz_parsing.QuestionSpec) error {
	qParams.MakeEssayQuestion()
	logrus.Infof("Creating question: %s (essay)", question.Title)
	logrus.Debug("Params:", qParams.MakeParams())
	params := qParams.MakeParams()
	_, err := api.CreateQuestion(courseId, quizId, params)
	if err != nil {
		fmt.Fprintln(os.Stderr, "[!] Cannot create question:", question.Title)
		fmt.Fprintln(os.Stderr, "    Reason:", err)
	}
	return err
}

func createTextOnlyQuestion(qParams quizzes.QuestionBuilder, api rest.API, courseId, quizId rest.CanvasId, question quiz_parsing.QuestionSpec) error {
	qParams.MakeTextOnlyQuestion()
	logrus.Infof("Creating question: %s (text-only)", question.Title)
	logrus.Debug("Params:", qParams.MakeParams())
	params := qParams.MakeParams()
	_, err := api.CreateQuestion(courseId, quizId, params)
	if err != nil {
		fmt.Fprintln(os.Stderr, "[!] Cannot create question:", question.Title)
		fmt.Fprintln(os.Stderr, "    Reason:", err)
	}
	return err
}

func createFileUploadQuestion(qParams quizzes.QuestionBuilder, api rest.API, courseId, quizId rest.CanvasId, question quiz_parsing.QuestionSpec) error {
	qParams.MakeFileUploadQuestion()
	logrus.Infof("Creating question: %s (file upload)", question.Title)
	logrus.Debug("Params:", qParams.MakeParams())
	params := qParams.MakeParams()
	_, err := api.CreateQuestion(courseId, quizId, params)
	if err != nil {
		fmt.Fprintln(os.Stderr, "[!] Cannot create question:", question.Title)
		fmt.Fprintln(os.Stderr, "    Reason:", err)
	}
	return err
}

func (ctx *CourseContext) GetQuizObject(quizId rest.CanvasId) (rest.Quiz, error) {
	if ctx == nil {
		panic("Context in GetQuizObject is nil")
	}
	course := ctx.Course()
	courseId, err := course.AsId()
	if err != nil {
		return rest.Quiz{}, err
	}
	logrus.Infof("Getting quiz %d in course %d", quizId, courseId)
	return ctx.API.GetQuiz(courseId, quizId)
}

func (ctx *CourseContext) UpdateQuiz(quizId rest.CanvasId, quiz rest.QuizCreationSpec) error {
	if ctx == nil {
		panic("Context in GetQuizObject is nil")
	}
	course := ctx.Course()
	courseId, err := course.AsId()
	if err != nil {
		return err
	}
	logrus.Infof("Update quiz %d in course %d", quizId, courseId)
	return ctx.API.UpdateQuiz(courseId, quizId, quiz)
}

func (ctx *CourseContext) CreateQuizFromReader(reader io.Reader) error {
	if ctx == nil {
		panic("Context in CreateQuizFromReader is nil")
	}
	course := ctx.Course()
	id, err := course.AsId()
	if err != nil {
		return err
	}
	quizSpec, err := quiz_parsing.QuizSpecFromReader(reader)
	if err != nil {
		fmt.Fprintf(os.Stderr, "[!] Error. Cannot parse provided TOML file.")
		return err
	}

	fmt.Println("[+] Creating quiz:", quizSpec.Title)
	quiz, err := ctx.API.CreateQuiz(
		rest.QuizCreationSpec{
			Title:                         quizSpec.Title,
			QuizType:                      quizSpec.Kind,
			Text:                          quizSpec.Text,
			HideResults:                   quizSpec.HideResults,
			ShowCorrectAnswersAt:          quizSpec.ShowCorrectAnswersAt,
			HideCorrectAnswersAt:          quizSpec.HideCorrectAnswersAt,
			ScoringPolicy:                 quizSpec.ScoringPolicy,
			AccessCode:                    quizSpec.AccessCode,
			IPFilter:                      quizSpec.IPFilter,
			DueAt:                         quizSpec.DueAt,
			LockAt:                        quizSpec.LockAt,
			UnlockAt:                      quizSpec.UnlockAt,
			TimeLimit:                     quizSpec.TimeLimit,
			AllowedAttempts:               quizSpec.AllowedAttempts,
			ShuffleAnswers:                quizSpec.ShuffleAnswers,
			ShowCorrectAnswers:            quizSpec.ShowCorrectAnswers,
			ShowCorrectAnswersLastAttempt: quizSpec.ShowCorrectAnswersLastAttempt,
			OneQuestionAtATime:            quizSpec.OneQuestionAtATime,
			CantGoBack:                    quizSpec.CantGoBack,
			Published:                     quizSpec.Published,
			OneTimeResults:                quizSpec.OneTimeResults,
			AssignmentGroupId:             quizSpec.AssignmentGroupId,
		},
		id)
	if err != nil {
		fmt.Fprintln(os.Stderr, "[!] Could not create quiz!")
		return err
	}
	for _, groupSpec := range quizSpec.Groups {
		fmt.Println("[+] Creating group:", groupSpec.Name)
		canvasGroup, err := ctx.API.CreateQuizQuestionGroup(id, quiz.Id, groupSpec.Name,
			groupSpec.PickCount, groupSpec.Points)
		if err != nil {
			fmt.Fprintln(os.Stderr, "[!] Could not create question group:", groupSpec.Name)
			fmt.Fprintln(os.Stderr, "    Reason:", err)
			continue
		}
		for _, question := range groupSpec.McQuestions {
			fmt.Println(" -> Creating MC question:", question.Title)
			qParams, err := ctx.createQuestion(question.QuestionSpec, canvasGroup.Id)
			if err != nil {
				fmt.Fprintln(os.Stderr, "    [!] Error creating question:", question.Title)
				continue
			}
			err = createMcQuestion(qParams, ctx.API, id, quiz.Id, question)
		}

		for _, question := range groupSpec.MultiAnswerQuestions {
			fmt.Println(" -> Creating multi-answer question:", question.Title)
			qParams, err := ctx.createQuestion(question.QuestionSpec, canvasGroup.Id)
			if err != nil {
				fmt.Fprintln(os.Stderr, "    [!] Error creating question:", question.Title)
				continue
			}
			err = createMultiAnswerQuestion(qParams, ctx.API, id, quiz.Id, question)
		}
		for _, question := range groupSpec.NumericQuestions {
			fmt.Println(" -> Creating numeric question:", question.Title)
			qParams, err := ctx.createQuestion(question.QuestionSpec, canvasGroup.Id)
			if err != nil {
				fmt.Fprintln(os.Stderr, "    [!] Error creating question:", question.Title)
				continue
			}
			err = createNumericQuestion(qParams, ctx.API, id, quiz.Id, question)
		}
		for _, question := range groupSpec.ShortAnswerQuestions {
			fmt.Println(" -> Creating short answer question:", question.Title)
			qParams, err := ctx.createQuestion(question.QuestionSpec, canvasGroup.Id)
			if err != nil {
				fmt.Fprintln(os.Stderr, "    [!] Error creating question:", question.Title)
				continue
			}
			err = createShortAnswerQuestion(qParams, ctx.API, id, quiz.Id, question)
		}
		for _, question := range groupSpec.FillBlanksQuestions {
			fmt.Println(" -> Creating fill blank question:", question.Title)
			qParams, err := ctx.createQuestion(question.QuestionSpec, canvasGroup.Id)
			if err != nil {
				fmt.Fprintln(os.Stderr, "    [!] Error creating question:", question.Title)
				continue
			}
			err = createFillBlankQuestion(qParams, ctx.API, id, quiz.Id, question)
		}
		for _, question := range groupSpec.EssayQuestions {
			fmt.Println(" -> Creating essay question:", question.Title)
			qParams, err := ctx.createQuestion(question, canvasGroup.Id)
			if err != nil {
				fmt.Fprintln(os.Stderr, "    [!] Error creating question:", question.Title)
				continue
			}
			err = createEssayQuestion(qParams, ctx.API, id, quiz.Id, question)
		}
		for _, question := range groupSpec.TextOnlyQuestions {
			fmt.Println(" -> Creating text-only question:", question.Title)
			qParams, err := ctx.createQuestion(question.QuestionSpec, canvasGroup.Id)
			if err != nil {
				fmt.Fprintln(os.Stderr, "    [!] Error creating question:", question.Title)
				continue
			}
			err = createTextOnlyQuestion(qParams, ctx.API, id, quiz.Id, question.QuestionSpec)
		}
		for _, question := range groupSpec.FileUploadQuestions {
			fmt.Println(" -> Creating file upload question:", question.Title)
			qParams, err := ctx.createQuestion(question, canvasGroup.Id)
			if err != nil {
				fmt.Fprintln(os.Stderr, "    [!] Error creating question:", question.Title)
				continue
			}
			err = createFileUploadQuestion(qParams, ctx.API, id, quiz.Id, question)
		}
	}
	if err != nil {
		return fmt.Errorf("There were errors. Please check the result on Canvas matches your exepctations.")
	}
	return nil
}
