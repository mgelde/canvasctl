/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package quiz_parsing

const SampleTomlFile string = `
Title =  "My Quiz"
Text = "This is a quiz"
Kind = "practice_quiz"
ScoringPolicy = "keep_highest"
TimeLimit = 90
AllowedAttempts = 2
ShowCorrectAnswers = true
AssignmentGroupId = 42

[[Groups]]
Name= "Group 1"
PickCount = 1
Points = 1.2

[[Groups.McQuestions]]
Title = "Question 1"
Text = "This is the first question"
Correct = "this one is correct"
Wrong = ["these", "are", "all", "false"]

[[Groups.McQuestions]]
Title = "Question 2"
Text = "This is the second question"
Correct = "1337"
Wrong = ["1", "22", "42", "1338"]

[[Groups.MultiAnswerQuestions]]
Title = "Question 3"
Text = "This is  another question"
Correct = ["correct", "also correct"]
Wrong = ["these", "are", "all", "false"]

[[Groups.ShortAnswerQuestions]]
Title= "Question 4"
Text = "This is a short answer questions"
Answers = ["one", "two", "three"]

[[Groups.FillBlanksQuestions]]
Title = "Bla"
Text = "This is [a]. There are [b] [c]."
[Groups.FillBlanksQuestions.Answers]
a = ["question"]
b = ["three"]
c = ["blank fields", "blanks"]

[[Groups.NumericQuestions]]
Title = "Q1"
Text = "This is numeric"
[[Groups.NumericQuestions.Approximate]]
Answer = 12.123
Precision = 5
[[Groups.NumericQuestions.Range]]
Begin = 0.03
End = 1.1
[[Groups.NumericQuestions.Exact]]
Answer = 1.0
ErrorMargin = 0.01

[[Groups.FileUploadQuestions]]
Title = "Upload"
Text = "Use this to upload files."

[[Groups.EssayQuestions]]
Title = "Essay"
Text = "Use this to write long texts."
`
