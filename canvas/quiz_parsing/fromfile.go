/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package quiz_parsing

import (
	"fmt"
	"io"
	"net/url"
	"regexp"
	"strings"

	"github.com/BurntSushi/toml"
	"github.com/sirupsen/logrus"
	"gitlab.com/mgelde/canvasctl/rest"
)

// Common properties. To be embedded
type QuestionSpec struct {
	Title string
	Text  string
}

type McQuestionSpec struct {
	QuestionSpec
	Correct string
	Wrong   []string
}

type TextOnlySpec struct {
	QuestionSpec
}

type MultAnswersQuestionSpec struct {
	QuestionSpec
	Correct []string
	Wrong   []string
}

type ExactAnswer struct {
	Answer      float64
	ErrorMargin float64
}

type RangeAnswer struct {
	Begin float64
	End   float64
}

type ApproximateAnswer struct {
	Answer    float64
	Precision int64
}

type NumericQuestionSpec struct {
	QuestionSpec
	Exact       []ExactAnswer
	Range       []RangeAnswer
	Approximate []ApproximateAnswer
}

type ExactAnswerQuestionSpec struct {
	QuestionSpec
	Answers []string
}

type ShortAnswerQuestionSpec struct {
	QuestionSpec
	Answers []string
}

type FillBlanksQuestionSpec struct {
	QuestionSpec
	Answers map[string][]string
}

type QuestionGroupSpec struct {
	Name                 string
	PickCount            int
	Points               float64
	McQuestions          []McQuestionSpec
	MultiAnswerQuestions []MultAnswersQuestionSpec
	NumericQuestions     []NumericQuestionSpec
	ExactAnswerQuestions []ExactAnswerQuestionSpec
	ShortAnswerQuestions []ShortAnswerQuestionSpec
	FillBlanksQuestions  []FillBlanksQuestionSpec
	FileUploadQuestions  []QuestionSpec
	EssayQuestions       []QuestionSpec
	TextOnlyQuestions    []TextOnlySpec
}

type QuizSpec struct {
	Title                         string
	Kind                          *string
	Text                          *string
	HideResults                   *string
	ShowCorrectAnswersAt          *string
	HideCorrectAnswersAt          *string
	ScoringPolicy                 *string
	AccessCode                    *string
	IPFilter                      *string
	DueAt                         *string
	LockAt                        *string
	UnlockAt                      *string
	TimeLimit                     *int
	AllowedAttempts               *int
	ShuffleAnswers                *bool
	ShowCorrectAnswers            *bool
	ShowCorrectAnswersLastAttempt *bool
	OneQuestionAtATime            *bool
	CantGoBack                    *bool
	Published                     *bool
	OneTimeResults                *bool
	AssignmentGroupId             *rest.CanvasId

	Groups []QuestionGroupSpec
}

func QuizSpecFromReader(reader io.Reader) (QuizSpec, error) {
	var quizSpec QuizSpec
	if _, err := toml.DecodeReader(reader, &quizSpec); err != nil {
		return QuizSpec{}, err
	}
	return quizSpec, nil
}

type EmbeddedFileSpec struct {
	Filename     string
	Dirname      string
	MimeType     string
	TypeSpecific interface{}
	key          string
}

type ImageSpecificData struct {
	DimX string
	DimY string
}

type EmbeddedFilesContext struct {
	FileSpec     []EmbeddedFileSpec
	replacements map[string]string
}

func (context EmbeddedFilesContext) ReplaceFilesInText(text string) string {
	current := text
	for key, value := range context.replacements {
		re := regexp.MustCompile(regexp.QuoteMeta(key))
		current = re.ReplaceAllString(current, value)
	}
	logrus.Debug("Text after replacing:", current)
	return current
}

func (context EmbeddedFilesContext) AddReplacement(spec EmbeddedFileSpec, replacement string) {
	context.replacements[spec.key] = replacement
}

func specificForMimeType(mimeType, value string) (interface{}, error) {
	var returnValue interface{} = nil
	switch {
	case strings.HasPrefix(mimeType, "image/"):
		exp := regexp.MustCompile(`x=\((\d+?)\),y=\((\d+?)\)`)
		if submatch := exp.FindStringSubmatch(value); submatch != nil {
			returnValue = ImageSpecificData{
				DimX: submatch[1],
				DimY: submatch[2],
			}
		}
	default:
		logrus.Errorf("Unknown mime type: %s. Do not have any type-specific data for this type.",
			mimeType)
	}
	if returnValue == nil {
		return nil, fmt.Errorf(
			"Could not parse mime-type specific data. mime-type=(%s), data=(%s)",
			mimeType, value)
	}
	return returnValue, nil
}

func ReplaceLaTeXInDescription(text string) string {
	re := regexp.MustCompile(`{%tex\w?(.*?)%}`)
	return re.ReplaceAllStringFunc(text, func(match string) string {
		submatch := strings.TrimSpace(re.FindStringSubmatch(match)[1])
		/*
		 * This is a bit of a hack, because Go does not respect RFC 3986 and encodes spaces to '+'
		 * instead of '%20'. Hence we replace all those spaces by %20 after the innermost encoding.
		 * This should be sane, because any occurrence of '+' must have been a space prior to
		 * encoding and because %20 is is treated correctly by Go's encoding scheme.
		 */
		return fmt.Sprintf(`<img class="equation_image" title="%s" src="/equation_images/%s" alt="LaTeX: %s" data-equation-content="%s"/>`,
			submatch, url.QueryEscape(strings.ReplaceAll(url.QueryEscape(submatch), "+", "%20")), submatch, submatch)
	})
}

func GetFilesFromDescription(text string) EmbeddedFilesContext {
	filespec := EmbeddedFilesContext{
		FileSpec:     []EmbeddedFileSpec{},
		replacements: map[string]string{},
	}
	re := regexp.MustCompile(`{%mime=\((.*?)\),filename=\((.*?)\)(,folder=\((.*?)\))?(,type-specific=\((.*?)\))?%}`)
	matches := re.FindAllStringSubmatch(text, -1)
	for _, submatches := range matches {
		spec := EmbeddedFileSpec{}
		spec.Dirname = "/"
		spec.key = submatches[0]
		spec.MimeType = submatches[1]
		spec.Filename = submatches[2]
		if submatches[4] != "" {
			spec.Dirname = submatches[4]
		} else {
			spec.Dirname = "/"
		}
		if submatches[6] != "" {
			if specificData, err := specificForMimeType(spec.MimeType, submatches[6]); err == nil {
				spec.TypeSpecific = specificData
			} else {
				logrus.Warnf("Could not parse mime-type-specific data: %s", spec.MimeType)
				logrus.Warnf("Error was: %s", err)
			}
		} else {
			spec.TypeSpecific = nil
		}
		filespec.FileSpec = append(filespec.FileSpec, spec)
		logrus.Debugf("Added spec %v", filespec.FileSpec)
	}
	return filespec
}
