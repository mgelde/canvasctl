/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020, 2021, 2022 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package cmdline

import (
	"bytes"
	"fmt"
	"os"
	"strings"
	"syscall"

	"github.com/99designs/keyring"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/mgelde/canvasctl/config"
	"golang.org/x/crypto/ssh/terminal"
)

func ImportToken(ctx *cli.Context) error {
	if ctx.NArg() < 1 {
		return cli.Exit("Not enough arguments. I need a URL to match this token to.", 1)
	}
	url := strings.TrimSpace(ctx.Args().First())
	conf, err := config.LoadConfig()
	if err != nil {
		return cli.Exit(err, 1)
	}
	var token string = ""
	if site, err := conf.SiteByUrl(url); err == nil {
		if site.Token != "" {
			fmt.Println("[+] Site already has a token. I will wrap that token. Your canvas.conf will NOT be modified.")
			token = site.Token
		}
	}
	if token == "" {
		fmt.Print("[+] Please provide the token: ")
		tokenBytes, err := terminal.ReadPassword(int(syscall.Stdin))
		if err != nil {
			logrus.Errorf("Error readin password: %s", err)
			return cli.Exit("Could not read password", 1)
		}
		fmt.Println()
		token = strings.TrimSpace(string(tokenBytes))
	}
	if ctx.Bool("use-keyring") {
		if config.IsEncrypted(token) {
			token, err = config.DecryptTokenForUrl(token, url)
			if err != nil {
				logrus.Errorf("Could not decrypt token: %s", err)
				return cli.Exit("Could not descrypt token", 1)
			}
		}
		ring, err := keyring.Open(keyring.Config{
			// TODO: Make this configurable
			ServiceName:             "Default_keyring",
			LibSecretCollectionName: "Default_keyring",
		})
		if err != nil {
			return cli.Exit(err, 1)
		}
		if err = ring.Set(keyring.Item{
			Data:        []byte(token),
			Key:         url,
			Label:       "Canvas Token",
			Description: "A token to access the canvas API",
		}); err != nil {
			logrus.Error(err)
			return cli.Exit("Could not wrap token", 1)
		}
		fmt.Println("Imported the token into your keyring. You can now safely delete the token from your canvas.conf, if applicable.")
	} else {
		if config.IsEncrypted(token) {
			return cli.Exit("The token is already encrypted. Did you mean to use --use-keyring?", 1)
		}
		fmt.Print("Please provide a password: ")
		pwdBytes, err := terminal.ReadPassword(int(syscall.Stdin))
		if err != nil {
			return err
		}
		pwdBytes = bytes.TrimSpace(pwdBytes)
		fmt.Print("\nPlease repeat your password: ")
		pwdBytes2, err := terminal.ReadPassword(int(syscall.Stdin))
		fmt.Println()
		if err != nil {
			return err
		}
		pwdBytes2 = bytes.TrimSpace(pwdBytes2)

		if !bytes.Equal(pwdBytes, pwdBytes2) {
			fmt.Fprintf(os.Stderr, "Error. Passwords do not match")
			return cli.Exit("Cannot import token", 1)
		}

		ciphertext, err := config.EncryptToken(token, url, pwdBytes)

		fmt.Println("Place this in your canvas.conf file:")
		fmt.Printf("Token = \"%s\"\n", config.EncodeToken(ciphertext.Ciphertext, ciphertext.Nonce, ciphertext.Salt))
	}
	return nil
}

func ExportToken(ctx *cli.Context) error {
	if ctx.NArg() < 1 {
		return cli.Exit("Not enough arguments. I need a URL to match this token to.", 1)
	}
	url := strings.TrimSpace(ctx.Args().First())
	conf, err := config.LoadConfig()
	if err != nil {
		return cli.Exit(err, 1)
	}
	var token string
	if site, err := conf.SiteByUrl(url); err == nil {
		if site.Token == "" {
			fmt.Printf("[!] Site %s has no token.\n", url)
			return cli.Exit("Cannot unwrap token, because there was none.", 1)
		}
		token = site.Token
	}

	plaintext, err := config.DecryptTokenForUrl(token, url)
	if err != nil {
		logrus.Debug("Error during decryption", err)
		return cli.Exit("Could not decrypt token", 1)
	}

	fmt.Println("Place this in your canvas.conf file:")
	fmt.Printf("Token = \"%s\"\n", plaintext)
	return nil
}
