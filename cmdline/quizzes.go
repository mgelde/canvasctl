/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020, 2022 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package cmdline

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"unicode/utf8"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/mgelde/canvasctl/canvas"
	"gitlab.com/mgelde/canvasctl/rest"
)

type PrintFunc func(quizzes []rest.Quiz)

func shortPrintFunc(quizzes []rest.Quiz) {
	width := 0
	for _, quiz := range quizzes {
		if width < utf8.RuneCountInString(quiz.Title) {
			width = utf8.RuneCountInString(quiz.Title)
		}
	}
	header := fmt.Sprintf("ID        %-*s    Published", width, "Title")
	fmt.Println(header)
	fmt.Println(strings.Repeat("=", utf8.RuneCountInString(header)))
	for _, quiz := range quizzes {
		fmt.Printf("%-6d    %-*s    %v\n", quiz.Id, width, quiz.Title, quiz.Published)
	}
}

func fullPrintFunc(quizzes []rest.Quiz) {
	for _, quiz := range quizzes {
		fmt.Printf("%s\n", quiz.Title)
		fmt.Println(strings.Repeat("-", utf8.RuneCountInString(quiz.Title)))
		fmt.Printf(" - Id: %d\n", quiz.Id)
		fmt.Printf(" - Pulished: %v\n", quiz.Published)
		fmt.Printf(" - Type: %v\n", quiz.QuizType)
		fmt.Printf(" - Due at: %v\n", quiz.DueAt)
		fmt.Printf(" - Unlock at: %v\n", quiz.UnlockAt)
		fmt.Printf(" - Lock at: %v\n", quiz.LockAt)
	}
}

func getQuizPrinter(format string) PrintFunc {
	switch format {
	case "short":
		return shortPrintFunc
	case "full":
		return fullPrintFunc
	case "": // no format was specified
		return shortPrintFunc
	default:
		fmt.Printf("[!] I do not know the format '%s' and I am ignoring it\n", format)
		logrus.Infof("Using default for unknown format '%s'", format)
		return shortPrintFunc
	}
}

func CommandQuizzesList(c *cli.Context) error {
	ctx, err := canvas.NewCourseContext(c)
	if err != nil {
		return cli.Exit(err, 1)
	}
	course := ctx.Course()
	if err != nil {
		return cli.Exit(err, 1)
	}
	if c.Bool("quiet") && c.IsSet("format") {
		return cli.Exit(fmt.Errorf("Cannot specify --format and --quit at the same time."), 1)
	}

	var printer PrintFunc = nil
	if c.Bool("quiet") {
		printer = func(quizzes []rest.Quiz) {
			for _, quiz := range quizzes {
				fmt.Println(quiz.Id.ToBase10String())
			}
		}
	} else {
		printer = getQuizPrinter(c.String("format"))
	}

	quizzes, err := ctx.ListQuizzes()
	if err != nil {
		return cli.Exit(err, 1)
	}

	if !c.Bool("quiet") {
		fmt.Println("[+] Quizzes in course:", course.AsName())
	}
	printer(quizzes)

	return nil
}

func CommandCreateQuizStdin(c *cli.Context) error {
	ctx, err := canvas.NewCourseContext(c)
	if err != nil {
		return cli.Exit(err, 1)
	}
	fmt.Println("Creating quiz from STDIN")
	if err = ctx.CreateQuizFromReader(os.Stdin); err != nil {
		return cli.Exit(err, 1)
	}
	return nil
}

func CommandDeleteQuiz(c *cli.Context) error {
	ctx, err := canvas.NewCourseContext(c)
	if err != nil {
		return cli.Exit(err, 1)
	}
	fmt.Println("[+] Deleting quizzes:")
	for _, idStr := range c.Args().Slice() {
		quizId, err := strconv.ParseInt(idStr, 10, 64)
		if err != nil {
			return cli.Exit(fmt.Errorf("Cannot parse '%s' into an integer", idStr), 1)
		}
		fmt.Printf("  - Quiz: %d", quizId)
		if err = ctx.DeleteQuiz(rest.CanvasId(quizId)); err != nil {
			logrus.Errorf("Could not delete %d: %v", quizId, err)
			fmt.Println("\t[FAILED]")
		} else {
			fmt.Println("\t[DONE]")
		}
	}
	return nil
}

func CommandUpdateQuiz(c *cli.Context) error {
	ctx, err := canvas.NewCourseContext(c)
	if err != nil {
		return cli.Exit(err, 1)
	}
	fmt.Println("[+] Updating quiz:")
	idStr := c.Args().First()
	id, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		return cli.Exit(fmt.Errorf("Cannot parse '%s' into an integer", idStr), 1)
	}
	quiz := rest.QuizCreationSpec{}
	if c.IsSet("publish") {
		fmt.Printf("  - Setting published to %v\n", c.Bool("publish"))
		publish := c.Bool("publish")
		quiz.Published = &publish
	}
	if c.IsSet("title") {
		fmt.Printf("  - Setting title to %s\n", c.String("title"))
		quiz.Title = c.String("title")
	}
	if c.IsSet("lock-at") {
		fmt.Printf("  - Setting lock-at to %s\n", c.String("lock-at"))
		lockAt := c.String("lock-at")
		quiz.LockAt = &lockAt
	}
	if c.IsSet("due-at") {
		fmt.Printf("  - Setting due-at to %s\n", c.String("due-at"))
		dueAt := c.String("due-at")
		quiz.DueAt = &dueAt
	}
	if c.IsSet("unlock-at") {
		fmt.Printf("  - Setting unlock-at to %s\n", c.String("unlock-at"))
		unlockAt := c.String("unlock-at")
		quiz.UnlockAt = &unlockAt
	}
	err = ctx.UpdateQuiz(rest.CanvasId(id), quiz)
	if err != nil {
		return cli.Exit(err, 1)
	}
	return nil
}
