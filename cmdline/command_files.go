/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package cmdline

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/mgelde/canvasctl/canvas"
	"gitlab.com/mgelde/canvasctl/rest"
)

func CommandFilesList(c *cli.Context) error {
	ctx, err := canvas.NewCourseContext(c)
	if err != nil {
		return cli.Exit(err, 1)
	}

	if c.Bool("full") {
		if c.Args().Len() > 0 {
			fmt.Println("[!] Cannot use arguments with --full. Ignoring...")
		}
		if c.Bool("tree") {
			// Progress bar
			fmt.Print("[+] Getting data from server ")
			secondList := false
			ctx.API.SetProgressCallback(func(context rest.ProgressCallbackContext) {
				switch {
				case context == rest.ProgresStart || context == rest.ProgressContinue:
					fmt.Print(".")
				case context == rest.ProgressStop:
					if secondList {
						fmt.Println(". [DONE]")
					} else {
						secondList = true
					}
				}
			})

			tree, dangling, err := ctx.BuildCourseDirTree()
			if err != nil {
				return cli.Exit(err, 1)
			}
			fmt.Printf("/%s\n", tree.Name())
			drawRecursive("", tree)

			if dangling != nil && len(dangling) != 0 {
				fmt.Println("\n\n[!] In additon, the following dangling folders were found:")
				for _, dt := range dangling {
					fmt.Printf("   Folder: %s (parent ID = %d)\n", dt.Name(), dt.FolderInfo().ParentFolderId)
					for _, file := range dt.Files() {
						fmt.Printf("    - %s (%s)\n", file.DisplayName, file.ContentType)
					}
				}
			}

			return nil
		} else {
			if err := ctx.ListFiles(); err != nil {
				return cli.Exit(err, 1)
			}
		}
		return nil
	}
	if c.Bool("tree") {
		fmt.Println("[!] Error. We do not support tree output for specific folders at this point: --tree requires --full")
		return cli.Exit("Error. Illegal flag combination.", 1)
	}
	var path string
	switch c.NArg() {
	case 0:
		path = "/"
	case 1:
		path = c.Args().First()
	default:
		return cli.Exit("[!] Too many arguments. Expected at most one path.", 1)
	}

	resolved, err := ctx.ResolvePath(path)
	if err != nil {
		fmt.Printf("[!] Could not resolve folder %s\n", path)
		return cli.Exit(err, 1)
	}
	if resolved.Filename == "" { // path refers to a folder
		for _, file := range resolved.Files {
			fmt.Printf("- %s (%s)\n", file.DisplayName, file.ContentType)
		}
		for _, folder := range resolved.Folders {
			fmt.Printf("[DIR] %s", folder.Name)
		}
	} else {
		fmt.Printf("%s (%s)\n", resolved.Files[0].DisplayName, resolved.Files[0])
		fmt.Printf(" - Size: %d\n", resolved.Files[0].Size)
		fmt.Printf(" - Filename: %s\n", resolved.Files[0].Filename)
		fmt.Printf(" - Published: %t\n", !resolved.Files[0].Hidden)
		fmt.Printf(" - URL: %s\n", resolved.Files[0].Url)
	}
	return nil
}

func CommandFilePush(c *cli.Context) error {
	var ret error = nil
	ctx, err := canvas.NewCourseContext(c)
	if err != nil {
		return cli.Exit(err, 1)
	}
	if c.NArg() <= 0 {
		fmt.Println("Usage:", c.Command.Name, c.Command.ArgsUsage)
		return cli.Exit("Not enough arguments.", 1)
	}

	var moduleId rest.CanvasId = 0
	haveModule := false
	if c.IsSet("module") {
		name := c.String("module")
		if moduleId, err = ctx.ModuleIdFromName(name, true); err != nil {
			moduleId = 0
			logrus.Error(err)
			fmt.Fprintf(os.Stderr, "Cannot find or create module '%s'. Reason: %s\n", name, err)
		} else {
			haveModule = true
			logrus.Infof("Have module with id %d", moduleId)
		}
	}
	var maxLength int = 0
	for i := 0; i < c.NArg(); i++ {
		if len(c.Args().Get(i)) > maxLength {
			maxLength = len(c.Args().Get(i))
		}
	}
	fmt.Println("Pushing file:")
	for i := 0; i < c.NArg(); i++ {
		localname := c.Args().Get(i)
		remotename := filepath.Base(localname)
		remotepath := c.String("path")

		fmt.Printf(" - %-*s   ", maxLength, localname)
		logrus.Infof("localname=%s, remotename=%s, path=%s", localname, remotename, remotepath)
		fileId, url, err := ctx.PushFile(localname, remotename, remotepath)
		if err != nil {
			fmt.Println("[FAILED]")
			ret = cli.Exit(err, 1)
			continue
		} else {
			fmt.Println("[OK]")
			if c.Bool("verbose") {
				fmt.Println("   ", url)
			}
		}

		logrus.Debugf("File has Id %d and Url %s", fileId, url)
		if haveModule {
			if item, ok := ctx.FindModuleItem(moduleId, func(m rest.ModuleItem) bool {
				if m.ContentId == fileId {
					return true
				} else {
					return false
				}
			}); ok {
				logrus.Infof("Found a module item with file ID %d, namely item ID %d", fileId, item.Id)
			} else {
				logrus.Infof("Currently no module item with file ID %d. Creating one", fileId)
				if _, err = ctx.AddModuleItem(moduleId, fileId, remotename, canvas.MODITEM_FILE); err != nil {
					logrus.Warnf("Got error: %v", err)
					ret = cli.Exit(err, 1)
					fmt.Fprintln(os.Stderr, "Could not add file to module:", err)
				}
			}
		}
		if c.IsSet("publish") {
			if !c.Bool("publish") {
				logrus.Info("Unpublishing file...")
				err := ctx.PublishFile(fileId, false)
				logrus.Debugf("Done unpublishing. Error code: %v", err)
				if err != nil {
					logrus.Error(err)
					ret = cli.Exit(err, 1)
					fmt.Fprintln(os.Stderr, "Error unpublishing file")
				}
			} else {
				logrus.Info("Publishing file...")
				err := ctx.PublishFile(fileId, true)
				logrus.Debugf("Done publishing. Error code: %v", err)
				if err != nil {
					logrus.Error(err)
					ret = cli.Exit(err, 1)
					fmt.Fprintln(os.Stderr, "Error publishing file")
				}
			}
		}
	}
	return ret
}

func drawRecursive(prefix string, node canvas.DirectoryTree) {
	if node == nil {
		return
	}

	files := node.Files()
	children := node.Children()

	if len(files) > 0 {
		fmt.Printf("%s    |\n", prefix)
		for i, file := range files {
			if i < len(files)-1 || len(children) > 0 {
				fmt.Printf("%s    |-- %s (%s)\n", prefix, file.DisplayName, file.ContentType)
			} else {
				fmt.Printf("%s    +-- %s (%s)\n", prefix, file.DisplayName, file.ContentType)
			}
		}
	}
	for i, folder := range children {
		var newPrefix string
		fmt.Printf("%s    |\n", prefix)
		if i < len(children)-1 {
			newPrefix = prefix + "    |"
			fmt.Printf("%s    |-- /%s\n", prefix, folder.Name())
		} else {
			newPrefix = prefix + "     "
			fmt.Printf("%s    +-- /%s\n", prefix, folder.Name())
		}
		drawRecursive(newPrefix, folder)
	}
}
