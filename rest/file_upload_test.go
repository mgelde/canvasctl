/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"bytes"
	"encoding/json"
	"github.com/stretchr/testify/require"
	"gitlab.com/mgelde/canvasctl/testutils"
	"io/ioutil"
	"net/http"
	"strconv"
	"testing"
)

type fileUploadMockParams struct {
	Spec            PushFileSpec
	T               *testing.T
	ExpectedContent []byte
}

func fileUploadMockHandler(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, arg interface{}) {
	params := arg.(fileUploadMockParams)
	t := params.T
	switch {
	case isFirstRequest(req):
		if req.Header.Get("Authorization") == "" {
			t.Fatal("No authentication")
		}
		req.ParseMultipartForm(1024)
		expectedParts := map[string][]string{
			"name":               {params.Spec.RemoteFilename},
			"size":               {strconv.FormatUint(uint64(params.Spec.FileSize), 10)},
			"parent_folder_path": {params.Spec.RemotePath},
		}
		for key, val := range expectedParts {
			for i := range req.MultipartForm.Value[key] {
				if val[i] != req.MultipartForm.Value[key][i] {
					t.Fatalf("key %s got %s and %s.", key, val, req.MultipartForm.Value[key])
				}
			}
		}
		w.WriteHeader(200)
		w.Write([]byte(`{
				"upload_url": "` + m.Server.URL + `/bla",
				"upload_params": {
				"floop": "floop1",
				"bloop": "bloop2"
			  }}`))
	case isSecondRequest(req):
		if req.Header.Get("Authorization") != "" {
			t.Fatal("Must not send token in second request")
		}
		req.ParseMultipartForm(1024)
		expectedParts := map[string][]string{
			"floop": {"floop1"},
			"bloop": {"bloop2"},
		}
		for key, val := range expectedParts {
			for i := range req.MultipartForm.Value[key] {
				if val[i] != req.MultipartForm.Value[key][i] {
					t.Fatalf("key %s got %s and %s.", key, val, req.MultipartForm.Value[key])
				}
			}
		}
		filePart := req.MultipartForm.File["file"][0]
		require.Equal(t, filePart.Filename, params.Spec.RemoteFilename)
		require.Equal(t, filePart.Size, int64(len(params.ExpectedContent)))
		if file, err := filePart.Open(); err != nil {
			t.Fatal(err)
		} else {
			received, _ := ioutil.ReadAll(file)
			if !bytes.Equal(received, params.ExpectedContent) {
				t.Fatal("Received unexpected file content")
			}
		}

		w.Header().Add("Location", m.Server.URL+"/foo")
		w.WriteHeader(308) // any 3XX should be fine
	case isThirdRequest(req):
		if req.Header.Get("Authorization") == "" {
			t.Fatal("No authentication")
		}
		uploadReposne := FileUploadResponse{
			Id:          12,
			DisplayName: "displayname",
			Size:        666,
			ContentType: "image/jpeg",
			Url:         "someurl",
		}
		if jsonbytes, err := json.Marshal(&uploadReposne); err == nil {
			w.Write(jsonbytes)
		}
	default:
		t.Fatalf("Unexpected request: %s for %s", req.Method, req.URL.String())
	}
}

func createPushFileSpec() (PushFileSpec, []byte) {
	expected := []byte("some file content")
	reader := bytes.NewReader(expected)
	spec := PushFileSpec{
		File:           reader,
		FileSize:       uint64(len(expected)),
		RemoteFilename: "remoteName",
		RemotePath:     "remotePath",
	}
	return spec, expected
}

func isFirstRequest(req *http.Request) bool {
	if req.Method != "POST" || req.URL.Path != "/api/v1/courses/12/files" {
		return false
	}
	return true
}

func isSecondRequest(req *http.Request) bool {
	if req.Method != "POST" || req.URL.Path != "/bla" {
		return false
	}
	return true
}

func isThirdRequest(req *http.Request) bool {
	if req.Method != "GET" || req.URL.Path != "/foo" {
		return false
	}
	return true
}

func TestFileUpload(t *testing.T) {
	spec, expectedContent := createPushFileSpec()
	params := fileUploadMockParams{
		T:               t,
		ExpectedContent: expectedContent,
		Spec:            spec,
	}
	mock := testutils.NewMockCanvasServer(fileUploadMockHandler, params)
	defer mock.Server.Close()
	api, _ := NewAPI(mock.Server.URL, "token")
	resp, err := api.PushFile(CanvasId(12), spec)
	if err != nil {
		t.Fatal(err)
	}
	require.Equal(t, resp.Id, CanvasId(12))
	require.Equal(t, resp.Size, int64(666))
	require.Equal(t, resp.Url, "someurl")
	require.Equal(t, resp.DisplayName, "displayname")
	require.Equal(t, resp.ContentType, "image/jpeg")
}

func TestConfirmUpload(t *testing.T) {
	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, _ interface{}) {
			require.Equal(t, req.Header.Get("Authorization"), "Bearer token")
			require.Equal(t, req.URL.String(), "/fooo/mooo")
			w.Write([]byte(`{"id":1337, "url":"bla", "content-type":"text/plain", "size": 666}`))
		}, nil)
	defer mock.Server.Close()
	api, _ := NewAPI(mock.Server.URL, "token")
	resp := http.Response{
		StatusCode: 400,
		Body:       nil,
	}
	if _, err := confirmFileUpload(api.(*ApiImpl), &resp); err == nil {
		t.Fatal("Status 400 should produce an error")
	}
	resp.StatusCode = 201
	resp.Header = http.Header{"Location": {mock.Server.URL + "/fooo/mooo"}}
	fuploadResp, err := confirmFileUpload(api.(*ApiImpl), &resp)
	if err != nil {
		t.Fatal("201 should not produce an error", err)
	}
	require.Equal(t, fuploadResp.Id, CanvasId(1337))
	require.Equal(t, fuploadResp.Size, int64(666))
	require.Equal(t, fuploadResp.ContentType, "text/plain")
}
