/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020, 2022 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	"net/http"
	"net/url"
	"strconv"
)

func (a *ApiImpl) ListQuizzes(id CanvasId) ([]Quiz, error) {
	logrus.Debugf("Listing quizzes for course %d", id)
	uri := a.makeUri("courses", id.ToBase10String(), "quizzes")
	uri.SetQuery(&url.Values{"per_page": {"50"}})
	req, err := a.assembleRequest("GET", uri.String(), nil)
	if err != nil {
		return nil, err
	}
	var response []Quiz
	err = loadAllPages(a, req, &response)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (a *ApiImpl) GetQuiz(courseId, quizId CanvasId) (Quiz, error) {
	logrus.Debugf("Getting quiz %d for course %d", quizId, courseId)
	uri := a.makeUri("courses", courseId.ToBase10String(), "quizzes", quizId.ToBase10String())
	req, err := a.assembleRequest("GET", uri.String(), nil)
	if err != nil {
		return Quiz{}, err
	}
	var response Quiz
	err = a.processRequest(req, &response)
	if err != nil {
		return Quiz{}, err
	}
	return response, nil
}

func (a *ApiImpl) UpdateQuiz(courseId, quizId CanvasId, quiz QuizCreationSpec) error {
	logrus.Debugf("Updating quiz %d for course %d", quizId, courseId)
	uri := a.makeUri("courses", courseId.ToBase10String(), "quizzes", quizId.ToBase10String())
	urlValues := quizCreationSpecToUrlValues(quiz)
	logrus.Debugf("Url values encode to: %s", urlValues.Encode())
	req, err := a.assembleFormEncodedRequest("PUT", uri.String(), &urlValues)
	if err != nil {
		return err
	}
	err = a.processRequest(req, nil)
	if err != nil {
		return err
	}
	return nil
}

type QuizCreationSpec struct {
	Title                         string
	QuizType                      *string
	Text                          *string
	HideResults                   *string
	ShowCorrectAnswersAt          *string
	HideCorrectAnswersAt          *string
	ScoringPolicy                 *string
	AccessCode                    *string
	IPFilter                      *string
	DueAt                         *string
	LockAt                        *string
	UnlockAt                      *string
	TimeLimit                     *int
	AllowedAttempts               *int
	ShuffleAnswers                *bool
	ShowCorrectAnswers            *bool
	ShowCorrectAnswersLastAttempt *bool
	OneQuestionAtATime            *bool
	CantGoBack                    *bool
	Published                     *bool
	OneTimeResults                *bool
	AssignmentGroupId             *CanvasId
}

func quizCreationSpecToUrlValues(spec QuizCreationSpec) url.Values {
	quizSpecPtrString := map[string]*string{
		"quiz[title]":                   &spec.Title,
		"quiz[quiz_type]":               spec.QuizType,
		"quiz[description]":             spec.Text,
		"quiz[hide_results]":            spec.HideResults,
		"quiz[show_correct_answers_at]": spec.ShowCorrectAnswersAt,
		"quiz[hide_correct_answers_at]": spec.HideCorrectAnswersAt,
		"quiz[scoring_policy]":          spec.ScoringPolicy,
		"quiz[access_code]":             spec.AccessCode,
		"quiz[ip_filter]":               spec.IPFilter,
		"quiz[due_at]":                  spec.DueAt,
		"quiz[lock_at]":                 spec.LockAt,
		"quiz[unlock_at]":               spec.UnlockAt,
		"quiz[text]":                    spec.Text,
	}

	quizSpecPtrInt := map[string]*int{
		"quiz[time_limit]":       spec.TimeLimit,
		"quiz[allowed_attempts]": spec.AllowedAttempts,
	}
	quizSpecPtrCanvasId := map[string]*CanvasId{
		"quiz[assignment_group_id]": spec.AssignmentGroupId,
	}

	quizSpecPtrBool := map[string]*bool{
		"quiz[shuffle_answers]":                   spec.ShuffleAnswers,
		"quiz[show_correct_answers]":              spec.ShowCorrectAnswers,
		"quiz[show_correct_answers_last_attempt]": spec.ShowCorrectAnswersLastAttempt,
		"quiz[one_question_at_a_time]":            spec.OneQuestionAtATime,
		"quiz[cant_go_back]":                      spec.CantGoBack,
		"quiz[published]":                         spec.Published,
		"quiz[one_time_results]":                  spec.OneTimeResults,
	}

	quizSpecs := url.Values{}

	for queryAttribute, valuePtr := range quizSpecPtrString {
		if valuePtr != nil {
			quizSpecs.Add(queryAttribute, *valuePtr)
		}
	}

	for queryAttribute, valuePtr := range quizSpecPtrInt {
		if valuePtr != nil {
			quizSpecs.Add(queryAttribute, strconv.Itoa(*valuePtr))
		}
	}
	for queryAttribute, valuePtr := range quizSpecPtrCanvasId {
		if valuePtr != nil {
			quizSpecs.Add(queryAttribute, valuePtr.ToBase10String())
		}
	}

	for queryAttribute, valuePtr := range quizSpecPtrBool {
		if valuePtr != nil {
			quizSpecs.Add(queryAttribute, strconv.FormatBool(*valuePtr))
		}
	}
	return quizSpecs
}

func (a *ApiImpl) CreateQuiz(
	spec QuizCreationSpec,
	courseId CanvasId,
) (Quiz, error) {
	logrus.Debugf("Creating a quiz for course %d", courseId)

	quizSpecs := quizCreationSpecToUrlValues(spec)
	uri := a.makeUri("courses", courseId.ToBase10String(), "quizzes").String()
	logrus.Debugf("RequestURI: %s", uri)
	req, err := a.assembleFormEncodedRequest("POST", uri, &quizSpecs)
	if err != nil {
		return Quiz{}, err
	}
	var quiz Quiz
	err = a.processRequest(req, &quiz)
	if err != nil {
		return Quiz{}, err
	}
	return quiz, nil
}

type canvasQuestionGroupCreationResponse struct {
	Group []QuestionGroup `json:"quiz_groups"`
}

func (a *ApiImpl) CreateQuizQuestionGroup(courseId, quizId CanvasId, title string,
	pickCount int, questionPoints float64) (QuestionGroup, error) {

	logrus.Debugf("Creating a question group for course %d and quiz %d", courseId, quizId)
	uri := a.makeUri("courses", courseId.ToBase10String(),
		"quizzes", quizId.ToBase10String(), "groups").String()
	logrus.Debugf("RequestURI: %s", uri)
	params := url.Values{
		"quiz_groups[][name]":            []string{title},
		"quiz_groups[][pick_count]":      []string{strconv.Itoa(pickCount)},
		"quiz_groups[][question_points]": []string{strconv.FormatFloat(questionPoints, 'f', 2, 64)},
	}
	req, err := a.assembleFormEncodedRequest("POST", uri, &params)
	if err != nil {
		return QuestionGroup{}, err
	}
	// This one returns a 201 instead of a 200 on success
	respBytes, err := a.processRequestCheckRaw(req, []int{http.StatusCreated}, nil)

	var quiz canvasQuestionGroupCreationResponse
	if err = json.Unmarshal(respBytes, &quiz); err != nil {
		return QuestionGroup{}, err
	}
	if quiz.Group == nil || len(quiz.Group) < 1 {
		return QuestionGroup{}, fmt.Errorf("Error: Server responded with empty question group list")
	}
	return quiz.Group[0], nil
}

func (a *ApiImpl) CreateQuestion(courseId, quizId CanvasId, params *url.Values) (QuizQuestion, error) {
	logrus.Debugf("Adding a question to quiz %d (course %d)", quizId, courseId)
	logrus.Debugf("Params: %v", *params)
	uri := a.makeUri("courses", courseId.ToBase10String(),
		"quizzes", quizId.ToBase10String(), "questions").String()
	logrus.Debugf("RequestURI: %s", uri)
	req, err := a.assembleFormEncodedRequest("POST", uri, params)
	if err != nil {
		return QuizQuestion{}, err
	}
	var question QuizQuestion
	err = a.processRequest(req, &question)
	if err != nil {
		return QuizQuestion{}, err
	}
	return question, nil
}

func (a *ApiImpl) DeleteQuiz(courseId, quizId CanvasId) error {
	logrus.Debugf("Deleting quiz with ID %d (course %d)", quizId, courseId)
	uri := a.makeUri("courses", courseId.ToBase10String(), "quizzes", quizId.ToBase10String()).
		String()
	logrus.Debugf("RequestURI: %s", uri)
	req, err := a.assembleRequest("DELETE", uri, nil)
	if err != nil {
		return err
	}
	err = a.processRequest(req, nil)
	return err
}
