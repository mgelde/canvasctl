/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/mgelde/canvasctl/testutils"
	"net/http"
	"path"
	"strconv"
	"testing"
)

func TestModifyFile(t *testing.T) {
	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, _ interface{}) {
			require.Equal(t, req.Header.Get("Authorization"), "Bearer token")
			id, err := strconv.ParseInt(path.Base(req.URL.Path), 10, 64)
			if err != nil {
				t.Fatal("receved illegal URI: ", req.URL.RequestURI())
			}
			require.Equal(t, id, int64(666))
			if v := req.FormValue("locked"); v == "" {
				t.Fatal("locked not present in query")
			} else {
				require.Equal(t, "true", v)
			}
			if v := req.FormValue("parent_folder_id"); v == "" {
				t.Fatal("parent_folder_id not present in query")
			} else {
				require.Equal(t, "12", v)
			}
			w.Write([]byte(`{"id":666,"folder_id":123,"filename":"a.txt","locked":true,"folder_id":12}`))
		}, nil)
	defer mock.Server.Close()
	api, _ := NewAPI(mock.Server.URL, "token")
	file, err := api.ModifyFile(666, MakeFileModification().Published(false).Move(12).Build())
	require.Nil(t, err, "File mod returned err", err)
	require.Equal(t, file.Id, CanvasId(666))
	require.Equal(t, file.FolderId, CanvasId(12))
	require.Equal(t, file.Locked, true)
}
