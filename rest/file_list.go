/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020, 2022 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"net/url"

	"github.com/sirupsen/logrus"
)

func (c *ApiImpl) ListFiles(id CanvasId) ([]FileInfo, error) {
	logrus.Infof("Listing files in course %d", id)
	uri := c.makeUri("courses", id.ToBase10String(), "files")
	uri.SetQuery(&url.Values{"per_page": {"100"}})
	logrus.Debugf("URI: %s", uri.String())
	req, err := c.assembleRequest("GET", uri.String(), nil)
	if err != nil {
		return nil, err
	}
	var array []FileInfo
	err = loadAllPages(c, req, &array)
	if err != nil {
		if httpErr, ok := IsHTTPError(err); ok {
			errors := UnmarshalErrors(httpErr.Body())
			if errors.Errors != nil && len(errors.Errors) >= 1 {
				logrus.Infof("Error body was %s", errors.Errors[0])
			}
		}
		return nil, err
	}
	logrus.Debugf("Got this array of files back %v", array)
	return array, nil
}

func (c *ApiImpl) ListAllFoldersInCourse(courseId CanvasId) ([]FolderInfo, error) {
	logrus.Infof("Listing folders in course %d", courseId)
	uri := c.makeUri("courses", courseId.ToBase10String(), "folders")
	uri.SetQuery(&url.Values{"per_page": {"100"}})
	logrus.Debugf("URI: %s", uri.String())
	req, err := c.assembleRequest("GET", uri.String(), nil)
	if err != nil {
		return nil, err
	}
	var array []FolderInfo
	err = loadAllPages(c, req, &array)
	if err != nil {
		if httpErr, ok := IsHTTPError(err); ok {
			errors := UnmarshalErrors(httpErr.Body())
			if errors.Errors != nil && len(errors.Errors) >= 1 {
				logrus.Infof("Error body was %s", errors.Errors[0])
			}
		}
		return nil, err
	}
	logrus.Debugf("Got this array of folders back %s", array)
	return array, nil
}

func (c *ApiImpl) ResolvePath(courseId CanvasId, path string) ([]FolderInfo, error) {
	logrus.Infof("Resolving path '%s' in course %d", path, courseId)
	uri := c.makeUri("courses", courseId.ToBase10String(), "folders", "by_path", path)
	uri.SetQuery(&url.Values{"per_page": {"100"}})
	logrus.Debugf("URI: %s", uri.String())
	req, err := c.assembleRequest("GET", uri.String(), nil)
	if err != nil {
		return nil, err
	}
	var array []FolderInfo
	err = loadAllPages(c, req, &array)
	if err != nil {
		logrus.Debugf("Received err %s", err.Error())
		if httpErr, ok := IsHTTPError(err); ok {
			errors := UnmarshalErrors(httpErr.Body())
			if errors.Errors != nil && len(errors.Errors) >= 1 {
				logrus.Infof("Error body was %s", errors.Errors[0])
			}
		}
		return nil, err
	}
	logrus.Debugf("Got array %v", array)
	return array, nil
}

func (c *ApiImpl) ListFilesInFolder(folderId CanvasId) ([]FileInfo, error) {
	logrus.Infof("Listing files in folder %d", folderId)
	uri := c.makeUri("folders", folderId.ToBase10String(), "files")
	uri.SetQuery(&url.Values{"per_page": {"100"}})
	logrus.Debugf("URI: %s", uri.String())
	req, err := c.assembleRequest("GET", uri.String(), nil)
	if err != nil {
		return nil, err
	}
	var array []FileInfo
	err = loadAllPages(c, req, &array)
	if err != nil {
		return nil, err
	}
	return array, nil
}

func (c *ApiImpl) GetFile(fileId CanvasId) (FileInfo, error) {
	logrus.Infof("Getting file %d", fileId)
	uri := c.makeUri("files", fileId.ToBase10String())
	logrus.Debugf("URI: %s", uri.String())
	req, err := c.assembleRequest("GET", uri.String(), nil)
	if err != nil {
		return FileInfo{}, err
	}
	var fileInfo FileInfo
	if err = c.processRequest(req, &fileInfo); err != nil {
		return FileInfo{}, err
	}
	return fileInfo, nil
}

func (c *ApiImpl) ListFoldersInFolder(folderId CanvasId) ([]FolderInfo, error) {
	logrus.Infof("Getting sub-folders of folder %d", folderId)
	uri := c.makeUri("folders", folderId.ToBase10String(), "folders")
	uri.SetQuery(&url.Values{"per_page": {"100"}})
	logrus.Debugf("URI: %s", uri.String())
	req, err := c.assembleRequest("GET", uri.String(), nil)
	if err != nil {
		return nil, err
	}
	var folders []FolderInfo
	if err = loadAllPages(c, req, &folders); err != nil {
		if httpErr, ok := IsHTTPError(err); ok {
			errors := UnmarshalErrors(httpErr.Body())
			if errors.Errors != nil && len(errors.Errors) >= 1 {
				logrus.Infof("Error body was %s", errors.Errors[0])
			}
		}
		return nil, err
	}
	return folders, nil
}
