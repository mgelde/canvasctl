/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/mgelde/canvasctl/testutils"
	"net/http"
	"testing"
)

func TestListModules(t *testing.T) {
	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, ptr interface{}) {
			require.Equal(t, req.Header.Get("Authorization"), "Bearer floop")
			require.Equal(t, req.URL.Path, "/api/v1/courses/12/modules")
			if value, ok := req.URL.Query()["per_page"]; !ok {
				t.Fatal("No pagination was used")
			} else {
				require.Equal(t, value[0], "10")
			}
			includeParams := req.URL.Query()["include"]
			if len(includeParams) > 0 && includeParams[0] == "items" {
				w.Write([]byte(`[{"id":123, "name":"bla", "items_count":1,
					"items":[{"id":6429,"title":"Slides Handout","position":1,"content_id":1234,"published":true}]},
				{"id":222, "name":"bla2", "items_count":1,
					"items":[{"id":7779,"title":"Slides Handout 2","position":1,"content_id":12345,"published":true}]}]`))
			} else {
				w.Write([]byte(`[{"id":123, "name":"bla", "items_count":3},{"id":123, "name":"bla", "items_count":3}]`))
			}
		}, nil)
	defer mock.Server.Close()
	api, _ := NewAPI(mock.Server.URL, "floop")

	mods, err := api.GetModules(12, true)
	if err != nil {
		t.Fatal("Got err", err)
	}
	require.Equal(t, len(mods), 2)
	require.Equal(t, mods[0].Id, CanvasId(123))
	require.Equal(t, mods[0].ItemsCount, uint32(1))
	require.Equal(t, mods[0].Name, "bla")
	require.Equal(t, len(mods[0].Items), 1)
	require.Equal(t, mods[0].Items[0].Id, CanvasId(6429))
	require.Equal(t, mods[0].Items[0].Title, "Slides Handout")
	require.Equal(t, mods[0].Items[0].ContentId, CanvasId(1234))

	obj, err := api.GetModules(12, false)
	require.Equal(t, err, nil)
	require.Equal(t, len(obj), 2)
	require.Equal(t, len(obj[0].Items), 0)
	require.Equal(t, obj[0].ItemsCount, uint32(3))
}

func TestAddModuleItem(t *testing.T) {
	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, _ interface{}) {
			require.Equal(t, req.Header.Get("Authorization"), "Bearer floop")
			require.Equal(t, req.URL.Path, "/api/v1/courses/12/modules/31/items")
			require.Equal(t, req.FormValue("module_item[title]"), "123")
			require.Equal(t, req.FormValue("module_item[type]"), "application/pdf")
			require.Equal(t, req.FormValue("module_item[content_id]"), "666")
			w.Write([]byte(`{"id":12,"title":"123"}`))
		}, nil)
	defer mock.Server.Close()
	api, _ := NewAPI(mock.Server.URL, "floop")

	item := ModuleItem{
		Title:     "123",
		Type:      "application/pdf",
		ContentId: 666,
	}
	it, err := api.CreateModuleItem(12, 31, item)
	if err != nil {
		t.Fatal(err)
	}
	require.Equal(t, it.Id, CanvasId(12))
	require.Equal(t, it.Title, "123")
}
