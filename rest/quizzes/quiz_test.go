/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package quizzes

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/mgelde/canvasctl/rest"
	"testing"
)

func TestMakeQuestionWithoutQuizGroup(t *testing.T) {
	q := NewQuestion()
	q.SetTitle("a")
	q.AddDescription("b")
	params := q.MakeParams()
	require.Equal(t, params.Get("question[question_name]"), "a")
	require.Equal(t, params.Get("question[question_text]"), "b")
	require.Equal(t, params.Get("question[quiz_group_id]"), "")

	q.SetQuizGroup(rest.CanvasId(1234))
	params = q.MakeParams()
	require.Equal(t, params.Get("question[quiz_group_id]"), "1234")
}

func TestMakeMultipleChoiceQuestion(t *testing.T) {
	q := NewQuestion()
	q.SetTitle("a")
	q.AddDescription("b")

	q.MakeMultipleChoiceQuestion().AddAnswer("answer 1", true).AddAnswer("answer 2", false)

	params := q.MakeParams()
	require.Equal(t, params.Get("question[answers][0][answer_text]"), "answer 1")
	require.Equal(t, params.Get("question[answers][1][answer_text]"), "answer 2")
	require.Equal(t, params.Get("question[answers][0][answer_weight]"), "100.00")
	require.Equal(t, params.Get("question[answers][1][answer_weight]"), "0.00")
	require.Equal(t, params.Get("question[answers][2][answer_text]"), "")
}

func TestMakeNumericQuestion(t *testing.T) {
	q := NewQuestion()
	q.MakeNumericAnswer().AddAnswerExact(12.1, 0.01)
	q.MakeNumericAnswer().AddAnswerRange(0.1, 1.3).AddAnswerApproximate(12, 3)

	params := q.MakeParams()
	require.Equal(t, params.Get("question[question_type]"), "numerical_question")

	require.Equal(t, params.Get("question[answers][0][numerical_answer_type]"), "exact_answer")
	require.Equal(t, params.Get("question[answers][0][answer_exact]"), "12.100000")
	require.Equal(t, params.Get("question[answers][0][answer_error_margin]"), "0.010000")
	require.Equal(t, params.Get("question[answers][1][numerical_answer_type]"), "range_answer")
	require.Equal(t, params.Get("question[answers][1][answer_range_start]"), "0.100000")
	require.Equal(t, params.Get("question[answers][1][answer_range_end]"), "1.300000")
	require.Equal(t, params.Get("question[answers][2][numerical_answer_type]"), "precision_answer")
	require.Equal(t, params.Get("question[answers][2][answer_approximate]"), "12.00000000")
	require.Equal(t, params.Get("question[answers][2][answer_precision]"), "3")

	// This makes little sense, but we just mimick what the web-interface does here...
	require.Equal(t, params.Get("question[answers][0][answer_weight]"), "100.0")
	require.Equal(t, params.Get("question[answers][1][answer_weight]"), "100.0")
	require.Equal(t, params.Get("question[answers][2][answer_weight]"), "100.0")
}

func TestNumberIsSaved(t *testing.T) {
	q := NewQuestion().SetTitle("A").AddDescription("B")
	q.MakeMultipleChoiceQuestion()
	require.Equal(t, 0, q.(*questionBuilderImpl).getNumber())

	// Use contrstructor again. Should not NOT start counting at 0 again
	q.MakeMultipleChoiceQuestion().AddAnswer("1", true)
	require.Equal(t, 1, q.(*questionBuilderImpl).getNumber())
	q.MakeMultipleChoiceQuestion().AddAnswer("2", true)
}

func TestSettingQuestionTypes(t *testing.T) {
	q := NewQuestion().(*questionBuilderImpl)
	require.Panics(t, func() {
		q.setType(18) // illegal question type
	})
	q.setType(QuizQuestionMultipleAnswers) // should not panic

	require.Panics(t, func() {
		// should not be able to set a second time with a different value
		q.setType(QuizQuestionMultipleChoice)
	})
	require.Panics(t, func() {
		q.MakeMultipleChoiceQuestion() // should panic for same reason
	})
	q.MakeMultipleAnswerQuestion() // should work because same question type
}

func TestMakeShortAnswerQuestion(t *testing.T) {
	q := NewQuestion().SetTitle("title").AddDescription("description")
	q.MakeShortAnswer().AddAnswer("this is correct").AddAnswer("this, too")
	require.Panics(t, func() {
		q.MakeMultipleAnswerQuestion()
	})
	params := q.MakeParams()
	require.Equal(t, params.Get("question[answers][0][answer_text]"), "this is correct")
	require.Equal(t, params.Get("question[answers][0][answer_weight]"), "100.0")
	require.Equal(t, params.Get("question[answers][1][answer_text]"), "this, too")
	require.Equal(t, params.Get("question[answers][1][answer_weight]"), "100.0")
	require.Equal(t, params.Get("question[question_type]"), "short_answer_question")
}

func TestMakeEssayQuestion(t *testing.T) {
	q := NewQuestion().SetTitle("title").AddDescription("description")
	q.MakeEssayQuestion()
	require.Panics(t, func() {
		q.MakeMultipleAnswerQuestion()
	})
	params := q.MakeParams()
	require.Equal(t, params.Get("question[question_type]"), "essay_question")
}

func TestMakeFileUploadQuestion(t *testing.T) {
	q := NewQuestion().SetTitle("title").AddDescription("description")
	q.MakeFileUploadQuestion()
	require.Panics(t, func() {
		q.MakeMultipleAnswerQuestion()
	})
	params := q.MakeParams()
	require.Equal(t, params.Get("question[question_type]"), "file_upload_question")
}

func TestMakeMultipleBlanksQuestion(t *testing.T) {
	q := NewQuestion().SetTitle("title").AddDescription("[a] [b] [c]")
	q.MakeMultipleBlankQuestion()
	require.Panics(t, func() {
		q.MakeMultipleChoiceQuestion()
	})
	builder := q.MakeMultipleBlankQuestion().AddAnswer("a", "answer a").AddAnswer("b", "answer b")
	_, err := builder.ValidateAndReturn()
	require.NotNil(t, err, "[c] has no answer")
	builder.AddAnswer("c", "answer c")
	params, err := builder.ValidateAndReturn()
	require.Nil(t, err, "all fields have at least one answer")

	require.Equal(t, params.Get("question[answers][0][answer_text]"), "answer a")
	require.Equal(t, params.Get("question[answers][0][blank_id]"), "a")
	require.Equal(t, params.Get("question[answers][0][answer_weight]"), "100.0")
	require.Equal(t, params.Get("question[answers][2][answer_text]"), "answer c")
	require.Equal(t, params.Get("question[answers][2][blank_id]"), "c")
	require.Equal(t, params.Get("question[answers][2][answer_weight]"), "100.0")

	require.Equal(t, params.Get("question[question_type]"), "fill_in_multiple_blanks_question")
}
