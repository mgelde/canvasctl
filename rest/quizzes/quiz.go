/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package quizzes

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/mgelde/canvasctl/rest"
	"net/url"
	"regexp"
	"strconv"
	"strings"
)

type QuestionBuilder interface {
	SetQuizGroup(id rest.CanvasId) QuestionBuilder
	SetTitle(string) QuestionBuilder
	AddDescription(text string) QuestionBuilder
	MakeShortAnswer() ShortAnswerBuilder
	MakeMultipleBlankQuestion() MultipleBlankQuestionBuilder
	MakeMultipleChoiceQuestion() TrueFalseAnswerBuilder
	MakeMultipleAnswerQuestion() TrueFalseAnswerBuilder
	MakeNumericAnswer() NumericalAnswerBuilder
	MakeTextOnlyQuestion()
	MakeEssayQuestion()
	MakeFileUploadQuestion()
	MakeParams() *url.Values
	add(key, value string)
	setType(t QuizQuestionType)
	getNumber() int
	incNumber()
	getDescription() string
}

type questionBuilderImpl struct {
	Params url.Values
	Number int
	Type   QuizQuestionType
	Locked bool
}

func NewQuestion() QuestionBuilder {
	return &questionBuilderImpl{
		Params: url.Values{},
		Number: 0,
		Type:   0,
		Locked: false,
	}
}

func (q *questionBuilderImpl) getNumber() int {
	return q.Number
}

func (q *questionBuilderImpl) incNumber() {
	q.Number += 1
}

func (q *questionBuilderImpl) getDescription() string {
	return q.Params.Get("question[question_text]")
}

func (q *questionBuilderImpl) setType(t QuizQuestionType) {
	if q.Locked {
		if q.Type != t {
			logrus.Errorf("Have a type already (%d), but were expected to set %d", q.Type, t)
			panic("Already locked to a type")
		}
		return
	}

	switch t {
	case QuizQuestionMultipleAnswers:
		q.add("question[question_type]", "multiple_answers_question")
	case QuizQuestionMultipleChoice:
		q.add("question[question_type]", "multiple_choice_question")
	case QuizQuestionNumericAnswer:
		q.add("question[question_type]", "numerical_question")
	case QuizQuestionShortAnswer:
		q.add("question[question_type]", "short_answer_question")
	case QuizQuestionMultipleBlanks:
		q.add("question[question_type]", "fill_in_multiple_blanks_question")
	case QuizQuestionEssay:
		q.add("question[question_type]", "essay_question")
	case QuizQuestionFileUpload:
		q.add("question[question_type]", "file_upload_question")
	case QuizQuestionTextOnly:
		q.add("question[question_type]", "text_only_question")
	default:
		logrus.Errorf("Unknown question type: %d", t)
		panic("Unknwon question type")
	}
	q.Type = t
	q.Locked = true
}

func (q *questionBuilderImpl) add(key, value string) {
	q.Params.Add(key, value)
}

func (q *questionBuilderImpl) SetQuizGroup(id rest.CanvasId) QuestionBuilder {
	q.add("question[quiz_group_id]", id.ToBase10String())
	return q
}

func (q *questionBuilderImpl) SetTitle(title string) QuestionBuilder {
	q.add("question[question_name]", title)
	return q
}
func (q *questionBuilderImpl) AddDescription(text string) QuestionBuilder {
	q.add("question[question_text]", text)
	return q
}

func (q *questionBuilderImpl) MakeParams() *url.Values {
	return &q.Params
}

type TrueFalseAnswerBuilder interface {
	AddAnswer(answerText string, correct bool) TrueFalseAnswerBuilder
}

type trueFalseBuilderImpl struct {
	Builder QuestionBuilder
}

func (b *questionBuilderImpl) MakeMultipleAnswerQuestion() TrueFalseAnswerBuilder {
	b.setType(QuizQuestionMultipleAnswers)
	return trueFalseBuilderImpl{b}
}

func (b *questionBuilderImpl) MakeMultipleChoiceQuestion() TrueFalseAnswerBuilder {
	b.setType(QuizQuestionMultipleChoice)
	return trueFalseBuilderImpl{b}
}

func (b *questionBuilderImpl) MakeFileUploadQuestion() {
	b.setType(QuizQuestionFileUpload)
}

func (b *questionBuilderImpl) MakeEssayQuestion() {
	b.setType(QuizQuestionEssay)
}

func (b *questionBuilderImpl) MakeTextOnlyQuestion() {
	b.setType(QuizQuestionTextOnly)
}

type NumericalAnswerBuilder interface {
	AddAnswerExact(answer, margin float64) NumericalAnswerBuilder
	AddAnswerRange(begin, end float64) NumericalAnswerBuilder
	AddAnswerApproximate(answer float64, margin int64) NumericalAnswerBuilder
}

type numericalAnswerBuilderImpl struct {
	Builder QuestionBuilder
}

func (b *questionBuilderImpl) MakeNumericAnswer() NumericalAnswerBuilder {
	b.setType(QuizQuestionNumericAnswer)
	return numericalAnswerBuilderImpl{b}
}

type ShortAnswerBuilder interface {
	AddAnswer(answer string) ShortAnswerBuilder
}

type shortAnswerBuilderImpl struct {
	Builder QuestionBuilder
}

func (b *questionBuilderImpl) MakeShortAnswer() ShortAnswerBuilder {
	b.setType(QuizQuestionShortAnswer)
	return shortAnswerBuilderImpl{b}
}

type MultipleBlankQuestionBuilder interface {
	AddAnswer(field, answer string) MultipleBlankQuestionBuilder
	ValidateAndReturn() (*url.Values, error)
}

type multipleBlanksQuestionBuilderImpl struct {
	Builder     QuestionBuilder
	Association map[string]struct{}
}

func (b *questionBuilderImpl) MakeMultipleBlankQuestion() MultipleBlankQuestionBuilder {
	b.setType(QuizQuestionMultipleBlanks)
	return multipleBlanksQuestionBuilderImpl{b, make(map[string]struct{})}
}

func (b multipleBlanksQuestionBuilderImpl) AddAnswer(field, answer string) MultipleBlankQuestionBuilder {
	number := b.Builder.getNumber()
	b.Builder.add(fmt.Sprintf("question[answers][%d][blank_id]", number), field)
	b.Builder.add(fmt.Sprintf("question[answers][%d][answer_text]", number), answer)
	b.Builder.add(fmt.Sprintf("question[answers][%d][answer_weight]", number), "100.0")
	b.Association[field] = struct{}{}
	b.Builder.incNumber()
	return b
}

func (b multipleBlanksQuestionBuilderImpl) ValidateAndReturn() (*url.Values, error) {
	descr := b.Builder.getDescription()
	exp, err := regexp.Compile(`\[([^[\]]+)\]`)
	if err != nil {
		panic("hard coded regex is syntactically illegal")
	}
	allFields := exp.FindAllString(descr, -1)
	for _, field := range allFields {
		field = strings.Trim(field, "[]")
		if _, present := b.Association[field]; !present {
			return nil, fmt.Errorf("Field %s does not have an answer", field)
		}
	}
	return b.Builder.MakeParams(), nil
}

func (b shortAnswerBuilderImpl) AddAnswer(answer string) ShortAnswerBuilder {
	number := b.Builder.getNumber()
	b.Builder.add(fmt.Sprintf("question[answers][%d][answer_weight]", number), "100.0")
	b.Builder.add(fmt.Sprintf("question[answers][%d][answer_text]", number), answer)
	b.Builder.incNumber()
	return b
}

func (b numericalAnswerBuilderImpl) AddAnswerExact(answer, margin float64) NumericalAnswerBuilder {
	number := b.Builder.getNumber()
	b.Builder.add(
		fmt.Sprintf("question[answers][%d][numerical_answer_type]", number),
		"exact_answer")
	b.Builder.add(
		fmt.Sprintf("question[answers][%d][answer_exact]", number),
		strconv.FormatFloat(answer, 'f', 6, 64))
	b.Builder.add(
		fmt.Sprintf("question[answers][%d][answer_error_margin]", number),
		strconv.FormatFloat(margin, 'f', 6, 64))
	// The web interface seems to add this, so we also add it for good measure
	b.Builder.add(
		fmt.Sprintf("question[answers][%d][answer_weight]", number),
		"100.0")
	b.Builder.incNumber()
	return b
}
func (b numericalAnswerBuilderImpl) AddAnswerRange(begin, end float64) NumericalAnswerBuilder {
	number := b.Builder.getNumber()
	b.Builder.add(
		fmt.Sprintf("question[answers][%d][numerical_answer_type]", number),
		"range_answer")
	b.Builder.add(
		fmt.Sprintf("question[answers][%d][answer_range_start]", number),
		strconv.FormatFloat(begin, 'f', 6, 64))
	b.Builder.add(
		fmt.Sprintf("question[answers][%d][answer_range_end]", number),
		strconv.FormatFloat(end, 'f', 6, 64))
	// The web interface seems to add this, so we also add it for good measure
	b.Builder.add(
		fmt.Sprintf("question[answers][%d][answer_weight]", number),
		"100.0")

	b.Builder.incNumber()
	return b
}
func (b numericalAnswerBuilderImpl) AddAnswerApproximate(answer float64, margin int64) NumericalAnswerBuilder {
	number := b.Builder.getNumber()
	// for some reason this is called "precision_answer", even though it only allows integer margins
	b.Builder.add(
		fmt.Sprintf("question[answers][%d][numerical_answer_type]", number),
		"precision_answer")
	b.Builder.add(
		fmt.Sprintf("question[answers][%d][answer_approximate]", number),
		strconv.FormatFloat(answer, 'f', 8, 64))
	b.Builder.add(
		fmt.Sprintf("question[answers][%d][answer_precision]", number),
		strconv.FormatInt(margin, 10))
	// The web interface seems to add this, so we also add it for good measure
	b.Builder.add(
		fmt.Sprintf("question[answers][%d][answer_weight]", number),
		"100.0")

	b.Builder.incNumber()
	return b
}

func (i trueFalseBuilderImpl) AddAnswer(answerText string, correct bool) TrueFalseAnswerBuilder {
	number := i.Builder.getNumber()

	i.Builder.add(
		fmt.Sprintf("question[answers][%d][answer_text]", number), answerText)
	var weight float64
	if correct {
		weight = 100
	} else {
		weight = 0
	}
	i.Builder.add(
		fmt.Sprintf("question[answers][%d][answer_weight]", number),
		strconv.FormatFloat(weight, 'f', 2, 64))
	i.Builder.incNumber()
	return i
}

type QuizQuestionType int

const (
	QuizQuestionMultipleChoice QuizQuestionType = iota
	QuizQuestionMultipleAnswers
	QuizQuestionNumericAnswer
	QuizQuestionShortAnswer
	QuizQuestionMultipleBlanks
	QuizQuestionEssay
	QuizQuestionFileUpload
	QuizQuestionTextOnly
)
