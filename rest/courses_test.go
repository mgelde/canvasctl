/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/mgelde/canvasctl/testutils"
	"net/http"
	"testing"
)

func TestListCourses(t *testing.T) {
	var i int = 0
	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, ptr interface{}) {
			require.Equal(t, req.URL.Path, "/api/v1/courses")
			require.Equal(t, req.Header.Get("Authorization"), "Bearer floop")
			i := ptr.(*int)
			switch *i {
			case 0:
				w.Write([]byte(`[{"id":1221, "name":"Course 1"}, {"id":1222, "name":"Course 2"}]`))
			case 1:
				//not a list
				w.Write([]byte(`imnotjson`))
			case 2:
				//one not valid
				w.Write([]byte(`[{"id":1221, "name":"Course 1"}, idontknowwhatimdoing]`))
			default:
				t.Fatal("Only two requests should be recorded")
			}
			*i++
		}, &i)
	defer mock.Server.Close()
	api, _ := NewAPI(mock.Server.URL, "floop")
	courses, err := api.GetCourses()
	require.Equal(t, err, nil)
	require.Equal(t, len(courses), 2)
	require.Equal(t, courses[0].Id, CanvasId(1221))
	require.Equal(t, courses[0].Name, "Course 1")
	require.Equal(t, courses[1].Id, CanvasId(1222))
	require.Equal(t, courses[1].Name, "Course 2")

	// invalid json
	courses, err = api.GetCourses()
	require.NotNil(t, err)
	require.Equal(t, len(courses), 0)

	// different invalid json
	courses, err = api.GetCourses()
	require.NotNil(t, err)
	require.Equal(t, len(courses), 0)
}

func TestShowCourses(t *testing.T) {
	var i int = 0
	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, ptr interface{}) {
			require.Equal(t, req.URL.Path, "/api/v1/courses/123")
			require.Equal(t, req.Header.Get("Authorization"), "Bearer floop")
			i := ptr.(*int)
			switch *i {
			case 0:
				w.Write([]byte(`{"id":1221, "name":"Course 1"}`))
			case 1:
				//invalid json
				w.Write([]byte(`imnotjson`))
			default:
				t.Fatal("Only two requests should be recorded")
			}
			*i++
		}, &i)
	defer mock.Server.Close()
	api, _ := NewAPI(mock.Server.URL, "floop")
	course, err := api.GetSingleCourse(CanvasId(123))
	require.Nil(t, err)
	require.Equal(t, course.Id, CanvasId(1221))
	require.Equal(t, course.Name, "Course 1")
	_, err = api.GetSingleCourse(CanvasId(123))
	require.NotNil(t, err)
}
