/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020, 2022 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"fmt"
	"net/http"
	"strings"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	"gitlab.com/mgelde/canvasctl/testutils"
)

func TestHasNext(t *testing.T) {
	header := http.Header{
		"Link": {`<https://www.example.com/api/v1/courses?page=1&per_page=2>; rel="current",<https://www.example.com/api/v1/courses?page=2&per_page=2>; rel="next",<https://www.example.com/api/v1/courses?page=1&per_page=2>; rel="first",<https://www.example.com/api/v1/courses?page=4&per_page=2>; rel="last"`},
	}
	next, url := nextPage(&header)
	require.True(t, next, "Expected a next link, but got none.")
	require.Equal(t, url, "https://www.example.com/api/v1/courses?page=2&per_page=2")

	// no 'rel="next"'
	header = http.Header{
		"Link": {`<https://www.example.com/api/v1/courses?page=1&per_page=2>; rel="current",<https://www.example.com/api/v1/courses?page=1&per_page=2>; rel="first",<https://www.example.com/api/v1/courses?page=1&per_page=2>; rel="last"`},
	}
	next, url = nextPage(&header)
	require.False(t, next, "Did not expect another URL")

	// no Link header
	header = http.Header{}
	next, url = nextPage(&header)
	require.False(t, next, "Did not expect another URL")
}

type HandlerArg struct {
	i *int
	t *testing.T
}

func paginationMockHandler(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, arg interface{}) {
	a := arg.(HandlerArg)
	i := a.i
	t := a.t
	responses := []string{
		strings.ReplaceAll(`<https://www.example.com/api/v1/courses?page=1&per_page=2>; rel="current",<https://www.example.com/api/v1/courses?page=2&per_page=2>; rel="next",<https://www.example.com/api/v1/courses?page=1&per_page=2>; rel="first",<https://www.example.com/api/v1/courses?page=4&per_page=2>; rel="last"`,
			"https://www.example.com/",
			m.Server.URL+"/"),
		strings.ReplaceAll(
			`<https://www.example.com/api/v1/courses?page=2&per_page=2>; rel="current",<https://www.example.com/api/v1/courses?page=3&per_page=2>; rel="next",<https://www.example.com/api/v1/courses?page=1&per_page=2>; rel="first",<https://www.example.com/api/v1/courses?page=4&per_page=2>; rel="last"`,
			"https://www.example.com/",
			m.Server.URL+"/"),
		strings.ReplaceAll(
			`<https://www.example.com/api/v1/courses?page=3&per_page=2>; rel="current",<https://www.example.com/api/v1/courses?page=4&per_page=2>; rel="next",<https://www.example.com/api/v1/courses?page=1&per_page=2>; rel="first",<https://www.example.com/api/v1/courses?page=4&per_page=2>; rel="last"`,
			"https://www.example.com/",
			m.Server.URL+"/"),
		strings.ReplaceAll(
			`<https://www.example.com/api/v1/courses?page=4&per_page=2>; rel="current",<https://www.example.com/api/v1/courses?page=1&per_page=2>; rel="first",<https://www.example.com/api/v1/courses?page=4&per_page=2>; rel="last"`,
			"https://www.example.com/",
			m.Server.URL+"/"),
	}
	w.Header().Add("Link", responses[*i])
	if *i >= 1 && !strings.Contains(req.URL.RequestURI(), fmt.Sprintf("?page=%d&", *i+1)) {
		t.Fatalf("Expected call for page %d, but got this URL: %s", *i, req.URL.Path)
	}
	(*i)++
	w.WriteHeader(200)
	w.Write([]byte(`[{"id":66,"name":"Flooop","workflow_state":"active",
        "parent_account_id":13,"root_account_id":1,"uuid":"base64","default_time_zone"
        :"Europe/Berlin"}]`))
}

func TestPagination(t *testing.T) {
	logrus.SetLevel(logrus.DebugLevel)
	var i int
	arg := HandlerArg{
		i: &i,
		t: t,
	}
	mock := testutils.NewMockCanvasServer(paginationMockHandler, arg)
	defer mock.Server.Close()
	api, err := NewAPI(mock.Server.URL, "token")
	if err != nil {
		t.FailNow()
	}
	// set callback
	numCalled := 0
	startIndex := -1
	stopIndex := -1
	api.SetProgressCallback(func(context ProgressCallbackContext) {
		switch {
		case context == ProgresStart:
			startIndex = numCalled

		case context == ProgressStop:
			stopIndex = numCalled
		}
		numCalled++
	})

	req, _ := http.NewRequest("GET", mock.Server.URL+"/floop", nil)
	var accounts []Account
	err = loadAllPages(api.(*ApiImpl), req, &accounts)
	if err != nil {
		t.Fatal("Could not load pages:", err)
	}
	require.Equal(t, i, 4)
	require.Equal(t, 4, len(accounts))
	require.Equal(t, CanvasId(66), accounts[0].Id)

	require.Equal(t, numCalled, 6)
	require.Equal(t, startIndex, 0)
	require.Equal(t, stopIndex, 5)
}
