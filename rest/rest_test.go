/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"fmt"
	"github.com/stretchr/testify/require"
	"gitlab.com/mgelde/canvasctl/testutils"
	"net/http"
	"net/url"
	"testing"
)

func TestNewAPI(t *testing.T) {
	_, err := NewAPI("http://www.notls.com", "")
	if err == nil {
		t.Fatal("Should fail without TLS, but didn't")
	}
}

func TestMakeUri(t *testing.T) {
	api, err := NewAPI("https://www.example.com/", "")
	if err != nil {
		t.FailNow()
	}
	uri := api.(*ApiImpl).makeUri("/more/").String()
	require.Equal(t, uri, "https://www.example.com/api/v1/more")
}

func TestAddQueryParams(t *testing.T) {
	api, _ := NewAPI("https://www.example.com", "")
	uri := api.(*ApiImpl).makeUri("a", "bb", "ccc").SetQuery(&url.Values{
		"param1": {"value1"},
		"param2": {"value2"},
	}).String()
	require.Equal(t, uri,
		"https://www.example.com/api/v1/a/bb/ccc?param1=value1&param2=value2")
}

func TestAssembleRequest(t *testing.T) {
	api, err := NewAPI("https://www.example.com", "token")
	if err != nil {
		t.FailNow()
	}

	req, err := api.(*ApiImpl).assembleRequest("POST", "https://www.example.com/somestring", nil)
	if err != nil {
		t.FailNow()
	}
	require.Equal(t, req.Method, "POST")
	require.Equal(t, req.URL.String(), "https://www.example.com/somestring")
	require.Equal(t, req.Header.Get("Authorization"), "Bearer token")

	_, err = api.(*ApiImpl).assembleRequest("GET", "_ttps://www.exmaple.com/somestring", nil)
	if err == nil {
		t.FailNow()
	}
}

func TestAssembleFormEncodedRequest(t *testing.T) {
	api, err := NewAPI("https://www.example.com", "token")
	if err != nil {
		t.FailNow()
	}

	params := url.Values{}
	params.Add("key", "value")

	req, err := api.(*ApiImpl).assembleFormEncodedRequest("POST", "https://www.example.com/somestring", &params)
	if err != nil {
		t.FailNow()
	}
	require.Equal(t, req.Method, "POST")
	require.Equal(t, req.URL.String(), "https://www.example.com/somestring")
	require.Equal(t, req.Header.Get("Authorization"), "Bearer token")
	require.Equal(t, req.Header.Get("Content-Type"), "application/x-www-form-urlencoded")

	require.Equal(t, req.FormValue("key"), "value")
}

func TestProcessRequestCheck(t *testing.T) {
	mock := testutils.NewMockCanvasServer(func(_ *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, _ interface{}) {
		w.Header().Add("foo", "bar")
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(`yippie`))
	}, nil)
	defer mock.Server.Close()
	api, err := NewAPI(mock.Server.URL, "mynotsosecrettoken")
	if err != nil {
		t.FailNow()
	}
	uri := api.(*ApiImpl).makeUri("bla").String()
	req, _ := api.(*ApiImpl).assembleRequest("GET", uri, nil)
	var header http.Header
	resp, _ := api.(*ApiImpl).processRequestCheckRaw(req, []int{http.StatusOK, http.StatusCreated}, &header)
	require.Equal(t, string(resp), `yippie`)
}

func TestProcessRequest(t *testing.T) {
	mock := testutils.NewMockCanvasServer(func(_ *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, _ interface{}) {
		w.Write([]byte(`{"Bla":12}`))
	}, nil)
	defer mock.Server.Close()
	api, err := NewAPI(mock.Server.URL, "mynotsosecrettoken")
	if err != nil {
		t.FailNow()
	}
	type dummy struct {
		Bla int
	}
	var outParam dummy
	uri := api.(*ApiImpl).makeUri("bla").String()
	req, _ := api.(*ApiImpl).assembleRequest("GET", uri, nil)
	err = api.(*ApiImpl).processRequest(req, &outParam)
	require.Nil(t, err)
	require.Equal(t, 12, outParam.Bla)

	// should also work if we do not want to parse the response
	err = api.(*ApiImpl).processRequest(req, nil)
	require.Nil(t, err)

}

func TestAuthentication(t *testing.T) {
	var receivedToken string = ""
	var authorizationHeaderSet bool = false
	mock := testutils.NewMockCanvasServer(func(_ *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, _ interface{}) {
		if header := req.Header.Get("Authorization"); header != "" {
			authorizationHeaderSet = true
			receivedToken = header
		}
		w.Header().Add("WWW-Authenticate", `Bearer realm="canvas-lms"`)
		w.WriteHeader(401)
		w.Write([]byte(`{"errors":[{"message":"Invalid access token."}]}`))
	}, nil)
	defer mock.Server.Close()

	api, err := NewAPI(mock.Server.URL, "mynotsosecrettoken")
	if err != nil {
		t.FailNow()
	}
	uri := api.(*ApiImpl).makeUri("bla").String()
	req, _ := api.(*ApiImpl).assembleRequest("GET", uri, nil)
	var header http.Header
	_, err = api.(*ApiImpl).processRequestCheckRaw(req, nil, &header)
	if err == nil {
		fmt.Println("Needed an error.")
		t.FailNow()
	}
	require.Equal(t, err.Error(), "Not authorized. Need authentication.")
	require.True(t, NeedsAuthentication(err), "Expected NeedsAuthentication(err) to be true")
	require.True(t, IsNotAuthorizedError(err), "Expected IsNotAuthorizedError(err) to be true")
	_, ok := IsHTTPError(err)
	require.True(t, ok)
	require.True(t, authorizationHeaderSet, "No authorization header was received")
	require.Equal(t, receivedToken, "Bearer mynotsosecrettoken")
	require.True(t, header == nil, "Header must be nil on error")
}

func TestHttpError(t *testing.T) {
	err := NewHTTPError(404, nil, fmt.Errorf("some error"))
	require.NotNil(t, err)

	recoveredErr, ok := IsHTTPError(err)
	require.True(t, ok)
	require.NotNil(t, recoveredErr)
	require.Equal(t, recoveredErr.Code(), 404)
	require.Nil(t, recoveredErr.Body())
	require.Equal(t, recoveredErr.InnerError().Error(), "some error")
}
