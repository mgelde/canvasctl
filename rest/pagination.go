/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020, 2022 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"container/list"
	"encoding/json"
	"net/http"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
)

func nextPage(header *http.Header) (bool, string) {
	linkHeader := header.Get("Link")
	if linkHeader == "" {
		return false, ""
	}
	logrus.Info("Pagination active. Finding next page...")
	components := strings.Split(linkHeader, ",")
	for _, linkObj := range components {
		parts := strings.Split(linkObj, ";")
		if len(parts) != 2 {
			logrus.Warn("Received unexpected format in Link header: ", linkObj)
			continue
		}
		link := strings.Trim(parts[0], "<> ")
		rel := strings.TrimSpace(parts[1])
		logrus.Debugf("-> Link (%s): %s", rel, link)
		if rel == "rel=\"next\"" {
			logrus.Debug("Success")
			return true, link
		}
	}
	return false, ""
}

/*
 * Out must be pointer to a slice. That slice type must be something that can be
 * handed to json.Unmarshal.
 */
func loadAllPages[X any, T *[]X](c *ApiImpl, req *http.Request, out T) error {
	if out == nil {
		panic("out must not be nil")
	}
	var perPage int64
	if req.URL.Query().Has("per_page") {
		var err error
		perPage, err = strconv.ParseInt(req.URL.Query().Get("per_page"), 10, 64)
		if err != nil {
			perPage = 10
			logrus.Warnf("Could not parse per_page: %v", err)
		}
	} else {
		perPage = 10
	}

	if c.ProgressCallbackFunc != nil {
		c.ProgressCallbackFunc(ProgresStart)
	}

	var header http.Header
	responsebytes, err := c.processRequestCheckRaw(req, nil, &header)
	if err != nil {
		logrus.Info("Could not perform request for pagination: ", err)
		return err
	}
	if c.ProgressCallbackFunc != nil {
		c.ProgressCallbackFunc(ProgressContinue)
	}

	responseList := list.New()

	nextUnmarshalSlice := make([]X, 0, perPage)
	err = json.Unmarshal(responsebytes, &nextUnmarshalSlice)
	if err != nil {
		logrus.Error("Could not unmarshal response json: ", err)
		logrus.Debugf("Recevied: %s", string(responsebytes))
	}
	logrus.Debugf("Unmarshalled %d entries", len(nextUnmarshalSlice))

	responseList.PushBack(nextUnmarshalSlice)
	count := len(nextUnmarshalSlice)

	for hasNext, url := nextPage(&header); hasNext; hasNext, url = nextPage(&header) {
		req, err := c.assembleRequest("GET", url, nil)
		if err != nil {
			logrus.Error("Could not assemble request for pagination", err)
			break
		}
		responsebytes, err = c.processRequestCheckRaw(req, nil, &header)
		if err != nil {
			logrus.Info("Could not perform request for pagination: ", err)
			break
		}
		if c.ProgressCallbackFunc != nil {
			c.ProgressCallbackFunc(ProgressContinue)
		}

		nextUnmarshalSlice = make([]X, 0, perPage)
		err = json.Unmarshal(responsebytes, &nextUnmarshalSlice)
		if err != nil {
			logrus.Error("Could not unmarshal response json: ", err)
			logrus.Debugf("Recevied: %s", string(responsebytes))
		}
		logrus.Debugf("Unmarshalled %d entries", len(nextUnmarshalSlice))

		responseList.PushBack(nextUnmarshalSlice)
		count += len(nextUnmarshalSlice)
	}

	outSlice := make([]X, 0, count)
	for elem := responseList.Front(); elem != nil; elem = elem.Next() {
		val := elem.Value.([]X)
		outSlice = append(outSlice, val...)
	}
	*out = outSlice

	if c.ProgressCallbackFunc != nil {
		c.ProgressCallbackFunc(ProgressStop)
	}
	return err
}
