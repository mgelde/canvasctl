/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020, 2022 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"net/url"
)

func (c *ApiImpl) GetModules(courseId CanvasId, details bool) ([]Module, error) {
	if c == nil {
		panic("No API")
	}
	values := &url.Values{"per_page": {"10"}}
	if details {
		values.Add("include", "items")
	}

	req, err := c.assembleRequest("GET",
		c.makeUri("courses", courseId.ToBase10String(), "modules").SetQuery(values).String(),
		nil)
	if err != nil {
		return nil, err
	}
	var array []Module
	err = loadAllPages(c, req, &array)
	if err != nil {
		return nil, err
	}
	return array, nil
}

func (c *ApiImpl) CreateModule(id CanvasId, name string) (Module, error) {
	uri := c.makeUri("courses", id.ToBase10String(), "modules")
	params := url.Values{
		"module[name]": {name},
	}
	req, err := c.assembleFormEncodedRequest("POST", uri.String(), &params)
	if err != nil {
		return Module{}, err
	}
	var module Module
	err = c.processRequest(req, &module)
	if err != nil {
		return Module{}, err
	}
	return module, nil
}

func (c *ApiImpl) CreateModuleItem(courseId, moduleId CanvasId, item ModuleItem) (ModuleItem, error) {
	uri := c.makeUri("courses", courseId.ToBase10String(), "modules", moduleId.ToBase10String(), "items")
	params := url.Values{
		"module_item[title]":      {item.Title},
		"module_item[type]":       {item.Type},
		"module_item[content_id]": {item.ContentId.ToBase10String()},
	}
	req, err := c.assembleFormEncodedRequest("POST", uri.String(), &params)
	if err != nil {
		return ModuleItem{}, err
	}
	var newItem ModuleItem
	err = c.processRequest(req, &newItem)
	if err != nil {
		return ModuleItem{}, err
	}
	return newItem, nil
}

func (c *ApiImpl) GetModuleItems(courseId, moduleId CanvasId) ([]ModuleItem, error) {
	uri := c.makeUri("courses", courseId.ToBase10String(), "modules", moduleId.ToBase10String(), "items")
	req, err := c.assembleRequest("GET", uri.String(), nil)
	if err != nil {
		return nil, err
	}
	var items []ModuleItem
	err = c.processRequest(req, &items)
	if err != nil {
		return nil, err
	}
	return items, nil
}
