/*
canvasctl: Use Canvas LMS from command line
    Copyright (C) 2020 Marcus Gelderie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package rest

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/mgelde/canvasctl/testutils"
	"net/http"
	"testing"
)

func TestListFiles(t *testing.T) {
	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, _ interface{}) {
			require.Equal(t, req.Header.Get("Authorization"), "Bearer token")
			require.Equal(t, req.URL.Path, "/api/v1/courses/12/files")
			require.Equal(t, req.URL.Query().Get("per_page"), "100")
			w.Write([]byte(`[{"id":1234, "uuid":"aababbababab", "folder_id":4321,
			"display_name": "bla", "filename": "bla.zip", "content-type":"application/zip"},
			{"id":12345, "uuid":"aababbababab", "folder_id":54321,
			"display_name": "bla2", "filename": "bla2.txt", "content-type":"text/plain"}]`))
		}, nil)
	defer mock.Server.Close()
	api, _ := NewAPI(mock.Server.URL, "token")

	files, err := api.ListFiles(12)
	if err != nil {
		t.Fatal("Error:", err)
	}
	require.Equal(t, len(files), 2)
	require.Equal(t, files[0].Id, CanvasId(1234))
	require.Equal(t, files[0].FolderId, CanvasId(4321))
	require.Equal(t, files[0].Filename, "bla.zip")
	require.Equal(t, files[0].ContentType, "application/zip")
	require.Equal(t, files[0].DisplayName, "bla")

	require.Equal(t, files[1].Id, CanvasId(12345))
	require.Equal(t, files[1].FolderId, CanvasId(54321))
	require.Equal(t, files[1].Filename, "bla2.txt")
	require.Equal(t, files[1].ContentType, "text/plain")
	require.Equal(t, files[1].DisplayName, "bla2")
}

func TestListCourseFolders(t *testing.T) {
	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, _ interface{}) {
			require.Equal(t, req.Header.Get("Authorization"), "Bearer token")
			require.Equal(t, req.URL.Path, "/api/v1/courses/12/folders")
			require.Equal(t, req.URL.Query().Get("per_page"), "100")
			w.Write([]byte(`[{
  "context_type": "Course",
  "context_id": 1401,
  "files_count": 0,
  "position": 2,
  "folders_url": "https://www.example.com/api/v1/folders/2938/folders",
  "files_url": "https://www.example.com/api/v1/folders/2938/files",
  "full_name": "course files/foldera/folderb",
  "id": 2938,
  "folders_count": 0,
  "name": "folderb",
  "parent_folder_id": 2937
},
{
  "context_type": "Course",
  "context_id": 1401,
  "files_count": 0,
  "position": 2,
  "folders_url": "https://www.example.com/api/v1/folders/2937/folders",
  "files_url": "https://www.example.com/api/v1/folders/2937/files",
  "full_name": "course files/foldera",
  "id": 2937,
  "folders_count": 0,
  "name": "foldera",
  "parent_folder_id": 2934
}]`))
		}, nil)
	defer mock.Server.Close()
	api, _ := NewAPI(mock.Server.URL, "token")

	folders, err := api.ListAllFoldersInCourse(12)
	require.Nil(t, err)
	require.Equal(t, 2, len(folders))
	require.Equal(t, CanvasId(2938), folders[0].Id)
	require.Equal(t, CanvasId(2937), folders[0].ParentFolderId)
	require.Equal(t, "Course", folders[0].ContextType)
	require.Equal(t, "folderb", folders[0].Name)

	require.Equal(t, CanvasId(2937), folders[1].Id)
	require.Equal(t, CanvasId(2934), folders[1].ParentFolderId)
	require.Equal(t, "Course", folders[1].ContextType)
	require.Equal(t, "foldera", folders[1].Name)
}

func TestResolvePath(t *testing.T) {
	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, _ interface{}) {
			require.Equal(t, req.Header.Get("Authorization"), "Bearer token")
			require.Equal(t, req.URL.Path, "/api/v1/courses/12/folders/by_path/path/to/folder")
			require.Equal(t, req.URL.Query().Get("per_page"), "100")
			w.Write([]byte(`[{
  "context_type": "Course",
  "context_id": 1401,
  "files_count": 0,
  "position": 2,
  "folders_url": "https://www.example.com/api/v1/folders/2938/folders",
  "files_url": "https://www.example.com/api/v1/folders/2938/files",
  "full_name": "course files/foldera/folderb",
  "id": 2938,
  "folders_count": 0,
  "name": "folderb",
  "parent_folder_id": 2937
},
{
  "context_type": "Course",
  "context_id": 1401,
  "files_count": 0,
  "position": 2,
  "folders_url": "https://www.example.com/api/v1/folders/2937/folders",
  "files_url": "https://www.example.com/api/v1/folders/2937/files",
  "full_name": "course files/foldera",
  "id": 2937,
  "folders_count": 0,
  "name": "foldera",
  "parent_folder_id": 2934
}]`))
		}, nil)
	defer mock.Server.Close()
	api, _ := NewAPI(mock.Server.URL, "token")

	folders, err := api.ResolvePath(12, "/path/to/folder")
	require.Nil(t, err)
	require.Len(t, folders, 2)
	require.Equal(t, CanvasId(2938), folders[0].Id)
}

func TestListFilesInFolder(t *testing.T) {
	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, _ interface{}) {
			require.Equal(t, req.Header.Get("Authorization"), "Bearer token")
			require.Equal(t, "/api/v1/folders/12/files", req.URL.Path)
			require.Equal(t, "100", req.URL.Query().Get("per_page"))
			w.Write([]byte(`[{"id":1234, "uuid":"aababbababab", "folder_id":4321,
			"display_name": "bla", "filename": "bla.zip", "content-type":"application/zip"},
			{"id":12345, "uuid":"aababbababab", "folder_id":54321,
			"display_name": "bla2", "filename": "bla2.txt", "content-type":"text/plain"}]`))
		}, nil)
	defer mock.Server.Close()
	api, _ := NewAPI(mock.Server.URL, "token")

	files, err := api.ListFilesInFolder(12)
	if err != nil {
		t.Fatal("Error:", err)
	}
	require.Len(t, files, 2)
	require.Equal(t, files[0].Id, CanvasId(1234))
	require.Equal(t, files[0].FolderId, CanvasId(4321))
	require.Equal(t, files[0].Filename, "bla.zip")
	require.Equal(t, files[0].ContentType, "application/zip")
	require.Equal(t, files[0].DisplayName, "bla")

	require.Equal(t, files[1].Id, CanvasId(12345))
	require.Equal(t, files[1].FolderId, CanvasId(54321))
	require.Equal(t, files[1].Filename, "bla2.txt")
	require.Equal(t, files[1].ContentType, "text/plain")
	require.Equal(t, files[1].DisplayName, "bla2")
}

func TestListFoldersInFolder(t *testing.T) {
	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, _ interface{}) {
			require.Equal(t, req.Header.Get("Authorization"), "Bearer token")
			require.Equal(t, "/api/v1/folders/12/folders", req.URL.Path)
			require.Equal(t, "100", req.URL.Query().Get("per_page"))
			w.Write([]byte(`[{
  "context_type": "Course",
  "context_id": 1401,
  "files_count": 0,
  "position": 2,
  "folders_url": "https://www.example.com/api/v1/folders/2938/folders",
  "files_url": "https://www.example.com/api/v1/folders/2938/files",
  "full_name": "course files/foldera/folderb",
  "id": 2938,
  "folders_count": 0,
  "name": "folderb",
  "parent_folder_id": 2937
},
{
  "context_type": "Course",
  "context_id": 1401,
  "files_count": 0,
  "position": 2,
  "folders_url": "https://www.example.com/api/v1/folders/2937/folders",
  "files_url": "https://www.example.com/api/v1/folders/2937/files",
  "full_name": "course files/foldera",
  "id": 2937,
  "folders_count": 0,
  "name": "foldera",
  "parent_folder_id": 2934
}]`))
		}, nil)
	defer mock.Server.Close()
	api, _ := NewAPI(mock.Server.URL, "token")

	files, err := api.ListFoldersInFolder(12)
	if err != nil {
		t.Fatal("Error:", err)
	}
	require.Len(t, files, 2)
	require.Equal(t, files[0].Id, CanvasId(2938))
	require.Equal(t, files[1].Id, CanvasId(2937))
}

func TestGetFile(t *testing.T) {
	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, _ interface{}) {
			require.Equal(t, req.Header.Get("Authorization"), "Bearer token")
			require.Equal(t, "/api/v1/files/12", req.URL.Path)
			w.Write([]byte(`{"id":1234, "uuid":"aababbababab", "folder_id": 4321, "display_name": "bla", "filename": "bla.zip", "content-type":"application/zip"}`))
		}, nil)
	defer mock.Server.Close()
	api, _ := NewAPI(mock.Server.URL, "token")

	file, err := api.GetFile(12)
	require.Nil(t, err)
	require.Equal(t, CanvasId(1234), file.Id)
}
