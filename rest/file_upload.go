/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"bytes"
	"fmt"
	"github.com/sirupsen/logrus"
	"io"
	"mime/multipart"
	"net/http"
	"strconv"
)

type uploadResponse struct {
	Url    string            `json:"upload_url"`
	Params map[string]string `json:"upload_params"`
}

type PushFileSpec struct {
	File           io.Reader
	FileSize       uint64
	RemoteFilename string
	RemotePath     string
}

func makeUploadAnnoucementRequestBody(pushSpec PushFileSpec) (io.Reader, string, error) {
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	writer.WriteField("name", pushSpec.RemoteFilename)
	writer.WriteField("size", strconv.FormatUint(pushSpec.FileSize, 10))
	writer.WriteField("parent_folder_path", pushSpec.RemotePath)
	//"content_type" :
	//"on_duplicate":
	err := writer.Close()
	if err != nil {
		logrus.Error(err)
		return nil, "", err
	}
	return body, writer.FormDataContentType(), nil
}

func makeFileUploadRequestBody(pushSpec PushFileSpec,
	uploadResp uploadResponse) (io.Reader, string, error) {
	body := bytes.NewBuffer(make([]byte, 0, pushSpec.FileSize+uint64(1024)))
	writer := multipart.NewWriter(body)

	logrus.Debug("Url received from upload announcement:", uploadResp.Url)
	for key, val := range uploadResp.Params {
		logrus.Debugf("Adding key=%s, val=%s to next POST request", key, val)
		writer.WriteField(key, val)
	}

	fileWriter, err := writer.CreateFormFile("file", pushSpec.RemoteFilename)
	if err != nil {
		return nil, "", err
	}
	io.Copy(fileWriter, pushSpec.File)
	err = writer.Close()
	if err != nil {
		return nil, "", err
	}

	return body, writer.FormDataContentType(), nil
}

func confirmFileUpload(c *ApiImpl, resp *http.Response) (FileUploadResponse, error) {
	if (resp.StatusCode >= 300 && resp.StatusCode <= 308) || http.StatusCreated == resp.StatusCode {
		redirUrl, err := resp.Location()
		if err != nil {
			logrus.Error(err)
			return FileUploadResponse{}, fmt.Errorf("Could not confirm file upload. May not have worked.")
		}
		req, err := c.assembleRequest("GET", redirUrl.String(), nil)
		if err != nil {
			return FileUploadResponse{}, err
		}
		var uploadResponse FileUploadResponse
		err = c.processRequest(req, &uploadResponse)
		if err != nil {
			return FileUploadResponse{}, err
		}
		return uploadResponse, nil
	} else {
		logrus.Errorf("Got resonse code %d (%s)", resp.StatusCode, resp.Status)
		return FileUploadResponse{}, fmt.Errorf("Got an unexpected status code code. Upload failed.")
	}
}

func (c *ApiImpl) PushFile(id CanvasId, pushSpec PushFileSpec) (FileUploadResponse, error) {
	logrus.Debug("Uploading file")

	// announce file upload
	uploadAnnouncementBody, contentType, err := makeUploadAnnoucementRequestBody(pushSpec)
	if err != nil {
		return FileUploadResponse{}, fmt.Errorf("Could not assemble upload announcement")
	}
	uploadRequest, err := c.assembleRequest(
		"POST",
		c.makeUri("courses", id.ToBase10String(), "files").String(),
		uploadAnnouncementBody)
	if err != nil {
		logrus.Error("Could not assemble upload announcement", err)
		return FileUploadResponse{}, fmt.Errorf("Could not send upload announcement. Error forming request.")
	}
	uploadRequest.Header.Set("Content-Type", contentType)
	var uploadResp uploadResponse
	err = c.processRequest(uploadRequest, &uploadResp)
	if err != nil {
		logrus.Error("Error during upload announcement", err)
		return FileUploadResponse{}, err
	}

	// upload actual file
	fileUploadBody, contentType, err := makeFileUploadRequestBody(pushSpec, uploadResp)
	if err != nil {
		return FileUploadResponse{}, fmt.Errorf("Could not assmeble upload request")
	}
	// Need to use DefaultClient directly, as response may be 301 or 201 (but not 200)
	// At this point we do not need authentication anyway. By having Post assemble the
	// request, we can avoid accidentally sending our access token to the wrong server.
	// Note:
	// we need to disable redirects, because Canvas wants a GET in any case, but http.Client
	// will do a POST in some cases
	defaultRedirectPolicy := http.DefaultClient.CheckRedirect
	http.DefaultClient.CheckRedirect = func(req *http.Request, via []*http.Request) error {
		return http.ErrUseLastResponse
	}
	logrus.Debug("Performing post to", uploadResp.Url)
	resp, err := http.DefaultClient.Post(uploadResp.Url, contentType, fileUploadBody)
	if err != nil {
		logrus.Error(err)
		return FileUploadResponse{}, fmt.Errorf("Cannot upload file. POST failed.")
	}
	resp.Body.Close()
	// restore default policy
	http.DefaultClient.CheckRedirect = defaultRedirectPolicy
	logrus.Debugf("Got status code %d (%s)", resp.StatusCode, resp.Status)

	// confirm upload, if success
	fileUploadResponse, err := confirmFileUpload(c, resp)
	if err != nil {
		return FileUploadResponse{}, nil
	}
	return fileUploadResponse, nil
}
