/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/mgelde/canvasctl/testutils"
	"net/http"
	"testing"
)

func TestListAccounts(t *testing.T) {
	var i int = 0
	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, ptr interface{}) {
			i := ptr.(*int)
			switch *i {
			case 0:
				require.Equal(t, req.Header.Get("Authorization"), "Bearer floop")
				w.Write([]byte(`[{"default_time_zone":"Europe/Berlin", "id":12, "name": "fancy"}]`))
			case 1:
				//not a list
				w.Write([]byte(`{"default_time_zone":"Europe/Berlin", "id":12, "name": "fancy"}`))
			default:
				t.Fatal("Only two requests should be recorded")
			}
			*i++
		}, &i)
	defer mock.Server.Close()
	api, _ := NewAPI(mock.Server.URL, "floop")

	accs, err := api.GetAccounts()
	require.Nil(t, err)
	require.Equal(t, len(accs), 1)
	require.Equal(t, accs[0].Id, CanvasId(12))
	require.Equal(t, accs[0].DefaultTimeZone, "Europe/Berlin")
	require.Equal(t, accs[0].Name, "fancy")

	obj, err := api.GetAccounts()
	require.NotNil(t, err)
	require.Equal(t, len(obj), 0)
}
