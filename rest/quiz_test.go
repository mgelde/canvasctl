/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020, 2022 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"io"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/mgelde/canvasctl/testutils"
)

func TestListQuizz(t *testing.T) {
	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, _ interface{}) {
			require.Equal(t, req.Header.Get("Authorization"), "Bearer token")
			exp := regexp.MustCompile(`/?api/v1/courses/(\d+)/quizzes`)
			require.Equal(t, exp.FindStringSubmatch(req.URL.Path)[1], "666")
			w.Write([]byte(`[{"id":1234, "quiz_type":"practice_quiz"}]`))
		}, nil)
	defer mock.Server.Close()
	api, _ := NewAPI(mock.Server.URL, "token")
	if quizzes, err := api.ListQuizzes(666); err != nil {
		t.Fatal("Error listing quizzes", err)
	} else {
		require.Equal(t, len(quizzes), 1)
		require.Equal(t, quizzes[0].Id, CanvasId(1234))
		require.Equal(t, quizzes[0].QuizType, "practice_quiz")
	}
}

func TestGetQuiz(t *testing.T) {
	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, _ interface{}) {
			require.Equal(t, req.Header.Get("Authorization"), "Bearer token")
			exp := regexp.MustCompile(`/?api/v1/courses/(\d+)/quizzes/(\d+)`)
			require.Equal(t, "666", exp.FindStringSubmatch(req.URL.Path)[1])
			require.Equal(t, "999", exp.FindStringSubmatch(req.URL.Path)[2])
			w.Write([]byte(`{"id":1234, "quiz_type":"practice_quiz", "title" : "This is a title"}`))
		}, nil)
	defer mock.Server.Close()
	api, _ := NewAPI(mock.Server.URL, "token")
	if quiz, err := api.GetQuiz(666, 999); err != nil {
		t.Fatal("Error listing quizzes", err)
	} else {
		require.Equal(t, CanvasId(1234), quiz.Id)
		require.Equal(t, "practice_quiz", quiz.QuizType)
		require.Equal(t, "This is a title", quiz.Title)
	}
}

func TestUpdateQuiz(t *testing.T) {
	newTitle := "New Title"
	newPublished := true
	newDueAt := "2022-01-01T13:14:15.012345"
	newLockAt := "2022-01-01T13:20:15.012345"
	newUnlockAt := "2022-01-01T12:16:15.012345"

	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, _ interface{}) {
			require.Equal(t, req.Header.Get("Authorization"), "Bearer token")
			exp := regexp.MustCompile(`/?api/v1/courses/(\d+)/quizzes/(\d+)`)
			require.Equal(t, "666", exp.FindStringSubmatch(req.URL.Path)[1])
			require.Equal(t, "999", exp.FindStringSubmatch(req.URL.Path)[2])
			stringBuilder := strings.Builder{}
			if _, err := io.Copy(&stringBuilder, req.Body); err != nil {
				t.Error("Could not parse body into string")
			}
			queryParams, err := url.ParseQuery(stringBuilder.String())
			if err != nil {
				t.Errorf("Could not parse URL query params: %v", err)
			}
			require.Equal(t, newTitle, queryParams.Get("quiz[title]"))
			require.Equal(t, strconv.FormatBool(newPublished), queryParams.Get("quiz[published]"))
			require.Equal(t, newDueAt, queryParams.Get("quiz[due_at]"))
			require.Equal(t, newLockAt, queryParams.Get("quiz[lock_at]"))
			require.Equal(t, newUnlockAt, queryParams.Get("quiz[unlock_at]"))
		}, nil)
	defer mock.Server.Close()

	spec := QuizCreationSpec{
		Title:     newTitle,
		Published: &newPublished,
		DueAt:     &newDueAt,
		LockAt:    &newLockAt,
		UnlockAt:  &newUnlockAt,
	}

	api, _ := NewAPI(mock.Server.URL, "token")
	if err := api.UpdateQuiz(666, 999, spec); err != nil {
		t.Fatal("Error updating quiz", err)
	}
}

func TestCreateQuiz(t *testing.T) {
	newTitle := "New Title"
	newPublished := true
	newDueAt := "2022-01-01T13:14:15.012345"
	newLockAt := "2022-01-01T13:20:15.012345"
	newUnlockAt := "2022-01-01T12:16:15.012345"

	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, _ interface{}) {
			require.Equal(t, req.Header.Get("Authorization"), "Bearer token")
			exp := regexp.MustCompile(`/?api/v1/courses/(\d+)/quizzes`)
			require.Equal(t, "666", exp.FindStringSubmatch(req.URL.Path)[1])
			require.Equal(t, "POST", req.Method)
			stringBuilder := strings.Builder{}
			if _, err := io.Copy(&stringBuilder, req.Body); err != nil {
				t.Error("Could not parse body into string")
			}
			queryParams, err := url.ParseQuery(stringBuilder.String())
			if err != nil {
				t.Errorf("Could not parse URL query params: %v", err)
			}
			require.Equal(t, newTitle, queryParams.Get("quiz[title]"))
			require.Equal(t, strconv.FormatBool(newPublished), queryParams.Get("quiz[published]"))
			require.Equal(t, newDueAt, queryParams.Get("quiz[due_at]"))
			require.Equal(t, newLockAt, queryParams.Get("quiz[lock_at]"))
			require.Equal(t, newUnlockAt, queryParams.Get("quiz[unlock_at]"))
			w.Write([]byte(`{"id":1234, "title": "New title", "published" : true, "due_at" : null}`))
		}, nil)
	defer mock.Server.Close()

	spec := QuizCreationSpec{
		Title:     newTitle,
		Published: &newPublished,
		DueAt:     &newDueAt,
		LockAt:    &newLockAt,
		UnlockAt:  &newUnlockAt,
	}

	api, _ := NewAPI(mock.Server.URL, "token")
	if quiz, err := api.CreateQuiz(spec, 666); err != nil {
		t.Fatal("Error updating quiz", err)
	} else {
		require.True(t, quiz.Published)
		require.Equal(t, "New title", quiz.Title)
		require.Equal(t, time.Time{}, quiz.DueAt)
		require.Equal(t, CanvasId(1234), quiz.Id)
	}
}

func TestAddQuestion(t *testing.T) {
	params := url.Values{
		"bllli":    []string{"a", "b", "c"},
		"ajkadjas": []string{"a"},
	}
	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, _ interface{}) {
			require.Equal(t, req.Header.Get("Authorization"), "Bearer token")
			require.Equal(t, req.Method, "POST")
			exp := regexp.MustCompile(`/?api/v1/courses/(\d+)/quizzes/(\d+)`)
			match := exp.FindStringSubmatch(req.URL.Path)
			require.Equal(t, match[1], "666")
			require.Equal(t, match[2], "777")
			require.Nil(t, req.ParseForm(), "cannot parse form")
			require.Equal(t, req.PostForm, params)

			w.Write([]byte(`{"id":14102,"quiz_id":1652,"quiz_group_id":638,"assessment_question_id":14008,"position":null,"question_name":"Floop","question_type":"essay_question","question_text":"cyber","points_possible":0.0,"correct_comments":"","incorrect_comments":"","neutral_comments":"","correct_comments_html":"","incorrect_comments_html":"","neutral_comments_html":"","answers":[],"variables":null,"formulas":null,"answer_tolerance":null,"formula_decimal_places":null,"matches":null,"matching_answer_incorrect_matches":null}`))
		}, nil)
	defer mock.Server.Close()
	api, _ := NewAPI(mock.Server.URL, "token")
	if question, err := api.CreateQuestion(666, 777, &params); err != nil {
		t.Fatal("Error creating question", err)
	} else {
		require.Equal(t, question.Id, CanvasId(14102))
		require.Equal(t, question.QuizId, CanvasId(1652))
		require.Equal(t, question.QuestionName, "Floop")
		require.Equal(t, question.QuestionText, "cyber")
	}
}

func TestAddQuestionGroup(t *testing.T) {
	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, _ interface{}) {
			require.Equal(t, req.Header.Get("Authorization"), "Bearer token")
			require.Equal(t, req.Method, "POST")
			require.Equal(t, req.URL.Path, "/api/v1/courses/123/quizzes/456/groups")
			if value := req.FormValue("quiz_groups[][name]"); value == "" {
				t.Fatal("Did not receive a quiz name")
			} else {
				require.Equal(t, value, "A Name")
			}
			if value := req.FormValue("quiz_groups[][pick_count]"); value == "" {
				t.Fatal("Did not receive a pick count")
			} else {
				require.Equal(t, value, "2")
			}
			if value := req.FormValue("quiz_groups[][question_points]"); value == "" {
				t.Fatal("Did not receive a question count")
			} else {
				require.Equal(t, value, "3.20")
			}
			w.WriteHeader(http.StatusCreated) // here Canvas sends a 201 instead of a 200...
			w.Write([]byte(`{"quiz_groups":[{"id":643,"quiz_id":1660,"name":"Gruppenbla","pick_count":1,"question_points":1.0,"position":1,"assessment_question_bank_id":null}]}`))
		}, nil)
	defer mock.Server.Close()
	api, _ := NewAPI(mock.Server.URL, "token")
	if group, err := api.CreateQuizQuestionGroup(123, 456, "A Name", 2, 3.2); err != nil {
		t.Fatal("Error creating question", err)
	} else {
		require.Equal(t, group.Id, CanvasId(643))
		require.Equal(t, group.QuizId, CanvasId(1660))
		require.Equal(t, group.Name, "Gruppenbla")
		require.Equal(t, group.PickCount, 1)
		require.Equal(t, group.QuestionPoints, float64(1.0))
	}
}

func TestDeleteQuiz(t *testing.T) {
	mock := testutils.NewMockCanvasServer(
		func(m *testutils.MockCanvas, w http.ResponseWriter, req *http.Request, _ interface{}) {
			require.Equal(t, req.Header.Get("Authorization"), "Bearer token")
			exp := regexp.MustCompile(`/?api/v1/courses/(\d+)/quizzes/(\d+)`)
			matches := exp.FindStringSubmatch(req.URL.Path)
			require.Equal(t, matches[1], "666")
			require.Equal(t, matches[2], "1234")
			w.Write([]byte(``))
		}, nil)
	defer mock.Server.Close()
	api, _ := NewAPI(mock.Server.URL, "token")
	if err := api.DeleteQuiz(666, 1234); err != nil {
		t.Fatal("Error listing quizzes", err)
	}
}

func TestTranslateQuizSpec(t *testing.T) {
	accessCode := "bla"
	text := "this is a text"
	quizType := "assignment"
	hideResults := "always"
	scoringPolicy := "scoring policy"
	showResultsAt := "20220101"
	hideResultsAt := "20220102"
	iPFilter := "iPFilter"
	dueAt := "dueAt"
	lockAt := "lockAt"
	unlockAt := "unlockAt"
	timeLimit := 1
	allowedAttempts := 2
	shuffleAnswers := true
	showCorrectAnswers := true
	showCorrectAnswersLastAttempt := true
	oneQuestionAtATime := true
	cantGoBack := true
	published := true
	oneTimeResults := true
	assignmentGroupId := CanvasId(1234)
	spec := QuizCreationSpec{
		Title:                         "Test",
		AccessCode:                    &accessCode,
		QuizType:                      &quizType,
		Text:                          &text,
		HideResults:                   &hideResults,
		ShowCorrectAnswersAt:          &showResultsAt,
		HideCorrectAnswersAt:          &hideResultsAt,
		ScoringPolicy:                 &scoringPolicy,
		IPFilter:                      &iPFilter,
		DueAt:                         &dueAt,
		LockAt:                        &lockAt,
		UnlockAt:                      &unlockAt,
		TimeLimit:                     &timeLimit,
		AllowedAttempts:               &allowedAttempts,
		ShuffleAnswers:                &shuffleAnswers,
		ShowCorrectAnswers:            &showCorrectAnswers,
		ShowCorrectAnswersLastAttempt: &showCorrectAnswersLastAttempt,
		OneQuestionAtATime:            &oneQuestionAtATime,
		CantGoBack:                    &cantGoBack,
		Published:                     &published,
		OneTimeResults:                &oneTimeResults,
		AssignmentGroupId:             &assignmentGroupId,
	}
	urlValues := quizCreationSpecToUrlValues(spec)
	require.Equal(t, urlValues.Get("quiz[title]"), "Test")
	require.Equal(t, urlValues.Get("quiz[access_code]"), accessCode)
	require.Equal(t, urlValues.Get("quiz[quiz_type]"), quizType)
	require.Equal(t, urlValues.Get("quiz[text]"), text)
	require.Equal(t, urlValues.Get("quiz[hide_results]"), hideResults)
	require.Equal(t, urlValues.Get("quiz[show_correct_answers_at]"), showResultsAt)
	require.Equal(t, urlValues.Get("quiz[hide_correct_answers_at]"), hideResultsAt)
	require.Equal(t, urlValues.Get("quiz[scoring_policy]"), scoringPolicy)
	require.Equal(t, urlValues.Get("quiz[ip_filter]"), iPFilter)
	require.Equal(t, urlValues.Get("quiz[due_at]"), dueAt)
	require.Equal(t, urlValues.Get("quiz[lock_at]"), lockAt)
	require.Equal(t, urlValues.Get("quiz[unlock_at]"), unlockAt)
	require.Equal(t, urlValues.Get("quiz[time_limit]"), strconv.Itoa(timeLimit))
	require.Equal(t, urlValues.Get("quiz[allowed_attempts]"), strconv.Itoa(allowedAttempts))
	require.Equal(t, urlValues.Get("quiz[shuffle_answers]"), strconv.FormatBool(shuffleAnswers))
	require.Equal(t, urlValues.Get("quiz[show_correct_answers]"), strconv.FormatBool(showCorrectAnswers))
	require.Equal(t, urlValues.Get("quiz[show_correct_answers_last_attempt]"), strconv.FormatBool(showCorrectAnswersLastAttempt))
	require.Equal(t, urlValues.Get("quiz[one_question_at_a_time]"), strconv.FormatBool(oneQuestionAtATime))
	require.Equal(t, urlValues.Get("quiz[cant_go_back]"), strconv.FormatBool(cantGoBack))
	require.Equal(t, urlValues.Get("quiz[published]"), strconv.FormatBool(published))
	require.Equal(t, urlValues.Get("quiz[one_time_results]"), strconv.FormatBool(oneTimeResults))
	require.Equal(t, urlValues.Get("quiz[assignment_group_id]"), assignmentGroupId.ToBase10String())
}
