/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"encoding/json"
	"github.com/stretchr/testify/require"
	"testing"
	"time"
)

func TestAccountParsing(t *testing.T) {
	raw := []byte(`[{"id":66,"name":"Flooop","workflow_state":"active",
        "parent_account_id":13,"root_account_id":1,"uuid":"base64","default_time_zone"
        :"Europe/Berlin"}]`)

	accounts, err := unmarshallAccounts(raw)
	if err != nil {
		t.Fatal("Could not unmarshal", err)
	}
	require.Equal(t, len(accounts), 1)
	a := accounts[0]

	require.Equal(t, a.Id, CanvasId(66))
	require.Equal(t, a.Name, "Flooop")
	require.Equal(t, a.WorfkflowState, "active")
	require.Equal(t, a.ParentAccountId, CanvasId(13))
	require.Equal(t, a.RootAccountId, CanvasId(1))
	require.Equal(t, a.Uuid, "base64")
	require.Equal(t, a.DefaultTimeZone, "Europe/Berlin")

	require.Equal(t, a.SisImportId, CanvasId(0))
	require.Equal(t, a.SisAccountId, "")
}

func TestAccountParsingFails(t *testing.T) {
	// not a list of accounts
	raw := []byte(`{"id":66,"name":"Flooop","workflow_state":"active",
        "parent_account_id":13,"root_account_id":1,"uuid":"base64","default_time_zone"
        :"Europe/Berlin"}`)
	_, err := unmarshallAccounts(raw)
	if err == nil {
		t.Fatal("Expected unmarshalling to fail")
	}
}

func TestCourseParsing(t *testing.T) {
	raw := []byte(`[{"id":321,"name":"CourseA","account_id":66,"uuid":"b64",
        "start_at":"2010-01-01T11:11:11Z","grading_standard_id":null,"is_public":false,
        "created_at":"2010-01-01T09:09:09Z","course_code":"CourseA","default_view":"modules",
        "root_account_id":1,"enrollment_term_id":1,"license":"private",
        "grade_passback_setting":null,"end_at":null,"public_syllabus":false,
        "public_syllabus_to_auth":false,"storage_quota_mb":1500,"is_public_to_auth_users":false,
        "apply_assignment_group_weights":false,
        "calendar":{"ics":"https://bla.instructure.com/feeds/calendars/somecal.ics"},
        "time_zone":"Europe/Berlin","blueprint":false,"sis_course_id":null,"integration_id":null,
        "enrollments":[{"type":"teacher","role":"TeacherEnrollment","role_id":1,"user_id":1234,
        "enrollment_state":"active","limit_privileges_to_course_section":false}],
        "hide_final_grades":false,"workflow_state":"available",
        "restrict_enrollments_to_course_dates":false,"overridden_course_visibility":""}]`)
	courses, err := unmarshallCourses(raw)
	if err != nil {
		t.Fatal("Could not unmarshal", err)
	}
	require.Equal(t, len(courses), 1)
	a := courses[0]

	require.Equal(t, a.Id, CanvasId(321))
	require.Equal(t, a.AccountId, CanvasId(66))
	require.Equal(t, a.Name, "CourseA")
	require.Equal(t, a.SisCourseId, "")
	require.Equal(t, a.Uuid, "b64")
	require.Equal(t, a.StartedAt, time.Date(2010, 1, 1, 11, 11, 11, 0, time.UTC))
	require.Equal(t, a.Code, "CourseA")

}

func TestCourseParsingFails(t *testing.T) {
	// invalid data types for id and name
	raw := []byte(`{"id":"NOTANINT","name":12345,"account_id":66,"uuid":"b64",
        "start_at":"2010-01-01T11:11:11Z","grading_standard_id":null,"is_public":false,
        "created_at":"2010-01-01T09:09:09Z","course_code":"CourseA","default_view":"modules",
        "root_account_id":1,"enrollment_term_id":1,"license":"private",
        "grade_passback_setting":null,"end_at":null,"public_syllabus":false,
        "public_syllabus_to_auth":false,"storage_quota_mb":1500,"is_public_to_auth_users":false,
        "apply_assignment_group_weights":false,
        "calendar":{"ics":"https://bla.instructure.com/feeds/calendars/somecal.ics"},
        "time_zone":"Europe/Berlin","blueprint":false,"sis_course_id":null,"integration_id":null,
        "enrollments":[{"type":"teacher","role":"TeacherEnrollment","role_id":1,"user_id":1234,
        "enrollment_state":"active","limit_privileges_to_course_section":false}],
        "hide_final_grades":false,"workflow_state":"available",
        "restrict_enrollments_to_course_dates":false,"overridden_course_visibility":""}`)
	_, err := unmarshallCourses(raw)
	if err == nil {
		t.Fatal("Expected unmarshalling to fail")
	}
}

func TestModuleParsing(t *testing.T) {
	raw := []byte(`[{"id":1268,"name":"Vorlesung 1","position":1,"unlock_at":null,"require_sequential_progress":false,"publish_final_grade":false,"prerequisite_module_ids":[],"published":true,"items_count":1,"items_url":"https://example.com/api/v1/courses/123/modules/1268/items"}]`)
	modules, err := unmarshallModules(raw)
	if err != nil {
		t.Fatal("Could not unmarshall", err)
	}
	mod := modules[0]
	require.Equal(t, mod.Id, CanvasId(1268))
	require.Equal(t, mod.Name, "Vorlesung 1")
	require.Equal(t, mod.ItemsCount, uint32(1))
}

func TestModuleParsingFails(t *testing.T) {
	// illegal types for id and name
	raw := []byte(`[{"id":"notanint","name":1234,"position":1,"unlock_at":null,"require_sequential_progress":false,"publish_final_grade":false,"prerequisite_module_ids":[],"published":true,"items_count":1,"items_url":"https://example.com/api/v1/courses/123/modules/1268/items"}]`)
	_, err := unmarshallModules(raw)
	if err == nil {
		t.Fatal("Expected unmarshalling to fail")
	}
}

func TestConvertIdToString(t *testing.T) {
	id := CanvasId(12345)
	require.Equal(t, id.ToBase10String(), "12345")
}

func TestUnmarshallModuleItem(t *testing.T) {
	raw := []byte(`{"id":12,"title":"123"}`)
	var it ModuleItem
	err := json.Unmarshal(raw, &it)
	if err != nil {
		t.Fatal("Could not unmarshal")
	}
	require.Equal(t, it.Id, CanvasId(12))
	require.Equal(t, it.Title, "123")
}

func TestStringers(t *testing.T) {
	f := FileInfo{
		DisplayName: "bla",
		Id:          CanvasId(12),
		FolderId:    CanvasId(1337),
	}
	require.Equal(t, f.String(), "bla (id=12) in folder with id 1337")

	q := Quiz{
		Id:                CanvasId(12),
		AssignmentGroupId: CanvasId(33),
		Title:             "title",
		QuizType:          "sometype",
	}
	require.Equal(t, q.String(), "title (id=12, type=sometype, group=33)")
	a := Account{
		Id:   CanvasId(12),
		Name: "name",
	}
	require.Equal(t, a.String(), "name (id=12)")
	c := Course{
		Id:   CanvasId(12),
		Name: "name",
	}
	require.Equal(t, c.String(), "name=name (id=12)")
	m := Module{
		Id:         CanvasId(12),
		Name:       "name",
		ItemsCount: 13,
		Published:  true,
	}
	require.Equal(t, m.String(), "name (id=12, items=13) [published]")
	m.Published = false
	require.Equal(t, m.String(), "name (id=12, items=13)")

	mi := ModuleItem{
		Id:        CanvasId(12),
		Title:     "name",
		Type:      "sometype",
		Published: true,
	}
	require.Equal(t, mi.String(), "name (id=12, type=sometype) [published]")
	mi.Published = false
	require.Equal(t, mi.String(), "name (id=12, type=sometype)")
}
