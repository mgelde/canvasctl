/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"path"
	"strings"

	"github.com/sirupsen/logrus"
)

type ProgressCallback func(ProgressCallbackContext)

type ProgressCallbackContext int

const (
	ProgresStart ProgressCallbackContext = iota
	ProgressContinue
	ProgressStop
)

type API interface {
	GetQuiz(courseId, quizId CanvasId) (Quiz, error)
	UpdateQuiz(courseId, quizId CanvasId, quiz QuizCreationSpec) error
	GetModules(courseId CanvasId, details bool) ([]Module, error)
	CreateModule(id CanvasId, name string) (Module, error)
	CreateModuleItem(courseId, moduleId CanvasId, item ModuleItem) (ModuleItem, error)
	GetModuleItems(courseId, moduleId CanvasId) ([]ModuleItem, error)
	GetSingleCourse(id CanvasId) (Course, error)
	GetCourses() ([]Course, error)
	GetAccounts() ([]Account, error)
	PushFile(id CanvasId, pushSpec PushFileSpec) (FileUploadResponse, error)
	ListFiles(id CanvasId) ([]FileInfo, error)
	ListAllFoldersInCourse(id CanvasId) ([]FolderInfo, error)
	ModifyFile(id CanvasId, mod url.Values) (FileInfo, error)
	ListQuizzes(id CanvasId) ([]Quiz, error)
	DeleteQuiz(courseId, quizId CanvasId) error
	CreateQuestion(courseId, quizzId CanvasId, params *url.Values) (QuizQuestion, error)
	CreateQuiz(spec QuizCreationSpec, courseId CanvasId) (Quiz, error)
	CreateQuizQuestionGroup(courseId, quizId CanvasId, title string, pick int, points float64) (QuestionGroup, error)
	SetProgressCallback(cb ProgressCallback) ProgressCallback
	ListFilesInFolder(folderId CanvasId) ([]FileInfo, error)
	ResolvePath(courseId CanvasId, path string) ([]FolderInfo, error)
	GetFile(fileId CanvasId) (FileInfo, error)
	ListFoldersInFolder(folderId CanvasId) ([]FolderInfo, error)
}

type ApiImpl struct {
	Url                  *url.URL
	Token                string
	ProgressCallbackFunc ProgressCallback
}

func NewAPI(u, token string) (API, error) {
	logrus.Debug("Creating new API with url:", u)
	parsedUrl, err := url.Parse(u)
	if !parsedUrl.IsAbs() {
		return nil, fmt.Errorf("Need an absolute URL, but got: %s", parsedUrl)
	}
	if parsedUrl.Scheme != "https" {
		return nil, fmt.Errorf("Need https")
	}
	if err != nil {
		return nil, err
	}
	return &ApiImpl{
		Url:                  parsedUrl,
		Token:                token,
		ProgressCallbackFunc: nil,
	}, nil
}

type Command struct {
	Method string
	Path   string
	Data   []byte
}

const BaseUri = "api/v1"

type myUri struct {
	Url url.URL
}

func (u *myUri) SetQuery(values *url.Values) *myUri {
	if u == nil {
		panic("myUri receiver is nil")
	}
	q := u.Url.Query()
	for key, items := range *values {
		for _, item := range items {
			q.Add(key, item)
		}
	}
	u.Url.RawQuery = q.Encode()
	return u
}

func (u *myUri) String() string {
	if u == nil {
		panic("myUri receiver nil")
	}
	return u.Url.String()
}

func (c *ApiImpl) makeUri(p ...string) *myUri {
	if c == nil {
		panic("nil ApiIml receiver")
	}
	joined := path.Join(p...)
	uri := myUri{Url: *c.Url}
	uri.Url.Path = path.Join(BaseUri, joined)
	return &uri
}

func (c *ApiImpl) assembleRequest(method string, url string, body io.Reader) (*http.Request, error) {
	if c == nil {
		panic("No API")
	}
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", "Bearer "+c.Token)

	return req, nil
}

func (c *ApiImpl) assembleFormEncodedRequest(method string, url string, params *url.Values) (*http.Request, error) {
	if c == nil {
		panic("No API")
	}

	req, err := c.assembleRequest(method, url, strings.NewReader(params.Encode()))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	return req, nil
}

type HTTPError interface {
	Code() int
	InnerError() error
	Body() []byte
	error
}

type httpError struct {
	code int
	body []byte
	err  error
}

func NewHTTPError(code int, body []byte, err error) error {
	return &httpError{code, body, err}
}

func (e *httpError) Error() string { return e.err.Error() }

func (e *httpError) Code() int         { return e.code }
func (e *httpError) InnerError() error { return e.err }
func (e *httpError) Body() []byte      { return e.body }

type authError struct {
	*httpError
	needAuthentication bool
}

func (e *authError) Error() string {
	return e.httpError.Error()
}

func (e *authError) Is(err error) bool {
	_, ok := err.(*authError)
	return ok
}

func NewAuthError(code int, body []byte, needsAuthentication bool) error {
	if needsAuthentication {
		return &authError{
			NewHTTPError(code, body, fmt.Errorf("Not authorized. Need authentication.")).(*httpError),
			needsAuthentication}
	} else {
		return &authError{
			NewHTTPError(code, body, fmt.Errorf("Not authorized.")).(*httpError),
			needsAuthentication}
	}
}

func IsNotAuthorizedError(err error) bool {
	return (&authError{}).Is(err)
}

func NeedsAuthentication(err error) bool {
	return IsNotAuthorizedError(err) && err.(*authError).needAuthentication
}

func IsHTTPError(err error) (HTTPError, bool) {
	if httpErr, ok := err.(*httpError); ok {
		return httpErr, true
	}
	if IsNotAuthorizedError(err) {
		unauthErr := err.(*authError)
		return unauthErr.httpError, true
	}
	return nil, false
}

/*
Process a request, check if the status code is 200 or not. Read the entire body, if it is.
*/
func (c *ApiImpl) processRequestCheckRaw(req *http.Request, okCodes []int, headerOut *http.Header) ([]byte, error) {
	if c == nil {
		panic("No API receiver")
	}
	logrus.Infof("Performing request to %v with method %v", req.URL, req.Method)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		logrus.Error("Error performing request", err)
		return nil, err
	}
	defer resp.Body.Close()

	logrus.Infof("Got reponse with status %d (%v)", resp.StatusCode, resp.Status)

	if okCodes == nil {
		okCodes = []int{http.StatusOK}
	}
	switch {
	case statusCodeIsInOkList(resp.StatusCode, okCodes):
		// do nothing
	case resp.StatusCode == http.StatusUnauthorized:
		logrus.Warnf("Not authorized %d, (%v)", resp.StatusCode, resp.Status)
		if h := resp.Header.Get("WWW-Authenticate"); h != "" {
			logrus.Info("WWW-Authenticate header was set")
			// header is set; Canvas wants a token
			return nil, NewAuthError(resp.StatusCode, nil, true)
		}
		return nil, NewAuthError(resp.StatusCode, nil, false)
	default:
		logrus.Warn("Unsuccessful request. Got Response with status:", resp.Status)
		if bytes, err := ioutil.ReadAll(resp.Body); err != nil {
			logrus.Errorf("Error reading body %v", err)
			return nil, NewHTTPError(resp.StatusCode,
				nil, fmt.Errorf("HTTP Status %v (%v)", resp.Status, resp.StatusCode))
		} else {
			logrus.Debug("Body:", string(bytes))
			return nil, NewHTTPError(resp.StatusCode,
				bytes, fmt.Errorf("HTTP Status %v (%v)", resp.Status, resp.StatusCode))
		}
	}

	if bytes, err := ioutil.ReadAll(resp.Body); err != nil {
		logrus.Error(err)
		return nil, fmt.Errorf("Error reading from body")
	} else {
		if headerOut != nil {
			*headerOut = resp.Header
		}
		return bytes, nil
	}
}

func (c *ApiImpl) processRequestCheck(req *http.Request, out interface{}, okCodes []int, headerOut *http.Header) error {
	respBytes, err := c.processRequestCheckRaw(req, okCodes, headerOut)
	if err != nil {
		return err
	}
	if out != nil {
		err = json.Unmarshal(respBytes, out)
		if err != nil {
			return err
		}
	}
	return nil
}

func (c *ApiImpl) processRequest(req *http.Request, out interface{}) error {
	return c.processRequestCheck(req, out, nil, nil)
}

func (c *ApiImpl) SetProgressCallback(cb ProgressCallback) ProgressCallback {
	current := c.ProgressCallbackFunc
	c.ProgressCallbackFunc = cb
	return current
}
