/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	"strconv"
	"time"
)

type CanvasId uint64

func (id CanvasId) ToBase10String() string {
	return strconv.FormatUint(uint64(id), 10)
}

type CanvasErrorMessage struct {
	Message string `json:"message"`
}

func (m CanvasErrorMessage) String() string {
	return m.Message
}

type CanvasErrors struct {
	Errors []CanvasErrorMessage `json:"errors"`
}

func UnmarshalErrors(bytes []byte) CanvasErrors {
	if bytes == nil {
		logrus.Debugf("Received empty slice. No errrors found")
		return CanvasErrors{nil}
	}
	var errors CanvasErrors
	err := json.Unmarshal(bytes, &errors)
	if err != nil {
		logrus.Errorf("Cannot decode errors: %s", string(bytes))
		logrus.Errorf("Got unmarshal error %s", err)
		return CanvasErrors{nil}
	}
	return errors
}

type Account struct {
	Id                         CanvasId `json:"id"`
	Name                       string   `json:"name"`
	Uuid                       string   `json:"uuid"`
	ParentAccountId            CanvasId `json:"parent_account_id"`
	RootAccountId              CanvasId `json:"root_account_id"`
	DefaultStorageQuotaMb      uint32   `json:"default_storage_quota_mb"`
	DefaultUserStorageQuotaMb  uint32   `json:"default_user_storage_quota_mb"`
	DefaultGroupStorageQuotaMb uint32   `json:"default_group_storage_quota_mb"`
	DefaultTimeZone            string   `json:"default_time_zone"`
	SisAccountId               string   `json:"sis_account_id"`
	IntegrationId              string   `json:"integration_id"`
	SisImportId                CanvasId `json:"sis_import_id"`
	LtiGuid                    string   `json:"lti_uid"`
	WorfkflowState             string   `json:"workflow_state"`
}

func unmarshallAccounts(bytes []byte) ([]Account, error) {
	var accounts []Account
	err := json.Unmarshal(bytes, &accounts)
	if err != nil {
		logrus.Error(err)
		return nil, fmt.Errorf("Could not unmarshal accounts")
	}
	return accounts, nil
}

func (a Account) String() string {
	return fmt.Sprintf("%v (id=%v)", a.Name, a.Id)
}

type Course struct {
	Name        string    `json:"name"`
	Id          CanvasId  `json:"id"`
	AccountId   CanvasId  `json:"account_id"`
	SisCourseId string    `json:"sis_course_id"`
	Uuid        string    `json:"uuid"`
	Code        string    `json:"course_code"`
	StartedAt   time.Time `json:"start_at"`
}

func (a Course) String() string {
	return fmt.Sprintf("name=%v (id=%v)", a.Name, a.Id)
}

func unmarshallCourses(bytes []byte) ([]Course, error) {
	var courses []Course
	err := json.Unmarshal(bytes, &courses)
	if err != nil {
		logrus.Error(err)
		return nil, fmt.Errorf("Could not unmarshal courses")
	}
	return courses, nil
}

type Module struct {
	Id                        CanvasId     `json:"id"`
	Name                      string       `json:"name"`
	Published                 bool         `json:"published"`
	Position                  uint32       `json:"position"`
	Items                     []ModuleItem `json:"items"`
	ItemsCount                uint32       `json:"items_count"`
	ItemsUrl                  string       `json:"items_url"`
	WorkflowState             string       `json:"workflow_state"`
	UnlockAt                  string       `json:"unlock_at"`
	RequireSequentialProgress bool         `json:"require_sequential_progress"`
	PrerequisiteModuleIds     []CanvasId   `json:"prerequisite_module_ids"`
	State                     string       `json:"state"`
	CompletedAt               string       `json:"completed_at"`
	PublishFinalGrade         bool         `json:"publish_final_grade"`
}

func unmarshallModules(bytes []byte) ([]Module, error) {
	var res []Module
	err := json.Unmarshal(bytes, &res)
	if err != nil {
		logrus.Error(err)
		return nil, fmt.Errorf("Could not unmarshal modules")
	}
	return res, nil
}

func (m Module) String() string {
	var pub string
	if m.Published {
		pub = " [published]"
	} else {
		pub = ""
	}
	return fmt.Sprintf("%v (id=%v, items=%v)%v", m.Name, m.Id, m.ItemsCount, pub)
}

type ModuleItem struct {
	Id                    CanvasId       `json:"id"`
	Title                 string         `json:"title"`
	ModuleId              CanvasId       `json:"module_id"`
	Position              int            `json:"position"`
	Indent                int            `json:"indent"`
	Type                  string         `json:"type"`
	ContentId             CanvasId       `json:"content_id"`
	HtmlUrl               string         `json:"html_url"`
	Url                   string         `json:"url"`
	PageUrl               string         `json:"page_url"`
	ExternalUrl           string         `json:"external_url"`
	NewTab                bool           `json:"new_tab"`
	CompletionRequirement string         `json:"completion_requirement"`
	Details               ContentDetails `json:"content_details"`
	Published             bool           `json:"published"`
}

func (m ModuleItem) String() string {
	var pub string
	if m.Published {
		pub = " [published]"
	} else {
		pub = ""
	}
	return fmt.Sprintf("%v (id=%v, type=%v)%v", m.Title, m.Id, m.Type, pub)
}

type FolderInfo struct {
	ContextType    string    `json:"context_type"`
	ContextId      CanvasId  `json:"context_id"`
	FilesCount     int       `json:"files_count"`
	Position       int       `json:"position"`
	UpdatedAt      time.Time `json:"updated_at"`
	FoldersUrl     string    `json:"folders_url"`
	FilesUrl       string    `json:"files_url"`
	FullName       string    `json:"full_name"`
	LockAt         time.Time `json:"lock_at"`
	Id             CanvasId  `json:"id"`
	FoldersCount   int       `json:"folders_count"`
	Name           string    `json:"name"`
	ParentFolderId CanvasId  `json:"parent_folder_id"`
	CreatedAt      time.Time `json:"created_at"`
	UnlockAt       time.Time `json:"unlock_at"`
	Hidden         bool      `json:"hidden"`
	HiddenForUser  bool      `json:"hidden_for_user"`
	Locked         bool      `json:"locked"`
	LockedForUser  bool      `json:"locked_for_user"`
	ForSubmissions bool      `json:"for_submissions"`
}

func (f FolderInfo) String() string {
	return fmt.Sprintf("%s (id=%d, parent_id=%d)", f.Name, f.Id, f.ParentFolderId)
}

type ContentDetails struct {
	PointsPossible  float64   `json:"points_possible"`
	DueAt           time.Time `json:"due_at"`
	UnlockAt        time.Time `json:"unlock_at"`
	LockAt          time.Time `json:"lock_at"`
	LockedForUser   bool      `json:"locked_for_user"`
	LockExplanation string    `json:"lock_explanation"`
	LockInfo        string    `json:"lock_info"`
}

type FileInfo struct {
	Id              CanvasId  `json:"id"`
	Uuid            string    `json:"uuid"`
	FolderId        CanvasId  `json:"folder_id"`
	DisplayName     string    `json:"display_name"`
	Filename        string    `json:"filename"`
	ContentType     string    `json:"content-type"`
	Url             string    `json:"url"`
	Size            int       `json:"size"`
	CreatedAt       time.Time `json:"createdAt"`
	UpdatedAt       time.Time `json:"updatedAt"`
	UnlockAt        time.Time `json:"unlockAt"`
	Locked          bool      `json:"locked"`
	Hidden          bool      `json:"hidden"`
	LockAt          time.Time `json:"lockAt"`
	HiddenForUser   bool      `json:"hidden_for_user"`
	ThumbnailUrl    string    `json:"thumbnailUrl"`
	ModifiedAt      time.Time `json:"modifiedAt"`
	MimeClass       string    `json:"mimeClass"`
	MediaEntryId    string    `json:"mediaEntry_id"`
	LockedForUser   bool      `json:"lockedFor_user"`
	LockInfo        string    `json:"lockInfo"`
	LockExplanation string    `json:"lockExplanation"`
	PreviewUrl      string    `json:"previewUrl"`
}

type FileUploadResponse struct {
	Id          CanvasId `json:"id"`
	Url         string   `json:"url"`
	ContentType string   `json:"content-type"`
	DisplayName string   `json:"display_name"`
	Size        int64    `json:"size"`
}

func (f FileInfo) String() string {
	return fmt.Sprintf("%s (id=%d) in folder with id %d", f.DisplayName, f.Id, f.FolderId)
}

type QuestionGroup struct {
	Id                       CanvasId `json:"id"`
	QuizId                   CanvasId `json:"quiz_id"`
	Name                     string   `json:"name"`
	PickCount                int      `json:"pick_count"`
	QuestionPoints           float64  `json:"question_points"`
	Position                 int      `json:"position"`
	AssessmentQuestionBankId CanvasId `json:"assessment_question_bank_id"`
}

type Quiz struct {
	Id                            CanvasId    `json:"id"`
	Title                         string      `json:"title"`
	HtmlUrl                       string      `json:"html_url"`
	MobileUrl                     string      `json:"mobile_url"`
	PreviewUrl                    string      `json:"preview_url"`
	Description                   string      `json:"description"`
	QuizType                      string      `json:"quiz_type"`
	AssignmentGroupId             CanvasId    `json:"assignment_group_id"`
	TimeLimit                     int64       `json:"time_limit"`
	ShuffleAnswers                bool        `json:"shuffle_answers"`
	HideResults                   string      `json:"hide_results"`
	ShowCorrectAnswers            bool        `json:"show_correct_answers"`
	ShowCorrectAnswersLastAttempt bool        `json:"show_correct_answers_last_attempt"`
	ShowCorrectAnswersAt          time.Time   `json:"show_correct_answers_at"`
	HideCorrectAnswersAt          time.Time   `json:"hide_correct_answers_at"`
	OneTimeResults                bool        `json:"one_time_results"`
	ScoringPolicy                 string      `json:"scoring_policy"`
	AllowedAttempts               int64       `json:"allowed_attempts"`
	OneQuestionAtATime            bool        `json:"one_question_at_a_time"`
	QuestionCount                 int64       `json:"question_count"`
	PointsPossible                float64     `json:"points_possible"`
	CantGoBack                    bool        `json:"cant_go_back"`
	AccessCode                    string      `json:"access_code"`
	IpFilter                      string      `json:"ip_filter"`
	DueAt                         time.Time   `json:"due_at"`
	LockAt                        time.Time   `json:"lock_at"`
	UnlockAt                      time.Time   `json:"unlock_at"`
	Published                     bool        `json:"published"`
	Unpublishable                 bool        `json:"unpublishable"`
	LockedForUser                 bool        `json:"locked_for_user"`
	LockInfo                      interface{} `json:"lock_info"`
	LockExplanation               string      `json:"lock_explanation"`
	SpeedgraderUrl                string      `json:"speedgrader_url"`
	QuizExtensionsUrl             string      `json:"quiz_extensions_url"`
	Permissions                   interface{} `json:"permissions"`
	AllDates                      interface{} `json:"all_dates"`
	VersionNumber                 int64       `json:"version_number"`
	QuestionTypes                 []string    `json:"question_types"`
	AnonymousSubmissions          bool        `json:"anonymous_submissions"`
}
type Answer struct {
	Id           CanvasId `json:"id"`
	Text         string   `json:"text"`
	Html         string   `json:"html"`
	Left         string   `json:"left"`
	Right        string   `json:"right"`
	Comments     string   `json:"comments"`
	CommentsHtml string   `json:"comments_html"`
	Weight       float64  `json:"weight"`
	MatchId      CanvasId `json:"match_id"`
}

type Match struct {
	Text    string   `json:"text"`
	MatchId CanvasId `json:"match_id"`
}

type QuizQuestion struct {
	Id                CanvasId `json:"id"`
	QuizId            CanvasId `json:"quiz_id"`
	Position          int      `json:"position"`
	QuestionName      string   `json:"question_name"`
	QuestionType      string   `json:"question_type"`
	QuestionText      string   `json:"question_text"`
	PointsPossible    float64  `json:"points_possible"`
	CorrectComments   string   `json:"correct_comments"`
	IncorrectComments string   `json:"incorrect_comments"`
	NeutralComments   string   `json:"neutral_comments"`
	Answers           []Answer `json:"answers"`
	Matches           []Match  `json:"matches"`
}

func (q Quiz) String() string {
	return fmt.Sprintf("%s (id=%d, type=%s, group=%d)", q.Title, q.Id, q.QuizType, q.AssignmentGroupId)
}
