/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"github.com/sirupsen/logrus"
	"net/url"
)

type FileModBuilder interface {
	Published(bool) FileModBuilder
	Move(newParentFolderId CanvasId) FileModBuilder
	Build() url.Values
}

type fileModBuilderImpl struct {
	spec url.Values
}

func (b *fileModBuilderImpl) Published(publish bool) FileModBuilder {
	if b == nil {
		panic("Receiver was nil")
	}
	if publish {
		b.spec.Add("locked", "false")
	} else {
		b.spec.Add("locked", "true")
	}
	return b
}

func (b *fileModBuilderImpl) Move(newParentFolderId CanvasId) FileModBuilder {
	if b == nil {
		panic("Receiver was nil")
	}
	b.spec.Add("parent_folder_id", newParentFolderId.ToBase10String())
	return b
}

func MakeFileModification() FileModBuilder {
	return &fileModBuilderImpl{spec: make(url.Values)}
}

func (b *fileModBuilderImpl) Build() url.Values {
	if b == nil {
		panic("Receiver was nil")
	}
	return b.spec
}

func (c *ApiImpl) ModifyFile(id CanvasId, mod url.Values) (FileInfo, error) {
	uri := c.makeUri("files", id.ToBase10String())
	logrus.Debugf("Modifying file with id %d. Modification %v", id, mod)
	logrus.Debugf("This is the uri: %s", uri.String())
	req, err := c.assembleFormEncodedRequest("PUT", uri.String(), &mod)
	if err != nil {
		return FileInfo{}, err
	}

	var finfo FileInfo
	err = c.processRequest(req, &finfo)
	if err != nil {
		return FileInfo{}, err
	}
	return finfo, nil
}
