/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020, 2022 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package rest

import (
	"fmt"
	"net/url"
)

func (c *ApiImpl) GetAccounts() ([]Account, error) {
	if c == nil {
		return nil, fmt.Errorf("No API")
	}
	req, err := c.assembleRequest("GET",
		c.makeUri("course_accounts").SetQuery(
			&url.Values{
				"per_page": {"10"},
			}).String(),
		nil)
	if err != nil {
		return nil, err
	}
	var array []Account
	err = loadAllPages(c, req, &array)
	if err != nil {
		return nil, err
	}
	return array, nil
}
