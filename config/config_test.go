/*
canvasctl: Use Canvas LMS from command line

	Copyright (C) 2020 Marcus Gelderie

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package config

import (
	"fmt"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
)

func TestConfigFilePaths(t *testing.T) {
	os.Setenv("HOME", "/home/is/home")
	os.Setenv("XDG_CONFIG_HOME", "/home/is/not/home/isays")

	locations := [][]string{
		{"$XDG_CONFIG_HOME", "anotherdir", "super"},
		{"$HOME", "dir", "bla"},
	}
	expected := []string{
		"/home/is/not/home/isays/anotherdir/super/canvas.conf",
		"/home/is/home/dir/bla/canvas.conf",
	}

	paths, err := configFilePaths(locations)
	if err != nil {
		t.Fatal("Failed to compute paths")
	}
	for i, item := range paths {
		require.Equal(t, item, expected[i])
	}
}

func TestConfigImplementation(t *testing.T) {
	expectations := map[string]string{
		"www.example.com": "mytoken",
		"www.another.com": "anothertoken",
	}
	config := config{
		Default: defaultSettings{
			URL: "www.default.com",
		},
		Sites: map[string]Site{
			"Bla": Site{
				URL:   "www.example.com",
				Token: expectations["www.example.com"],
			},
			"Foo": Site{
				URL:   "www.another.com",
				Token: expectations["www.another.com"],
			},
		},
	}
	require.Equal(t, config.GetDefaultURL(), "www.default.com")
	for url, expected := range expectations {
		url, err := config.GetTokenForURL(url)
		if err != nil {
			t.Fatal("Could not find token")
		}
		require.Equal(t, url, expected)
	}
}

func TestConfigFileLoading(t *testing.T) {
	config, err := LoadConfig()
	if err != nil {
		t.Fatal("Cannot load prepared config", err)
	}
	require.Equal(t, config.GetDefaultURL(), "https://www.example.com")
	token, err := config.GetTokenForURL(config.GetDefaultURL())
	if err != nil {
		t.Fatal("expteted to find a token")
	}
	require.Equal(t, token, "blablabla")
	_, err = config.GetTokenForURL("www.notinconfig.org")
	if err == nil {
		t.Fatal("Expected an error due to missing token in file")
	}
}

func TestMain(m *testing.M) {
	dir, err := ioutil.TempDir(os.TempDir(), "configXXX")
	if err != nil {
		fmt.Println("Cannot create temporary dir")
		os.Exit(1)
	}
	file, err := os.Create(filepath.Join(dir, "canvas.conf"))
	if err != nil {
		fmt.Println("Cannot create temporary file")
		os.Exit(1)
	}
	file.WriteString(ConfigMock)
	file.Close()
	defer os.Remove(file.Name())
	defer os.Remove(dir)
	configLocations = [][]string{{dir}}
	os.Exit(m.Run())
}

const ConfigMock = `[Default]
URL = "https://www.example.com"
[Sites]
[Sites.A]
URL = "https://www.example.com"
Token = "blablabla"`
